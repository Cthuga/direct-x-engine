#ifndef __TEXVERTEX_H_
#define __TEXVERTEX_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining custom libraries
#include "AbstractVertex.h"

namespace Engine_I
{
	//StructName  :  TexVertex
	//Description :  Check AbstractVertex for reference. This description is used for models that has textures and lighting
	struct TexVertex : AbstractVertex
	{
	public:
		TexVertex();
		TexVertex(D3DXVECTOR3 position, D3DXVECTOR3 normal,  D3DXCOLOR specPow, D3DXVECTOR2 texels);

		//Position of vertex
		D3DXVECTOR3 position;

		//Vertex normal used with light angle to define color intensity
		D3DXVECTOR3 normal;

		//Specular power of material
		D3DXCOLOR specPow;

		//Texture coordinates for vertex ( as texel )
		D3DXVECTOR2 texels;
	};
};

#endif