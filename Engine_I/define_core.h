//This is the place where I would work on compatibility between Direct X 10 and 11 versions. Actually there are pointed only defines 
//for included DirectX libraries. Reference for this header file is almost for each other headers.

#ifdef DX10
	#define D3DX <D3DX10.h>
	#define D3D <D3D10.h>
#else if DX11
	#define D3DX <D3DX11.h>
	#define D3D <D3D11.h>
#endif