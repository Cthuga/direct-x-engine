#ifndef __DRAWLIGHTS_H_
#define __DRAWLIGHTS_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <map>
#include <string>
#include <sstream>

//Defining custon libraries
#include "cbPerFrameDesc.h"
#include "LightDesc.h"
#include "GameTime.h"

namespace Engine_I
{
	//ClassName   : DrawLights
	//LifeTime    : Initialized during graphic initialization, executed on each frame
	//Description : Storage for all lights and their position
	class DrawLights
	{
	public:
		DrawLights();
		~DrawLights();

		//Name		  : Init
		//Description : Initialize lighting descriptions and fixed light number. 
		//Output	  : true when no errors, else false
		bool Init();

		//Name		  : Update
		//Description : Stores lighting behavior and position. Executed once for each frame
		//Input		  : gameTime - actual game time
		//				eyePos   - actual camera position
		void Update(GameTime gameTime, D3DXVECTOR3 eyePos);

		//Name		  : Shutdown
		//Description : Clearing routine
		void Shutdown();

		//Name		  : ChangePosition
		//Description : Change position of a single light
		//Input		  : index	 - index of updated light
		//				position - new position for light with specified index
		void ChangePosition(int index, D3DXVECTOR3 position);

		//Name		  : GetPosition
		//Description : Get actual position for single light with specified index
		//Input		  : index	 - index of updated light
		D3DXVECTOR3 GetPosition(int index);

		//Storage for all game lights
		CbPerFrameDesc lightDesc;

	private:

		//Actual game time
		GameTime time;

	};
};

#endif