#include "Model.h"

using namespace Engine_I;

Engine_I::Model::Model()
{
	vertexBuffer = 0;
	indexBuffer = 0;
	effect = 0;
	device = 0;
	indexBuffer = 0;
	Type = "Model";
	Topology = D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	Indexed = true;
	numFaces = 0;
	numIndices = 0;
	numVertices = 0;
	faultedState = 0;
	modelState.AbstractCBPerObjectDesc = new CbPerObjectDesc();
	modelState.Texture = 0;
	_ref = 0;
}


Engine_I::Model::~Model()
{

}

bool Engine_I::Model::Init(AbstractEffect *iEffect, ID3D10Device *&iDevice, MainTextInterface *&mainTextInterface, string name, ModelDesc *desc, StencilController *stencilController)
{
	effect = iEffect;
	device = iDevice;
	textInterface = mainTextInterface;
	this->Name = name;
	this->stencilController = stencilController;
	this->modelState.ModelDesc = ModelDesc(*desc);
	this->_ref = this;
	BasePlanes = Planes;

	return true;
}

bool Engine_I::Model::UpdateAndRender(AbstractCbPerObjectDesc *abstractEffectDesc)
{
	if(!Update(abstractEffectDesc)) return false;
	if(!this->Render()) return false;
	
	return true;
}

bool Engine_I::Model::Update(AbstractCbPerObjectDesc *abstractEffectDesc)
{
	CbPerObjectDesc temp = CbPerObjectDesc(*(CbPerObjectDesc*)abstractEffectDesc);

	if(!modelState.ModelDesc.ReflectedState &&  (!modelState.AbstractCBPerObjectDesc || modelState.AbstractCBPerObjectDesc->W != temp.W))
	{
		list<D3DXPLANE>::iterator it;
		Planes.clear();

		for(it = BasePlanes.begin(); it != BasePlanes.end(); it++)
		{
			D3DXPLANE out;
			D3DXMATRIX invMatrix;

		//	D3DXMATRIX transformationMatrix = temp.W;
			D3DXMatrixInverse(&invMatrix,0,&temp.W);
			D3DXMatrixTranspose(&invMatrix, &invMatrix);
			D3DXPlaneTransform(&out, &it._Ptr->_Myval, &invMatrix);
			D3DXPlaneNormalize(&out, &out);
		    Planes.push_back(D3DXPLANE(out));
		}
	}
	

	modelState.AbstractCBPerObjectDesc->W = D3DXMATRIX(temp.W);
	modelState.AbstractCBPerObjectDesc->WVP = D3DXMATRIX(temp.WVP);
	modelState.AbstractCBPerObjectDesc->texAdressingType = temp.texAdressingType;
	modelState.AbstractCBPerObjectDesc->Fog = temp.Fog;
	modelState.AbstractCBPerObjectDesc->Clip = temp.Clip;

	effect->UpdatePerObject(abstractEffectDesc);

	return true;
}

void Engine_I::Model::UpdateTexture(ID3D10ShaderResourceView *texture)
{
	if(modelState.Texture != texture)
	{
		modelState.Texture = texture;
	}

	effect->UpdatePerTexture(modelState.Texture);
}

bool Engine_I::Model::Render()
{
	if(faultedState != 0) return false;

	if(modelState.ModelDesc.Mirror)
	{
		return true;
		//device->OMSetDepthStencilState(stencilController->stateCreateNewStencil, 1);
	}

	UINT Stride = strideLength;

	if(strideLength == 0 || numIndices == 0 || numFaces == 0)
	{
		textInterface->Warning("Critical Error ! - during rendering Model, there were unitialized parameters");
		Trace::Error("Critical Error ! - during rendering Model, there were unitialized parameters");
		faultedState = 1;
		return false;
	}

	if(!vertexBuffer)
	{
		textInterface->Warning("Critical Error ! - during rendering Model, there was unitialized vertex Buffer");
		Trace::Error("Critical Error ! - during rendering Model, there was unitialized vertex Buffer");
		faultedState = 1;
		return false;
	}

	UINT Offset = 0;

	D3D10_TECHNIQUE_DESC techDesc;

	if(!effect->technique)
	{
		faultedState = 1;
		return false;
	}

	effect->technique->GetDesc(&techDesc);

	device->IASetInputLayout(effect->GetInputLayout());
	device->IASetPrimitiveTopology(Topology);

	if(modelState.ModelDesc.BlendState)
	{
		device->OMSetBlendState(modelState.ModelDesc.BlendState, modelState.ModelDesc.BlendFactor, 0xFFFFFFFF);
	}
	else
	{
		device->OMSetBlendState(0, 0, 0xFFFFFFFF);
	}

	for(UINT i = 0; i < techDesc.Passes ; i++)
	{
		effect->technique->GetPassByIndex(i)->Apply(0);

		device->IASetVertexBuffers(0, 1, &vertexBuffer, &Stride, &Offset);

		if(Indexed)
		{
			device->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
			device->DrawIndexed(numIndices, 0, 0);
		}
		else
		{
			device->Draw(numVertices,0);
		}
	}

 //	device->OMSetDepthStencilState(0,0);

	return true;
}

void Engine_I::Model::ShutDown()
{
	if(vertexBuffer)
	{
		vertexBuffer->Release();
		vertexBuffer = 0;
	}

	if(indexBuffer)
	{
		indexBuffer->Release();
		indexBuffer = 0;
	}

	if(_ref)
	{
		delete _ref;
		_ref = 0;
	}
}

void Engine_I::Model::AttachNewEffect(AbstractEffect *iEffect)
{
	effect = iEffect;
}

bool Engine_I::Model::CreateNewBuffers(AbstractVertex vertices[], int indices[], int sizeofVerticesStruct)
{
	HRESULT result;

	D3D10_BUFFER_DESC vertexDesc;
	ZeroMemory(&vertexDesc, sizeof(vertexDesc));

	vertexDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	vertexDesc.ByteWidth = sizeofVerticesStruct * numVertices;
	vertexDesc.CPUAccessFlags = 0;
	vertexDesc.MiscFlags = 0;
	vertexDesc.Usage = D3D10_USAGE_IMMUTABLE;

	D3D10_BUFFER_DESC indexDesc;
	ZeroMemory(&indexDesc, sizeof(indexDesc));

	indexDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	indexDesc.ByteWidth = sizeof(int) * numIndices;
	indexDesc.CPUAccessFlags = 0;
	indexDesc.MiscFlags = 0;
	indexDesc.Usage = D3D10_USAGE_IMMUTABLE;

	D3D10_SUBRESOURCE_DATA vData;
	ZeroMemory(&vData, sizeof(vData));

	vData.pSysMem = vertices;

	D3D10_SUBRESOURCE_DATA iData;
	ZeroMemory(&iData, sizeof(iData));

	iData.pSysMem = indices;

	result = device->CreateBuffer(&vertexDesc, &vData, &vertexBuffer);
	if(FAILED(result))
	{
		string message = "Failed to create vertex buffer";
		Trace::Log(message);
		textInterface->Warning(message);
		return false;
	}

	result = device->CreateBuffer(&indexDesc, &iData, &indexBuffer);
	if(FAILED(result))
	{
		string message = "Failed to create index buffer";
		Trace::Log(message);
		textInterface->Warning(message);
		return false;
	}

	return true;
}

string Engine_I::Model::GetTechniqueName()
{
	D3D10_TECHNIQUE_DESC techDesc;
	ZeroMemory(&techDesc, sizeof(techDesc));

	effect->technique->GetDesc(&techDesc);
	
	return techDesc.Name;
}

int Engine_I::Model::GetIndicesCount()
{
	return numIndices;
}

int Engine_I::Model::GetVerticesCount()
{
	return numVertices;
}

int Engine_I::Model::GetNumFaces()
{
	return numFaces;
}

ModelState* Engine_I::Model::GetLastModelState()
{
	return &modelState;
}

void Engine_I::Model::Refresh()
{
	effect->UpdatePerObject(this->modelState.AbstractCBPerObjectDesc);
}

CbPerObjectDesc* Engine_I::Model::GetPerObjectDesc()
{
	return modelState.AbstractCBPerObjectDesc;
}

bool Engine_I::Model::UpdateDescription(ModelDesc modelDesc)
{
	this->modelState.ModelDesc = modelDesc;

	return true;
}