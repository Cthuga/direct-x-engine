#include "MirrorController.h"

using namespace Engine_I;

Engine_I::MirrorController::MirrorController()
{
	device = 0;
	mainTextInterface = 0;
	world = 0;
	stencilController = 0;
}

Engine_I::MirrorController::~MirrorController()
{

}		

Engine_I::MirrorController::MirrorController(const MirrorController& other)
{

}

MirrorController& Engine_I::MirrorController::operator=(const MirrorController& other)
{
	return MirrorController(other);
}

bool Engine_I::MirrorController::Init(ID3D10Device *device, ID3D10DepthStencilView *depthStencilView, MainTextInterface *mainTextInterface, World *world, StencilController *stencilController, RasterController *rasterController, BlendController *blendController)
{
	this->device = device;
	this->mainTextInterface = mainTextInterface;
	this->world = world;
	this->stencilController = stencilController;
	this->rasterController = rasterController;
	this->blendController = blendController;
	this->depthStencilView = depthStencilView;

	return true;
}

void Engine_I::MirrorController::Frame(World *world, DrawLights *drawLights, D3DXMATRIX V, D3DXMATRIX P, D3DXVECTOR3 eyePos)
{
	list<Model*> mirrors = GetMirrorsFromWorld();
	map<string, Model*> *models = world->GetModels();

	list<Model*>::iterator mirrorIt;
	 map<string, Model*> ::iterator modelIt;

	list<D3DXPLANE>::iterator it;

	int counter = 1;

	int numLights = drawLights->lightDesc.LightNum;

	for(mirrorIt = mirrors.begin() ; mirrorIt != mirrors.end() ; mirrorIt++)
	{
		for(it =  mirrorIt._Ptr->_Myval->Planes.begin() ; it !=  mirrorIt._Ptr->_Myval->Planes.end() ; it++)
		{
			device->OMSetDepthStencilState(stencilController->stateCreateNewStencil, counter);
			
			ModelState* mmodelState =  mirrorIt._Ptr->_Myval->GetLastModelState();
			CbPerObjectDesc* mdesc =  mirrorIt._Ptr->_Myval->GetPerObjectDesc();
			ModelDesc md = mmodelState->ModelDesc;
			md.Mirror = false;
		
			mirrorIt._Ptr->_Myval->UpdateDescription(md);
			mirrorIt._Ptr->_Myval->UpdateTexture(mmodelState->Texture);
			mirrorIt._Ptr->_Myval->UpdateAndRender(mdesc);
		
			md.Mirror = true;
			mirrorIt._Ptr->_Myval->UpdateDescription(md);

			counter ++;
		}
	}

	device->ClearDepthStencilView(depthStencilView, D3D10_CLEAR_DEPTH , 1.0f, 0);
	
	CbPerFrameDesc ld;

	device->RSSetState(rasterController->RSBackFace);

	counter = 1;

	LightDesc tempLights[256];
	for(int i = 0 ; i < numLights ; i++)
	{
		tempLights[i] = drawLights->lightDesc.Lights[i];
	}
	
	for(mirrorIt = mirrors.begin() ; mirrorIt != mirrors.end() ; mirrorIt++)
	{
		for(it =  mirrorIt._Ptr->_Myval->Planes.begin() ; it !=  mirrorIt._Ptr->_Myval->Planes.end() ; it++)
		{	
			for(int i = 0; i < numLights; i++)
			{		
					D3DXVECTOR4 lightPos = D3DXVECTOR4(drawLights->GetPosition(i).x,drawLights->GetPosition(i).y,drawLights->GetPosition(i).z,1);
					D3DXMATRIX transposeMatrix;
					D3DXMATRIX reflectMatrix;

					D3DXMatrixReflect(&reflectMatrix, &it._Ptr->_Myval);
					D3DXMatrixTranspose(&transposeMatrix, &reflectMatrix);
					
					D3DXVec4Transform(&lightPos, &lightPos, &transposeMatrix);

					drawLights->ChangePosition(i, D3DXVECTOR3(lightPos));							
					
					ld.Lights[i] = LightDesc(drawLights->lightDesc.Lights[i]);	

					D3DXVec3TransformNormal(&ld.Lights[i].Dirrection, &ld.Lights[i].Dirrection, &reflectMatrix); 
			}
	
			ld.EyePos = eyePos;
			ld.LightNum = numLights;

			world->UpdatePerFrame(&ld);
			
			for(int i = 0 ; i < numLights ; i++)
			{
				drawLights->lightDesc.Lights[i] = tempLights[i];
			}

			device->OMSetDepthStencilState(stencilController->stateWriteToExistingStencilOnly, counter);
			counter++;

			for(modelIt = models->begin() ; modelIt != models->end() ; modelIt++)
			{
				ModelState* modelState = modelIt->second->GetLastModelState();
				ModelDesc modelDescription = modelState->ModelDesc;

				if(modelDescription.Mirrored)
				{
					CbPerObjectDesc* modelStateDesc = new CbPerObjectDesc(*modelState->AbstractCBPerObjectDesc);
				
					if(mirrorIt._Ptr->_Myval->Name != modelIt._Ptr->_Myval.second->Name)
					{
						ModelState *modelState = models->operator[](modelIt._Ptr->_Myval.second->Name)->GetLastModelState();
						CbPerObjectDesc* desc = modelIt._Ptr->_Myval.second->GetPerObjectDesc();

						ModelDesc modelDesc = modelState->ModelDesc;
						modelDesc.BlendState = blendController->MirrorBlending;
						modelDesc.Mirror = false;
						modelDesc.ReflectedState = true;
						modelDesc.BlendFactor[0] = 0.4f; 
						modelDesc.BlendFactor[1] = 0.4f; 
						modelDesc.BlendFactor[2] = 0.4f; 
						modelDesc.BlendFactor[3] = 1.0f; 

						D3DXMATRIX W = desc->W;
						D3DXMATRIX R;
						D3DXMatrixReflect(&R, &it._Ptr->_Myval);
		
						CbPerObjectDesc reflectDesc;
						reflectDesc.WVP = W * R * V * P;
						reflectDesc.W = W * R;
						reflectDesc.Clip  = desc->Clip;
						reflectDesc.texAdressingType = desc->texAdressingType;
						reflectDesc.Fog = desc->Fog;
						
						modelIt._Ptr->_Myval.second->UpdateDescription(modelDesc);
						modelIt._Ptr->_Myval.second->UpdateTexture(modelState->Texture);
						modelIt._Ptr->_Myval.second->UpdateAndRender(&reflectDesc);
					}

					modelIt->second->Update(modelStateDesc);
					modelIt->second->UpdateDescription(modelDescription);			
				}
			}

			device->ClearDepthStencilView(depthStencilView, D3D10_CLEAR_DEPTH , 1.0f, 0);
			
		}
	}

	device->RSSetState(0);
	device->OMSetDepthStencilState(0,0);
}

void Engine_I::MirrorController::ShutDown()
{
	if(mainTextInterface)
	{
		delete mainTextInterface;
		mainTextInterface = 0;
	}

	if(world)
	{
		delete world;
		world = 0;
	}
}

list<Model*> Engine_I::MirrorController::GetMirrorsFromWorld()
{
	map<string, Model*>::iterator it;
	map<string, Model*> *allModels = world->GetModels();
	list<Model*> lists;

	for(it = allModels->begin() ; it != allModels->end() ; it++)
	{
		ModelState* modelState = it->second->GetLastModelState();
		ModelDesc desc = modelState->ModelDesc;

		if(desc.Mirror)
		{
			lists.push_back(it->second);
		}
	}

	return lists;
}

void Engine_I::MirrorController::Reflect(Model* model, Model* mirror, D3DXMATRIX V, D3DXMATRIX P)
{	



		
	
}