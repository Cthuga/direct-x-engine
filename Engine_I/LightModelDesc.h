#ifndef __LIGHTMODELDESC_H_
#define __LIGHTMODELDESC_H_

#include "define_core.h"

#include D3D
#include D3DX

#include <Windows.h>

#include "ModelDesc.h"

namespace Engine_I
{
	struct LightModelDesc : ModelDesc
	{
	public:
		LightModelDesc();
		~LightModelDesc();

		D3DXCOLOR Diffuse;
	};
};

#endif