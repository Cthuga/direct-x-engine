#include "BaseEffectDesc.h"

using namespace Engine_I;

Engine_I::BaseEffectDesc::BaseEffectDesc()
{
	ZeroMemory(this, sizeof(this));

	D3DXMatrixIdentity(&W);
	D3DXMatrixIdentity(&WVP);

	texAdressingType = -1;
	Clip = false;
}

Engine_I::BaseEffectDesc::BaseEffectDesc(D3DXMATRIX WVP)
{
	ZeroMemory(this, sizeof(this));

	this->WVP = WVP;
}