//				As reminder description of Direct X rendering stages :
//
//					Input Assembler Stage <------------------+
//							||								 |
//							\/								 |
//					 Vertex Shader Stage <-------------------+
//							||								 |
//							\/								 |
//				    Geometry Shader Stage <------------------+
//							||								 |
//							\/								 |
//							 +----> Stream Output Stage ---->+-----> GPU Resources, Buffers, Textures
//							||								 |	 
//							\/								 |	(the communication between GPU and CPU has been done through World class - check World.h
//					 Rasterizer Stage <----------------------+		       It is : World <---> EffectController <---> HLSL Script Files )	
//							||							     |				     Check World.H and EffectController.h for reference
//							\/								 |
//					Pixel Shader Stage <---------------------+
//							||							     |
//							\/							     |
//			       Output Merger Stage <-------------------->+

//Define helper effect script files
#include "light_helper.fx"
#include "fog.fx"

//Communication between GPU ( effect script file ) and CPU ( C++ code ) has been done with help of cbuffer. 
//Cbuffer stores values that can be get/set from C++ code. Because of engine architecture there is few "cbuffers". Every cbuffer is updated in different stage of frame:

//cbFixed is updated during initialization of application. It is updated only once
cbuffer cbFixed
{
	Fog fog;	//Structure with fog description ( check fog.fx for reference )
};

//cbPerFrame is updated once per frame
cbuffer cbPerFrame
{
	Light lightArray[256];	//description of all lights in game. Because array has fixed length, it is set to maximum number of lights 256. ( Check "light_helper.fx" for reference )
	float3 eyePos;			//actual position of camera. It is used to calculate some lighting effect
	int lightsCount;		//number of lights is passed because of performance reson. I am thinking about moving this value to "cbfixed" cbuffer.
};

//cbPerObject is updated once per rendered model
cbuffer cbPerObject
{
	float4x4 W;				//world transformation matrix - necessary for transformation calculations
	float4x4 WVP;			//world transformation matrix * view transformation matrix * presentation transformation matrix - representation of final position of object according to camera view
	int texAdressingType;	//textures might be attached to model in different ways. Actually there are prepared 2 ways of attaching : 1 - change size of texture to size of the face ; 2 - fill face with fixed size texture pattern  
	bool gClip;				//when set, add clipping logic to result
	bool gFog;				//when set, add fog logic to result
};

//gTexture is updated once per changing texture
Texture2D gTexture;

//This structure is passed as INPUT to Vertex Shader Stage. Reminder of render stages is described in RasterController.h. Also check TexVertex.h for C++ description of this InputStructure.
//Connection between C++ and GPU version of this structure is made in Model class ( check Model.h for reference ).
//Whole process take place in World class ( check World.h for reference )
struct VS_IN
{
	float3  pos	    : POSITION;		//Position of vertex
	float3  normal  : NORMAL;		//Vertex normal used with light angle to define color intensity
	float4  specPow : SPEC_POW;		//Specular power of material
	float2  texels  : TEXELS;		//Texture coordinates for vertex ( as texel )
};

//This structure is passed as OUTPUT from Vertex Shader Stage to INPUT Pixel Shader Stage. Reminder of render stages is described in RasterController.h
struct VS_OUT
{
	float4 pos		: SV_POSITION;	//Position of vertex
	float3 posW		: POSITION;		//Position of vertex transformated in World space
	float3 normal   : NORMAL;		//Vertex normal used with light angle to define color intensity
	float4 specPow  : SPEC_POW;		//Specular power of material
	float2 texels   : TEXELS;		//Texture coordinates for vertex ( as texel )
	float fogSat    : FOG;			//Fog saturation value calculated during Vertex Shader Stage
};

//As it was mentioned in comment of texAdressingType (cbPerObject) there are different ways of attaching textures to faces. Here are descriptions of attaching methods 

//Change size of texture to size of the face
SamplerState borderSampler
{
	Filter = ANISOTROPIC;	//Filter that makes best visual effects for textures seen in angle
	AddressU = BORDER;		//Change size of texture to size of the face in U coordinate
	AddressV = BORDER;		//Change size of texture to size of the face in V coordinate
};

//Fill face with fixed size texture pattern  
SamplerState wideSampler
{
	Filter = ANISOTROPIC;	//Filter that makes best visual effects for textures seen in angle
	AddressU = WRAP;		//Fill face with fixed size texture pattern  in U coordinate
	AddressV = WRAP;		//Fill face with fixed size texture pattern  in V coordinate
};

//This is body of Vertex Shader Stage - logic executed once per vertex ( input was described earlier, check also TexVertex.h for C++ input description )
VS_OUT VS(VS_IN vIn)
{
	VS_OUT vOut;
	
	float4 helper;

	//This part is responsible for calculation of position every vertex and normal in 2d result screen
		vOut.pos = mul(float4(vIn.pos, 1.0f) , WVP);

		helper = mul(float4(vIn.pos, 1.0f), W);
		vOut.posW = float3(helper.x, helper.y, helper.z);

		helper= mul(float4(vIn.normal, 0.0f), WVP);
		vOut.normal = float3(helper.x, helper.y, helper.z);
	//end of calcularion

	//these values are simply passed to Pixel Shader stage
	vOut.specPow = vIn.specPow;
	vOut.texels = vIn.texels;
	
	//For each vertex there is calculated fog saturation and passed to Pixel Shader Stage
	float d = distance(vOut.posW, eyePos);
	vOut.fogSat = saturate((d - fog.gFogStart) /fog.gFogRange );

	return vOut;
}

//This is body of Pixel Shader Stage - logic executed once per pixel seen on the screen
float4 PS(VS_OUT vIn) : SV_TARGET
{
	//Prepare empty color 
	float4 outColor = {0.0f, 0.0f, 0.0f, 0.0f};
	
	//Attach texture color for this pixel based on gTexture value. Color is determined according to texAdressingType. Check texAdressingType comment description.
	if(texAdressingType == 2)
	{
		outColor = gTexture.Sample(wideSampler, vIn.texels);
	}
	else
	{
		outColor = gTexture.Sample(borderSampler, vIn.texels);
	} 
	
	//If there is clip flag set, and texture has got alpha value, then texture with nearly minimum alpha value would be clipped to get transparent effect.
	if(gClip)
	{
		clip(outColor.a - 0.15f);
	}

	//From here there is updating pixel color value based on each light described in lightArray ( check cbPerFrame->lightArray few lines up )
	
	//Surface info is helper structure described in light_helper.fx
	SurfaceInfo vSurface = { vIn.posW, vIn.normal, outColor, vIn.specPow };
	
	float3 resultCol = {0,0,0};
	
	//Now according to every light type ( check light_helper.fx for reference ) update pixel color using concrete lighting type method
	for(int i = 0; i < lightsCount ; i++)
	{
		  if(lightArray[i].type == 1)
		  {
			 resultCol += ParrallelLight(vSurface, lightArray[i], eyePos);
		  }
		  else if(lightArray[i].type == 2)
		  {
			 resultCol += PointLight(vSurface, lightArray[i], eyePos);
		  }
		  else if(lightArray[i].type == 3)
		  {
			 resultCol += SpotLight(vSurface, lightArray[i], eyePos);
		  }
	} 

	float3 foggedColor = {0.0f, 0.0f, 0.0f};

	//After preparing color updated by each light value, add fog color value if fog color flag was set
	if(gFog)
	{
		foggedColor = lerp( resultCol, fog.gFogColor, vIn.fogSat );
	}
	else
	{
		foggedColor = resultCol;
	}

	//Return final color to next stage
	return float4(foggedColor, outColor.a);
}

//Technique representing effect for C++ code. We can say that's Main function for HLSL script files. Every model has attached Effect class ( see AbstractEffect.h for reference )
//and every Effect class has attached technique executed once per frame.
technique10 BaseTechnique
{
	//Every technique for real have array of "Main" functions. They might be combined in different ways during rendering models in C++ code. Every index in this array is "Pass" with number attached to it.
	//Actually there is only one Pass and one Technique per effect
	pass P0
	{
		//In Pass body there is Stage Rendering Routine ( check beginning of this file for reminder )

		//First execute method VS for Vertex Shader Stage
		SetVertexShader( CompileShader( vs_4_0, VS()) );

		//Then there is Geometry Shader Stage, actually empty
		SetGeometryShader( NULL );

		//Finally execute method PD for Pixel Shader Stage
		SetPixelShader ( CompileShader( ps_4_0, PS()) );
	}
}