#include "TextureController.h"

using namespace Engine_I;

Engine_I::TextureController::TextureController()
{
	this->textureInterface = TextureInterface();
}

Engine_I::TextureController::~TextureController()
{

}

bool Engine_I::TextureController::Init(ID3D10Device *device, MainTextInterface *mainTextInterface)
{
	WIN32_FIND_DATA ff;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	this->device = device;
	this->mainTextInterface = mainTextInterface;

	textureInterface.Init(device, mainTextInterface);

	hFind = FindFirstFile(L".\\Textures\\*", &ff);

	if(hFind == INVALID_HANDLE_VALUE)
	{
		Trace::Error("Nie istnieje folder z teksturami");
		mainTextInterface->Warning("Texture dirrectory not found");
		return false;
	}
	else
	{
		textureContainer.ConainerName = ".\\Textures\\";
	}

	do
	{
		wstring fileName = ff.cFileName;
		if(fileName == L"." || fileName == L"..") continue;

		string sFileName = string(fileName.begin(), fileName.end());

		if(ff.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			ostringstream containerName;
			containerName << textureContainer.ConainerName << sFileName << '\\';
			textureContainer.ChildContainers[containerName.str()].ConainerName = containerName.str();

			FindFilesInDir(&textureContainer.ChildContainers[containerName.str()]);
		}
		else
		{		
			ostringstream textureName;
			textureName << textureContainer.ConainerName << sFileName;

			textureInterface.AddTextureFromFile(textureName.str());
			textureContainer.Textures.push_back(textureName.str());
		}
	}
	while(FindNextFile(hFind, &ff) != 0);

	return true;
}

bool Engine_I::TextureController::FindFilesInDir(TextureContainer *container)
{
	WIN32_FIND_DATA ff;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	wostringstream ofdirrectory;
	ofdirrectory << wstring(container->ConainerName.begin(), container->ConainerName.end()) << "*";
	wstring dirrectory = ofdirrectory.str();

	hFind = FindFirstFile(dirrectory.c_str(), &ff);

	if(hFind == INVALID_HANDLE_VALUE)
	{
		return true;
	}

	do
	{
		wstring fileName = ff.cFileName;
		if(fileName == L"." || fileName == L"..") continue;

		string sFileName = string(fileName.begin(), fileName.end());

		if(ff.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			ostringstream containerName;
			containerName << container->ConainerName << sFileName << '\\';
			container->ChildContainers[containerName.str()].ConainerName = containerName.str();

			FindFilesInDir(&container->ChildContainers[containerName.str()]);
		}
		else
		{		
			ostringstream textureName;
			textureName << container->ConainerName << sFileName;

			textureInterface.AddTextureFromFile(textureName.str());
			container->Textures.push_back(textureName.str());
		}
	}
	while(FindNextFile(hFind, &ff) != 0);

	return true;
}

void Engine_I::TextureController::ShutDown()
{
	textureInterface.ShutDown();
}

TextureContainer Engine_I::TextureController::GetTextureContainer()
{
	return textureContainer;
}

ID3D10ShaderResourceView* Engine_I::TextureController::GetTexture(string textureName)
{
	return textureInterface.GetTexture(textureName);
}