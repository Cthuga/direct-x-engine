#include "window.h"

using namespace Engine_I;

Engine_I::Window::Window(LPCWSTR name)
{
	hinstance = GetModuleHandle(NULL);
	className = name;
	deviceStarted = false;
	appPaused = false;
	this->hwnd = 0;
}

Engine_I::Window::~Window()
{

}

void Engine_I::Window::ShutDown()
{
	ShowCursor(true);

	graphic.ShutDown();

	if(hwnd)
	{
		DestroyWindow(hwnd);
		hwnd = 0;
	}

	if(hinstance)
	{
		UnregisterClass(className, hinstance);
		hinstance = 0;
	}
}

bool Engine_I::Window::Init()
{
	//This is initialization of *.ini file - some system-level configuration informations
	if(!engine.Init())
	{
		Trace::Error("Failed to load configuration file *.ini");
		return false;
	}

	Config config = engine.GetConfig();

	width = config.configGraphic.ScreenWidth;
	height = config.configGraphic.ScreenHeight;

	WNDCLASSEX wcex;
	ZeroMemory(&wcex, sizeof(wcex));

	wcex.cbClsExtra = 0;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.cbWndExtra = 0;
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOWFRAME);
	wcex.hCursor = 0;
	wcex.hIcon = 0;
	wcex.hIconSm = 0;
	wcex.hInstance = hinstance;
	wcex.lpfnWndProc = (WNDPROC)WndProc;
	wcex.lpszClassName = className;
	wcex.lpszMenuName = NULL;
	wcex.style = CS_VREDRAW | CS_HREDRAW;

	RegisterClassEx(&wcex);

	HWND desktopHWND = GetDesktopWindow();

	RECT systemRect = { 0, 0, 0, 0 };	
	GetWindowRect(desktopHWND, &systemRect);

	int leftX = (int)floor((double)((systemRect.right - config.configGraphic.ScreenWidth) / 2));
	int upY = (int)floor((double)((systemRect.bottom - config.configGraphic.ScreenHeight) / 2));
	
	RECT windowRect = { leftX, upY, width + leftX, height + upY};
	AdjustWindowRect(&windowRect,  WS_OVERLAPPEDWINDOW, false);

	appPaused = false;

	HWND hwndTemp = CreateWindow(className, className, WS_OVERLAPPEDWINDOW, windowRect.left, windowRect.top, windowRect.right - windowRect.left, windowRect.bottom - windowRect.top, NULL, NULL, hinstance, this);
	if(!hwndTemp) 
	{
		Trace::Error("Failed to create window");
		return false;
	}

	hwnd = hwndTemp;
	input.ResetKeys();

	//Before window is shown, every part of graphic engine is also initialized. This method makes a lot. 
	if(!graphic.Init(engine, hwnd))
	{
		Trace::Error("--Failed to init graphic, application is closing");
		return false;
	}

	ShowWindow(hwnd, SW_SHOW);
	UpdateWindow(hwnd);

	ShowCursor(false);

	Trace::Log("Window started");
	
	deviceStarted = true;

	return true;
}

void Engine_I::Window::Run()
{
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));

	timer.Reset();
	Trace::Log("Starting Message Peeking");
	
	//This should be done at Graphic::Init - TODO: REMOVE this part
	graphic.InitCore();

	while(msg.message != WM_QUIT)
	{
		if(PeekMessage(&msg, hwnd, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);

			if(msg.message == WM_QUIT)
			{
				return;
			}
		}
		
		if(!appPaused)
		{
			//And everything that is dependent on freme is executed here
			graphic.Frame(timer.GetGameTime(), input);
		}
		else
		{
			Sleep(50);
		}

		//For each frame update also game clock
		timer.Tick();
	}

}


LRESULT CALLBACK Engine_I::WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static Window *app = 0;
	switch(message)
	{
		case WM_CREATE:
			CREATESTRUCT* cs = (CREATESTRUCT*)lParam;		
			app = (Window*)cs->lpCreateParams;
			return 0;

	}

	if( app )
		return app->MessageHandler(hwnd, message, wParam, lParam);
	else
		return DefWindowProc(hwnd, message, wParam, lParam);
}

LRESULT Engine_I::Window::MessageHandler(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	ostringstream str;
	str.str("");

	//this is main message handling. A lot of glibberish stuff here at this moment. Sorry for that
	switch(message)
	{		
	/*	case WM_KEYDOWN:			
			str << "==MESSAGE== WM_KEYDOWN : " << WM_CHAR;
			Trace::Log(str.str());
			input.KeyDown(WM_CHAR);
			return 0;

		case WM_KEYUP:
			str << "==MESSAGE== WM_KEYUP : " << WM_CHAR;
			Trace::Log(str.str());
			input.KeyUp(WM_CHAR);
			return 0;*/

		case WM_CLOSE:
			Trace::Log("==MESSAGE== WM_CLOSE");
			PostQuitMessage(0);
			return 0;

		case WM_DESTROY:
			Trace::Log("==MESSAGE== WM_DESTROY");
			PostQuitMessage(0);
			return 0;

		case WM_QUIT:
			Trace::Log("==MESSAGE== WM_QUIT");
			PostQuitMessage(0);
			return 0;

		case WM_ACTIVATE:		
			if(LOWORD(wParam) == WA_INACTIVE)
			{
				Trace::Log("==MESSAGE== WM_INACTIVATE");
				timer.Pause();
				appPaused = true;
			}
			else
			{
				Trace::Log("==MESSAGE== WM_ACTIVATE");
				timer.UnPause();
				appPaused = false;
			}
			return 0;

		case WM_SIZE:
				//Resizing window routine
				if(LOWORD(lParam))
				{
					width = LOWORD(lParam);
					height = HIWORD(lParam);
					graphic.width = width;
					graphic.height = height;

					if(deviceStarted)
					{
						switch(wParam)
						{
							case SIZE_MINIMIZED:
								minimalized = true;
								maximalized = false;
								appPaused = true;
								timer.Pause();
								return 0;

							case SIZE_MAXIMIZED:
								minimalized = false;
								maximalized = true;
								timer.UnPause();
								appPaused = false;
								OnResize();
								return 0;

							case SIZE_RESTORED:
								if(minimalized)
								{
									minimalized = false;
									timer.UnPause();
									appPaused = false;
									OnResize();
								}
								else if(maximalized)
								{
									maximalized = false;
									OnResize();
								}
								else if(resizing)
								{
									appPaused = true;
									timer.Pause();
								}
								else
								{
									OnResize();
								}
						}		
					}
					return 0;
					
					case WM_GETMINMAXINFO:
						((MINMAXINFO*)lParam)->ptMinTrackSize.x = 400;
						((MINMAXINFO*)lParam)->ptMinTrackSize.y = 400;
						return 0;
				}
				else
				{
					return DefWindowProc(hwnd, message, wParam, lParam);
				}
			return 0;

		case WM_ENTERSIZEMOVE:
			Trace::Log("==MESSAGE== WM_ENTERSIZEMOVE");
			timer.Pause();
			appPaused = true;
			resizing = true;
			return 0;

		case WM_EXITSIZEMOVE:
			width = LOWORD(wParam);
			height = HIWORD(wParam);
			Trace::Log("==MESSAGE== WM_EXITSIZEMOVE");
			OnResize();
			timer.UnPause();
			appPaused = false;
			resizing = false;
			return 0;
	}

	return DefWindowProc(hwnd, message, wParam, lParam);
}

void Engine_I::Window::OnResize()
{
	graphic.Resize();
}