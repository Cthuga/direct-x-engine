#include "d3xcore.h"

#ifdef __DEBUG
  #define DEVICE_FLAGS D3D10_CREATE_DEVICE_DEBUG | D3D10_CREATE_DEVICE_PREVENT_INTERNAL_THREADING_OPTIMIZATIONS
#else
  #define DEVICE_FLAGS 0
#endif

using namespace Engine_I;

Engine_I::D3XCore::D3XCore()
{

}

Engine_I::D3XCore::~D3XCore()
{

}

bool Engine_I::D3XCore::Init(HWND hwnd, int width, int height, ID3D10Device*& device, IDXGISwapChain*& swapChain, ID3D10RenderTargetView*& renderTargetView, ID3D10DepthStencilView*& depthStencilView, ID3DX10Font*& font, bool fullscreen)
{
	HRESULT result;

	DXGI_SWAP_CHAIN_DESC sDes;
	ZeroMemory(&sDes, sizeof(sDes));

	sDes.BufferCount = 1;
	sDes.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sDes.BufferDesc.Height = height;
	sDes.BufferDesc.RefreshRate.Denominator = 1;
	sDes.BufferDesc.RefreshRate.Numerator = 60;
	sDes.BufferDesc.Scaling = DXGI_MODE_SCALING_CENTERED;
	sDes.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sDes.BufferDesc.Width = width;
	sDes.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sDes.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	sDes.OutputWindow = hwnd;
	sDes.SampleDesc.Count = 1;
	sDes.SampleDesc.Quality = 0;
	sDes.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	sDes.Windowed = !fullscreen;

	result = D3D10CreateDeviceAndSwapChain(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, DEVICE_FLAGS, D3D10_SDK_VERSION, &sDes, &swapChain, &device);
	if(FAILED(result))
	{
		Trace::Error("Failed to create SwapChain during initialization of Direct X");
		return false;
	}

	CommonInit(hwnd, width, height, device, swapChain, renderTargetView, depthStencilView, font, fullscreen);

	return true;
}

bool Engine_I::D3XCore::CommonInit(HWND hwnd, int width, int height, ID3D10Device*& device, IDXGISwapChain*& swapChain, ID3D10RenderTargetView*& renderTargetView, ID3D10DepthStencilView*& depthStencilView, ID3DX10Font*& font, bool fullscreen)
{
	HRESULT result;

	ID3D10Texture2D* backBuffer = 0;
	ID3D10Texture2D* depthBuffer = 0;

	result = swapChain->GetBuffer(0, __uuidof(ID3D10Texture2D), (LPVOID*)&backBuffer);
	if(FAILED(result))
	{
		if(backBuffer)
		{
			backBuffer->Release();
			backBuffer = 0;
		}

		Trace::Error("Failed to access BackBuffer during initialization of Direct X");
		return false;
	}

	result = device->CreateRenderTargetView(backBuffer, 0, &renderTargetView);
	if(FAILED(result))
	{
		if(backBuffer)
		{
			backBuffer->Release();
			backBuffer = 0;
		}

		Trace::Error("Failed to create RenderTargetView during Direct X initialization");
		return false;
	}

	backBuffer->Release();
	backBuffer = 0;

	D3D10_TEXTURE2D_DESC dsDesc;
	ZeroMemory(&dsDesc, sizeof(dsDesc));

	dsDesc.ArraySize = 1;
	dsDesc.BindFlags = D3D10_BIND_DEPTH_STENCIL;
	dsDesc.CPUAccessFlags = 0;
	dsDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dsDesc.Height = height;
	dsDesc.MipLevels = 1;
	dsDesc.MiscFlags = 0;
	dsDesc.SampleDesc.Count = 1;
	dsDesc.SampleDesc.Quality = 0;
	dsDesc.Usage = D3D10_USAGE_DEFAULT;
	dsDesc.Width = width;

	result = device->CreateTexture2D(&dsDesc, NULL, &depthBuffer);
	if(FAILED(result))
	{
		if(depthBuffer)
		{
			depthBuffer->Release();
			depthBuffer = 0;
		}

		Trace::Error("Failed to create DepthStencilView Buffer during initialization of Direct X");
		return false;
	}

	result = device->CreateDepthStencilView(depthBuffer, 0, &depthStencilView);
	if(FAILED(result))
	{
		if(depthBuffer)
		{
			depthBuffer->Release();
			depthBuffer = 0;
		}

		Trace::Error("NFailed to create DepthStencilView  during initialization of Direct X");
		return false;
	}

	depthBuffer->Release();
	depthBuffer = 0;

	D3D10_VIEWPORT viewport;
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.MaxDepth = 1.0f;
	viewport.MinDepth = 0;
	viewport.Height = height;
	viewport.Width = width;

	device->RSSetViewports(1, &viewport);

	D3DX10_FONT_DESC fontDes;
	ZeroMemory(&fontDes, sizeof(fontDes));

	fontDes.CharSet = DEFAULT_CHARSET;
	wcscpy_s(fontDes.FaceName, L"Times New Roman");
	fontDes.Height = 14;
	fontDes.Italic = false;
	fontDes.MipLevels = 1;
	fontDes.OutputPrecision = OUT_DEFAULT_PRECIS;
	fontDes.PitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
	fontDes.Quality = DEFAULT_QUALITY;
	fontDes.Weight = 0;
	fontDes.Width = 0;

	result = D3DX10CreateFontIndirect(device, &fontDes, &font);
	if(FAILED(result))
	{
		Trace::Error("Failed to create Font during initialization of Direct X");
		return false;
	}

	device->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

	return true;
}