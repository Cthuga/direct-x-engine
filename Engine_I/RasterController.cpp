#include "RasterController.h"

using namespace Engine_I;

Engine_I::RasterController::RasterController()
{
	device = 0;
	mainTextInterface = 0;
	RSBackFace = 0;
}

Engine_I::RasterController::~RasterController()
{

}

Engine_I::RasterController::RasterController(const RasterController &other)
{

}

RasterController& Engine_I::RasterController::operator=(const RasterController &other)
{
	return RasterController(other);
}

bool Engine_I::RasterController::Init(ID3D10Device *device, MainTextInterface *mainTextInterface)
{
	this->device = device;
	this->mainTextInterface = mainTextInterface;

	if(!InitRSBackFace()) return false;

	return true;
}

void Engine_I::RasterController::ShutDown()
{
	if(RSBackFace)
	{
		RSBackFace->Release();
		RSBackFace = 0;
	}
}

bool Engine_I::RasterController::InitRSBackFace()
{
	HRESULT result;

	D3D10_RASTERIZER_DESC desc;
	ZeroMemory(&desc, sizeof(D3D10_RASTERIZER_DESC));

	desc.AntialiasedLineEnable = false;
	desc.CullMode = D3D10_CULL_BACK;
	desc.DepthBias = 0;
	desc.DepthBiasClamp = 0;
	desc.DepthClipEnable = true;
	desc.FillMode = D3D10_FILL_SOLID;
	desc.FrontCounterClockwise = true;
	desc.MultisampleEnable = false;
	desc.ScissorEnable = false;
	desc.SlopeScaledDepthBias = 0.0f;

	result = device->CreateRasterizerState(&desc, &RSBackFace);
	if(FAILED(result))
	{
		Trace::Error("Failed to create RS Back Face state");
		mainTextInterface->Warning("Failed to create RS Back Face state");
		return false;
	}

	return true;
}