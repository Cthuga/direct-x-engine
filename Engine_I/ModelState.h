#ifndef __MODELSTATE_H_
#define __MODELSTATE_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining custon libraries
#include "ModelDesc.h"
#include "cbPerObjectDesc.h"

namespace Engine_I
{
	//ClassName   : ModelState
	//LifeTime    : Single frame between model updates
	//Description : It is used to cache some information about model ( for performance and some algorithm reasons )
	class ModelState
	{
	public:
		ModelState();

		//Cached CbPerObject structure ( check AbstractEffectDesc.h for reference )
		CbPerObjectDesc* AbstractCBPerObjectDesc;

		//Model description ( check ModelDesc.h for reference )
		ModelDesc ModelDesc;

		//Actual texture 
		ID3D10ShaderResourceView *Texture;
		
	};
};

#endif