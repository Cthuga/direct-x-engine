#include "actor.h"

using namespace Engine_I;

Engine_I::Actor::Actor()
{

}

Engine_I::Actor::~Actor()
{

}

void Engine_I::Actor::Init(Config iConfig)
{
	config = iConfig;

	D3DXMatrixIdentity(&V);

	eye = D3DXVECTOR3(0, 3.0f, -25.0f);
	up = D3DXVECTOR3(0, 1.0f, 0);
	target = D3DXVECTOR3(0, 0.0f,0.0f);

	D3DXMatrixLookAtLH(&V, &eye, &target, &up);
}

void Engine_I::Actor::Action(GameTime gameTime, Input iInput)
{
	input = iInput;
	time = gameTime;
	char val = ToLower(config.configInput.MoveForward)[0];
	
	
	if(GetAsyncKeyState('W') & 0x8000)
	{
		MoveForward();
	}

	if(GetAsyncKeyState('S') & 0x8000)
	{
		MoveBackward();
	}

	if(GetAsyncKeyState('A') & 0x8000)
	{
		StrafeLeft();
	}

	if(GetAsyncKeyState('D') & 0x8000)
	{
		StrafeRight();
	}

	D3DXMatrixLookAtLH(&V, &eye, &target, &up);
}

void Engine_I::Actor::MoveForward()
{
		eye += D3DXVECTOR3(0,0,0.3f);
		target += D3DXVECTOR3(0,0,0.3f);
}

void Engine_I::Actor::MoveBackward()
{
		eye += D3DXVECTOR3( 0, 0, -0.3f);
		target += D3DXVECTOR3(0,0,0.-0.3f);	
}

void Engine_I::Actor::StrafeLeft()
{
		eye += D3DXVECTOR3(-0.3f, 0, 0);
		target += D3DXVECTOR3(-0.3f, 0, 0);
}

void Engine_I::Actor::StrafeRight()
{	
		eye += D3DXVECTOR3(0.3f, 0, 0);
		target += D3DXVECTOR3(0.3f, 0, 0);
}

D3DXMATRIX Engine_I::Actor::GetView()
{
	return V;
}

D3DXVECTOR3 Engine_I::Actor::GetCoordinates()
{
	return eye;
}