#include "World.h"

using namespace Engine_I;

Engine_I::World::World()
{
	globalIndexCounter = 0;
	globalVertexCounter = 0;
	globalFacesCounter = 0;
	globalModelCounter = 0;

	effectController = 0;
	device = 0;
	actualTexture = 0;
}

Engine_I::World::~World()
{

}

bool Engine_I::World::Init(EffectController *effectController, ID3D10Device *device, MainTextInterface *mainTextInterface, StencilController *stencilController)
{
	this->effectController = effectController;
	this->device = device;
	this->mainTextInterface = mainTextInterface;
	this->stencilController = stencilController;

	return true;
}

bool Engine_I::World::UpdateAndRenderModel(AbstractCbPerObjectDesc *effectDescription, string modelName)
{
	if(actualTexture)
	{
		sModels[modelName]->UpdateTexture(actualTexture);
	}

	if(!sModels[modelName]->UpdateAndRender(effectDescription)) return false;

	return true;
}

bool Engine_I::World::RenderWorld()
{
	map<string, Model*>::iterator it;

	for(it = sModels.begin() ; it != sModels.end() ; it++)
	{
		if(!it->second->Render()) return false;
	}
}

void Engine_I::World::ShutDown()
{
	sModels.clear();
}

void Engine_I::World::AddWorld(Model *model, ModelDesc modelDesc, string effectName, string modelName)
{
	model->Init(effectController->GetEffect(effectName), this->device, this->mainTextInterface, modelName, &modelDesc, stencilController);
	
	if(model->Type == "Cube")
	{
		sModels[modelName] = new Cube(*(Cube*)model);
	}
	else if(model->Type == "Grid")
	{
		sModels[modelName] = new Grid(*(Grid*)model);
	}
	else if(model->Type == "Floor")
	{
		sModels[modelName] = new Floor(*(Floor*)model);
	}
	else 
	{
		sModels[modelName] = new Model(*(Model*)model);
	}
	
	globalVertexCounter += model->GetVerticesCount();
	globalIndexCounter += model->GetIndicesCount();
	globalFacesCounter += model->GetNumFaces();
	globalModelCounter++;
}

int Engine_I::World::GetGlobalVertexCounter()
{
	return globalVertexCounter;
}

int Engine_I::World::GetGlobalIndexCounter()
{
	return globalIndexCounter;
}

int Engine_I::World::GetGlobalFacesCounter()
{
	return globalFacesCounter;
}

int Engine_I::World::GetGlobalModelCounter()
{
	return globalModelCounter;
}

void Engine_I::World::UpdatePerFrame(AbstractCbPerFrameDesc *abstractCbPerFrameDesc)
{
	effectController->UpdatePerFrame(abstractCbPerFrameDesc);
}

void Engine_I::World::UpdatePerTexture(ID3D10ShaderResourceView *texture)
{
	actualTexture = texture;

	effectController->UpdatePerTexture(texture);
}

map<string, Model*>* Engine_I::World::GetModels()
{
	return &sModels;
}