#ifndef __DRAWWORLD_H_
#define __DRAWWORLD_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <string>
#include <sstream>

//core
#include "World.h"
#include "MainTextInterface.h"

//effects
#include "EffectController.h"
#include "AbstractEffect.h"
#include "BaseLightEffect.h"
#include "BaseEffect.h"

//models
#include "Grid.h"
#include "Cube.h"
#include "Model.h"
#include "Floor.h"

//descriptions
#include "ModelDesc.h"
#include "cbPerFrameDesc.h"
#include "cbPerObjectDesc.h"
#include "AbstractEffectDesc.h"

//draws
#include "DrawLights.h"

//globals
#include "GameTime.h"

//textures
#include "TextureController.h"
#include "BlendController.h"
#include "AllFixedDescriptions.h"

using namespace std;

namespace Engine_I
{
	//Name		  : DrawWorld
	//Lifetime	  : Initialized during graphic initialization, executed on each frame
	//Description : Main class where all drawing of models take place
	class DrawWorld
	{
	public:
		DrawWorld();
		~DrawWorld();

		//Name		  : Init
		//Description : Initialize all models and input layouts in HLSL. 
		//Input		  : world				- pointer to container of all models in game
		//				mainTextInterface	- pointer to console window
		//			    device				- pointer to device of direct X
		//				blendController		- pointer to all blend effects for engine
		//				drawLights			- pointer to all lights prepared for game
		//Output	  : true when no errors, else false
		bool Init(World *world, MainTextInterface *mainTextInterface, ID3D10Device *device, BlendController *blendController, DrawLights *drawLights);
		
		//Name		  : Frame
		//Description : Update all models for each frame. 
		//Input		  : world				- pointer to container of all models in game
		//				V					- actual V transformation Matrix
		//			    eyePos				- actual camera position
		//				time				- actual game time
		void Frame(D3DXMATRIX V, D3DXMATRIX P, D3DXVECTOR3 eyePos, GameTime time);

		//Name		  : ShutDown
		//Description : Clearing routine
		void ShutDown();

	private:

		//Pointer for container of all models in engine
		World *world;

		//Pointer to console in application
		MainTextInterface *mainTextInterface;

		//Pointer to DirectX Device
		ID3D10Device *device;

		//Pointer to light container
		DrawLights *drawLights;

		//Pointer to all textures from texture dirrectory
		TextureController textureController;

		//Pointer to all blend effects
		BlendController *blendController;

		//Pointer to fixed structures passed to HLSL script files (like fog)
		AllFixedDescriptions allFixedDescriptions;
	};
};

#endif