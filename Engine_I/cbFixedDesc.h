#ifndef __EFFECTFIXEDDESC_H_
#define __EFFECTFIXEDDESC_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D;
#include D3DX;

//Defining system libraries
#include <Windows.h>

//Defining custom libraries
#include "FogDesc.h"
#include "AbstractCbFixedDesc.h"

namespace Engine_I
{
	//StructName  : AbstractEffectDesc
	//Description : Check AbstractEffectDesc for reference
	struct CbFixedDesc : AbstractCbFixedDesc
	{
	public:
		CbFixedDesc();

		//Description of Fog
		FogDesc Fog;
	};
};

#endif