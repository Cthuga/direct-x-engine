#ifndef __TEXTINFOINTERFACE_H_
#define __TEXTINFOINTERFACE_H_

//Defining system libraries
#include <list>
#include <string>
#include <sstream>

//Defining custon libraries
#include "TextInfoInterfaceStruct.h"

namespace Engine_I
{
	//StructName  : TextInfoInterfaceComparator
	//Description : It is responsible for sorting text for each corner of the screen. Sorting routine depends on "priority" parameter
	struct TextInfoInterfaceComparator 
	{
		 bool operator()(const TextInfoInterfaceStruct& first, const TextInfoInterfaceStruct& second) const;
	};

	//ClassName   : TextInfoInterface
	//LifeTime    : Initialized during graphic initialization, executed on each frame
	//Description : It controls rendering text for each corner of the screen
	class TextInfoInterface
	{
	public:
		TextInfoInterface();
		~TextInfoInterface();

		//Name		  : AddTopLeft
		//Description : Adds new text for top left corner of the screen
		//Input		  : structure - description of text which would be added
		void AddTopLeft(TextInfoInterfaceStruct structure);

		//Name		  : AddTopRight
		//Description : Adds new text for top right corner of the screen
		//Input		  : structure - description of text which would be added
		void AddTopRight(TextInfoInterfaceStruct structure);

		//Name		  : AddBottomRight
		//Description : Adds new text for bottom right corner of the screen
		//Input		  : structure - description of text which would be added
		void AddBottomRight(TextInfoInterfaceStruct structure);

		//Name		  : AddBottomLeft
		//Description : Adds new text for bottom left corner of the screen
		//Input		  : structure - description of text which would be added
		void AddBottomLeft(TextInfoInterfaceStruct structure);

		//Name		  : Init
		//Description : Initialization of Text Info Interface
		//Input		  : fontInterface - Direct X font interface
		//				width		  - width of the screen
		//				height		  - height of the screen
		//Output	  : true when no errors, else false
		bool Init(ID3DX10Font* fontInterface, int width, int height);

		//Name		  : ShutDown
		//Description : ShutDown routine
		void ShutDown();

		//Name		  : DrawInterface
		//Description : Executed once for each frame. It is response for rendering routine
		void DrawInterface();

		//Name		  : Clear
		//Description : Clear all text containers.
		void Clear();
	
	private:

		//EnumName    : Dirrection
		//LifeTime    : Helper enum used by rendering routine to determine location of the text ( used by RenderList helper method )
		enum Dirrection
		{
			TopLeft,
			TopRight,
			BottomLeft,
			BottomRight
		};

		//Name		  : RenderList
		//Description : Used by "DrawInterface: method to render concrete collection of text ( for specified dirrection )
		//Input		  : renderList	  - topLeftList, topRightList, bottomLeftList or bottomRightList 
		//				dirrection	  - corner of the screen used for rendering text
		void RenderList(list<TextInfoInterfaceStruct> renderList, Dirrection dirrection);

		//Text collections ( check TextInfoInterfaceStruct.h for reference )
		list<TextInfoInterfaceStruct> topLeftList;		//top left corner of the screen
		list<TextInfoInterfaceStruct> topRightList;		//top right corner of the screen
		list<TextInfoInterfaceStruct> bottomLeftList;	//bottom left corner of the screen
		list<TextInfoInterfaceStruct> bottomRightList;	//bottom right corner of the screen
		list<TextInfoInterfaceStruct> mainList;			//chat(console) - check MainTextInterface.h for reference

		//Pointer to Direct X font interface
		ID3DX10Font* font;

		//Actual width of the screen field
		int sceneWidth;

		//Actual height of the screen field
		int sceneHeight;
	};
};

#endif