#include "Cube.h"

using namespace Engine_I;

Engine_I::Cube::Cube() : Model()
{
	Type = "Cube";
	faultedState = 0;
}

Engine_I::Cube::~Cube()
{

}

bool Engine_I::Cube::Init(AbstractEffect *effect, ID3D10Device *&device, MainTextInterface *&mainTextInterface, string name, ModelDesc *desc, StencilController *stencilController)
{
	faultedState = 0;
	Model::Init(effect, device, mainTextInterface, name, desc, stencilController);

	numIndices = 3*2*6;
	numVertices = 24;
	numFaces = 12;

	float pwidth = (float)desc->Width/2;
	float pheight = (float)desc->Height/2;
	float plong = (float)desc->Long/2;

	strideLength = sizeof(TexVertex);

	D3DXVECTOR3 v1 = D3DXVECTOR3(1,-1,-1);
	D3DXVECTOR3 v2 = D3DXVECTOR3(-1,-1,-1);
	D3DXVECTOR3 v3 = D3DXVECTOR3(-1,-1,1);
	D3DXVECTOR3 v4 = D3DXVECTOR3(1,-1,1);
	D3DXVECTOR3 v5 = D3DXVECTOR3(1,1,-1);
	D3DXVECTOR3 v6 = D3DXVECTOR3(-1,1,-1);
	D3DXVECTOR3 v7 = D3DXVECTOR3(-1,1,1);
	D3DXVECTOR3 v8 = D3DXVECTOR3(1,1,1);

	D3DXVECTOR3 v1N = D3DXVECTOR3(0,0,0);
	D3DXVECTOR3 v2N = D3DXVECTOR3(0,0,0);
	D3DXVECTOR3 v3N = D3DXVECTOR3(0,0,0);
	D3DXVECTOR3 v4N = D3DXVECTOR3(0,0,0);
	D3DXVECTOR3 v5N = D3DXVECTOR3(0,0,0);
	D3DXVECTOR3 v6N = D3DXVECTOR3(0,0,0);
	D3DXVECTOR3 v7N = D3DXVECTOR3(0,0,0);
	D3DXVECTOR3 v8N = D3DXVECTOR3(0,0,0);

	D3DXVec3Normalize(&v1N, &v1);
	D3DXVec3Normalize(&v2N, &v2);
	D3DXVec3Normalize(&v3N, &v3);
	D3DXVec3Normalize(&v4N, &v4);
	D3DXVec3Normalize(&v5N, &v5);
	D3DXVec3Normalize(&v6N, &v6);
	D3DXVec3Normalize(&v7N, &v7);
	D3DXVec3Normalize(&v8N, &v8);

	TexVertex vertices[24];
		
	//front wall
	vertices[0] = TexVertex(D3DXVECTOR3(pwidth, -pheight ,-plong), v1N, desc->SpecPower, D3DXVECTOR2(1/desc->TexXScale,1/desc->TexYScale)); 
	vertices[1] = TexVertex(D3DXVECTOR3(-pwidth, -pheight,-plong), v2N, desc->SpecPower, D3DXVECTOR2(0,1/desc->TexYScale)); 
	vertices[2] = TexVertex(D3DXVECTOR3(-pwidth, pheight, -plong), v6N, desc->SpecPower, D3DXVECTOR2(0,0)); 
	vertices[3] = TexVertex(D3DXVECTOR3(pwidth, pheight,  -plong), v5N, desc->SpecPower, D3DXVECTOR2(1/desc->TexXScale,0));  

	//back wall
	vertices[4] = TexVertex(D3DXVECTOR3(pwidth, -pheight,  plong), v4N, desc->SpecPower, D3DXVECTOR2(0,1/desc->TexYScale));
	vertices[5] = TexVertex(D3DXVECTOR3(-pwidth, -pheight, plong), v3N, desc->SpecPower, D3DXVECTOR2(1/desc->TexXScale,1/desc->TexYScale));
	vertices[6] = TexVertex(D3DXVECTOR3(-pwidth,pheight, plong), v7N, desc->SpecPower, D3DXVECTOR2(1/desc->TexXScale,0));
	vertices[7] = TexVertex(D3DXVECTOR3(pwidth, pheight, plong), v8N, desc->SpecPower, D3DXVECTOR2(0,0));

	//left wall
	vertices[8] = TexVertex(D3DXVECTOR3(-pwidth, -pheight,-plong), v2N, desc->SpecPower, D3DXVECTOR2(1/desc->TexXScale,1/desc->TexYScale)); 
	vertices[9] = TexVertex(D3DXVECTOR3(-pwidth, -pheight, plong), v3N, desc->SpecPower, D3DXVECTOR2(0,1/desc->TexYScale));
	vertices[10] = TexVertex(D3DXVECTOR3(-pwidth,pheight, plong), v7N, desc->SpecPower, D3DXVECTOR2(0,0));
	vertices[11] = TexVertex(D3DXVECTOR3(-pwidth, pheight, -plong), v6N, desc->SpecPower, D3DXVECTOR2(1/desc->TexXScale,0)); 

	//right wall
	vertices[12] = TexVertex(D3DXVECTOR3(pwidth, -pheight,  plong), v4N, desc->SpecPower, D3DXVECTOR2(0,1/desc->TexYScale));
	vertices[13] = TexVertex(D3DXVECTOR3(pwidth, -pheight ,-plong), v1N, desc->SpecPower, D3DXVECTOR2(1/desc->TexXScale,1/desc->TexYScale)); 
	vertices[14] = TexVertex(D3DXVECTOR3(pwidth, pheight,  -plong), v5N, desc->SpecPower, D3DXVECTOR2(1/desc->TexXScale,0));  
	vertices[15] = TexVertex(D3DXVECTOR3(pwidth, pheight, plong), v8N, desc->SpecPower, D3DXVECTOR2(0,0));

	//down wall
	vertices[16] = TexVertex(D3DXVECTOR3(pwidth, -pheight ,-plong), v1N, desc->SpecPower, D3DXVECTOR2(1/desc->TexXScale,1/desc->TexYScale)); 
	vertices[17] = TexVertex(D3DXVECTOR3(-pwidth, -pheight,-plong), v2N, desc->SpecPower, D3DXVECTOR2(0,1/desc->TexYScale)); 
	vertices[18] = TexVertex(D3DXVECTOR3(-pwidth, -pheight, plong), v3N, desc->SpecPower, D3DXVECTOR2(0,0));
	vertices[19] = TexVertex(D3DXVECTOR3(pwidth, -pheight,  plong), v4N, desc->SpecPower, D3DXVECTOR2(1/desc->TexXScale,0));

	//up wall
	vertices[20] = TexVertex(D3DXVECTOR3(pwidth, pheight,  -plong), v5N, desc->SpecPower, D3DXVECTOR2(0,1/desc->TexYScale));  
	vertices[21] = TexVertex(D3DXVECTOR3(-pwidth, pheight, -plong), v6N, desc->SpecPower, D3DXVECTOR2(1/desc->TexXScale,1/desc->TexYScale)); 
	vertices[22] = TexVertex(D3DXVECTOR3(-pwidth,pheight, plong), v7N, desc->SpecPower, D3DXVECTOR2(1/desc->TexXScale,0));
	vertices[23] = TexVertex(D3DXVECTOR3(pwidth, pheight, plong), v8N, desc->SpecPower, D3DXVECTOR2(0,0));

	int indices[] =
	{
		//front wall
		0,1,2,		0,2,3,

		//back wall
		6,5,4,		6,4,7,

		//left wall
		11,8,9,		11,9,10,

		//right wall
		12,13,14,	12,14,15,

		//down wall
		19,18,17,	19,18,16,

		//up wall
		20,21,22,	20,22,23
	};

	return CreateNewBuffers(vertices, indices, sizeof(TexVertex));
}

bool Engine_I::Cube::Render()
{
	if(!Model::Render()) return false;
}

void Engine_I::Cube::ShutDown()
{
	Model::ShutDown();
}

