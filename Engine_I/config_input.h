#ifndef __CONFIG_INPUT_H_
#define __CONFIG_INPUT_H_

//Defining system libraries
#include <string>

using namespace std;

namespace Engine_I
{
	//ClassName   : ConfigInput
	//LifeTime    : As long as "engine_ini.h" lifetime. 
	//Description : It stores all values from "*.ini" configuration file - Input Section. For some more info check "engine_ini.h" and "config.h" reference.
	//				Engine still doesn't use these values. It is for future versions
	class ConfigInput
	{
	public:
		ConfigInput();
		~ConfigInput();

		//Some keys used in engine:
		string MoveForward;		//Move forward key
		string MoveBackward;	//Move backward key
		string StrafeLeft;		//Strafe left key
		string StrafeRight;		//Strafe right key
		string Quit;			//Key used to quit application
		string Pause;			//Key used to pause application
	};
};

#endif