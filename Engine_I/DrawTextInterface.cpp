#include "DrawTextInterface.h"

using namespace Engine_I;

Engine_I::DrawTextInterface::DrawTextInterface()
{

}

Engine_I::DrawTextInterface::~DrawTextInterface()
{

}

void Engine_I::DrawTextInterface::BeginScene(GameTime gameTime, D3DXVECTOR3 V, int verCnt, int indCnt, int facCnt, int modCnt)
{
	time = gameTime;

	TextInfoInterfaceStruct headerInfo;
	ZeroMemory(&headerInfo, sizeof(headerInfo));

	TextInfoInterfaceStruct secondInfo;
	ZeroMemory(&secondInfo, sizeof(secondInfo));

	TextInfoInterfaceStruct fpsInfo;
	ZeroMemory(&fpsInfo, sizeof(fpsInfo));

	TextInfoInterfaceStruct gameHeaderInfo;
	ZeroMemory(&gameHeaderInfo, sizeof(gameHeaderInfo));

	TextInfoInterfaceStruct gameHeaderCoordInfo;
	ZeroMemory(&gameHeaderCoordInfo, sizeof(gameHeaderCoordInfo));

	TextInfoInterfaceStruct coordInfo;
	ZeroMemory(&coordInfo, sizeof(coordInfo));

	TextInfoInterfaceStruct vertexCountInfo;
	ZeroMemory(&vertexCountInfo, sizeof(vertexCountInfo));

	TextInfoInterfaceStruct indexCountInfo;
	ZeroMemory(&indexCountInfo, sizeof(indexCountInfo));

	TextInfoInterfaceStruct facesCounterInfo;
	ZeroMemory(&facesCounterInfo, sizeof(facesCounterInfo));

	TextInfoInterfaceStruct modelCounterInfo;
	ZeroMemory(&modelCounterInfo, sizeof(modelCounterInfo));

	ostringstream FPSStream;

	textInterface.Clear();

	if(time.Ms % 250 == 0)
	{
		FPSStream <<  time.FPS;
		FPS = FPSStream.str();
	}	

	FPSStream.str("");
	FPSStream << verCnt;
	
	StructHelper("Vertices count", FPSStream.str(), &vertexCountInfo);
	vertexCountInfo.Priority = 3;
	vertexCountInfo.Color = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	textInterface.AddBottomLeft(vertexCountInfo);

	FPSStream.str("");
	FPSStream << indCnt;
	
	StructHelper("Indices count", FPSStream.str(), &indexCountInfo);
	indexCountInfo.Priority = 4;
	indexCountInfo.Color = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	textInterface.AddBottomLeft(indexCountInfo);

	FPSStream.str("");
	FPSStream << facCnt;
	
	StructHelper("Faces count", FPSStream.str(), &facesCounterInfo);
	facesCounterInfo.Priority = 5;
	facesCounterInfo.Color = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	textInterface.AddBottomLeft(facesCounterInfo);

	FPSStream.str("");
	FPSStream << modCnt;
	
	StructHelper("Model count", FPSStream.str(), &modelCounterInfo);
	modelCounterInfo.Priority = 6;
	modelCounterInfo.Color = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	textInterface.AddBottomLeft(modelCounterInfo);

	StructHelper("FPS", FPS, &fpsInfo);
	fpsInfo.Priority = 2;
	fpsInfo.Color = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	textInterface.AddBottomLeft(fpsInfo);

	FPSStream.str("");
	FPSStream << gameTime.S;

	StructHelper("Seconds elapsed", FPSStream.str(), &fpsInfo);
	fpsInfo.Priority = 3;
	fpsInfo.Color = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	textInterface.AddBottomLeft(fpsInfo);

	StructHelper("-- Performance statistics --", "", &headerInfo);
	headerInfo.Priority = 1;
	headerInfo.Color = D3DXCOLOR(0, 0.8f, 0, 1.0f);
	textInterface.AddBottomLeft(headerInfo);

	StructHelper("-- Game Info --","", &gameHeaderInfo);
	gameHeaderInfo.Priority = 1;
	gameHeaderInfo.Color = D3DXCOLOR(0, 0.8f, 0, 1.0f);
	textInterface.AddTopLeft(gameHeaderInfo);

	StructHelper("Coordinates:","", &gameHeaderCoordInfo);
	gameHeaderCoordInfo.Priority = 2;
	gameHeaderCoordInfo.Color = D3DXCOLOR(0.8f, 0, 0, 1.0f);
	textInterface.AddTopLeft(gameHeaderCoordInfo);

	FPSStream.str("");
	FPSStream << "X : " << (int)floor(V.x) << " Y : " << (int)floor(V.y) << " Z : " << (int)floor(V.z);
	StructHelper(FPSStream.str(),"", &coordInfo);
	coordInfo.Priority = 3;
	coordInfo.Color = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	textInterface.AddTopLeft(coordInfo);
}

void Engine_I::DrawTextInterface::StructHelper(string parameter, string value, TextInfoInterfaceStruct* out)
{
	wostringstream FPSStream;

	wstring flushString;
	wstring wParameter(parameter.begin(), parameter.end());
	wstring wValue(value.begin(), value.end());

	FPSStream.clear();
	FPSStream.str(flushString);
	FPSStream << wParameter;

	out->Parameter = FPSStream.str();

	FPSStream.clear();
	FPSStream.str(flushString);
	FPSStream << wValue;

	out->Value = FPSStream.str();
}

void Engine_I::DrawTextInterface::RenderWorld()
{
	textInterface.DrawInterface();
}

void Engine_I::DrawTextInterface::RenderSprites()
{

}

void Engine_I::DrawTextInterface::EndScene()
{

}

bool Engine_I::DrawTextInterface::Init(ID3DX10Font* fontInterface, int width, int height)
{
	textInterface.Init(fontInterface, width, height);

	return true;
}

void Engine_I::DrawTextInterface::ShutDown()
{
	textInterface.ShutDown();
}