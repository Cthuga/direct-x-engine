#ifndef __ABSTRACTVERTEX_H_
#define __ABSTRACTVERTEX_H_

namespace Engine_I
{
	//StructName  :  AbstractVertex
	//Description :  Vertex in Direct X might have customized structure. This structure is passed to HLSL file for futher transformation. That's why
	//				 there is used derivation for Vertex structures. AbstractVertex is base structure for all futher Vertex Structures.
	struct AbstractVertex
	{

	};
};

#endif