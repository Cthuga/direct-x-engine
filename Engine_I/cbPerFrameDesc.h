#ifndef __BASELIGHTEFFECTDESC_H_
#define __BASELIGHTEFFECTDESC_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <Windows.h>

//Defining custom libraries
#include "AbstractCbPerFrameDesc.h"
#include "LightDesc.h"

namespace Engine_I
{
	//StructName  : CbPerFrameDesc
	//Description : Check AbstractEffectDesc for reference
	struct CbPerFrameDesc : AbstractCbPerFrameDesc
	{
	public:
		CbPerFrameDesc();

		//Description of all lights used for this game
		LightDesc Lights[256];

		//Actual camera position ( used for example to calculate some light special effects )
		D3DXVECTOR3 EyePos;

		//Number of lights - used for performance reasons
		int LightNum;
	};
};

#endif