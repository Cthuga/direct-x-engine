#include "TextInfoInterface.h"

using namespace Engine_I;

Engine_I::TextInfoInterface::TextInfoInterface()
{

}

Engine_I::TextInfoInterface::~TextInfoInterface()
{

}

void Engine_I::TextInfoInterface::AddTopLeft(TextInfoInterfaceStruct structure)
{
	topLeftList.push_back(structure);
}

void Engine_I::TextInfoInterface::AddTopRight(TextInfoInterfaceStruct structure)
{
	topRightList.push_back(structure);
}

void Engine_I::TextInfoInterface::AddBottomRight(TextInfoInterfaceStruct structure)
{
	bottomRightList.push_back(structure);
}

void Engine_I::TextInfoInterface::AddBottomLeft(TextInfoInterfaceStruct structure)
{
	bottomLeftList.push_back(structure);
}

void Engine_I::TextInfoInterface::DrawInterface()
{
	bottomLeftList.sort(TextInfoInterfaceComparator());
	bottomRightList.sort(TextInfoInterfaceComparator());
	topLeftList.sort();
	topRightList.sort();
	
	RenderList(bottomLeftList, BottomLeft);
	RenderList(bottomRightList, BottomRight);
	RenderList(topLeftList, TopLeft);
	RenderList(topRightList, TopRight);
}

bool Engine_I::TextInfoInterfaceComparator::operator()(const TextInfoInterfaceStruct& first, const TextInfoInterfaceStruct& second) const
{
	return first > second;
}

void Engine_I::TextInfoInterface::RenderList(list<TextInfoInterfaceStruct> renderList, Dirrection dirrection)
{
	wstring flushString;
	wostringstream writerStream;
	wstring result;

	list<TextInfoInterfaceStruct>::iterator iterator;

	int counter = 0;

	for(iterator = renderList.begin(); iterator != renderList.end()  ; iterator++)
	{
		counter++;

		writerStream.clear();
		writerStream.str(flushString);

		if(iterator->Value == flushString)
		{
			writerStream << iterator->Parameter;
		}
		else
		{
			writerStream << iterator->Parameter << " : " << iterator->Value;
		}

		result = writerStream.str();

		RECT fpsRect1 = { 5, sceneHeight - 15 * counter, 0, 0 };
		RECT fpsRect2 = { sceneWidth - 140, sceneHeight - 15 * counter, 0, 0 };
		RECT fpsRect3 = { 5, 5 + 15 * (counter - 1 ), 0, 0 };
		RECT fpsRect4 = { sceneWidth - 140, 5 + 15 * (counter - 1), 0, 0 };

		switch(dirrection)
		{
			case BottomLeft:			
				font->DrawText(0, result.c_str() , -1, &fpsRect1,  DT_NOCLIP, iterator->Color);
				break;

			case BottomRight:			
				font->DrawText(0, result.c_str() , -1, &fpsRect2,  DT_NOCLIP, iterator->Color);
				break;

			case TopLeft:				
				font->DrawText(0, result.c_str() , -1, &fpsRect3,  DT_NOCLIP, iterator->Color);
				break;

			case TopRight:			
				font->DrawText(0, result.c_str() , -1, &fpsRect4,  DT_NOCLIP, iterator->Color);
				break;
		}
	}
}

void Engine_I::TextInfoInterface::Clear()
{
	topLeftList.clear();
	topRightList.clear();
	bottomLeftList.clear();
	bottomRightList.clear();
}

bool Engine_I::TextInfoInterface::Init(ID3DX10Font* fontInterface, int width, int height)
{
	font = fontInterface;
	sceneWidth = width;
	sceneHeight = height;

	return true;
}

void Engine_I::TextInfoInterface::ShutDown()
{
	if(font)
	{
		font->Release();
		font = 0;
	}
}