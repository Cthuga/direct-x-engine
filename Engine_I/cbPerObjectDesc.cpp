#include "cbPerObjectDesc.h"

using namespace Engine_I;

Engine_I::CbPerObjectDesc::CbPerObjectDesc()
{
	ZeroMemory(this, sizeof(CbPerObjectDesc));

	D3DXMatrixIdentity(&W);
	D3DXMatrixIdentity(&WVP);

	texAdressingType = -1;
	Clip = false;
	Fog = 1;
}

Engine_I::CbPerObjectDesc::CbPerObjectDesc(D3DXMATRIX WVP)
{
	ZeroMemory(this, sizeof(CbPerObjectDesc));

	this->WVP = WVP;
}
