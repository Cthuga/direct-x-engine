#ifndef __GRAPHIC_H_
#define __GRAPHIC_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <string>
#include <sstream>
#include <memory>

//Defining custon libraries
#include "d3xcore.h"
#include "trace.h"
#include "engine_ini.h"
#include "GameTime.h"
#include "DrawTextInterface.h"
#include "MainTextInterface.h"
#include "World.h"
#include "input.h"
#include "actor.h"
#include "DrawWorld.h"
#include "TextureController.h"
#include "BlendController.h"
#include "DrawConfiguration.h"
#include "MirrorController.h"
#include "StencilController.h"
#include "RasterController.h"

namespace Engine_I
{
	//ClassName   : Graphic
	//LifeTime    : Initialized during initialization of WinMain, deleted during closing application
	//Description : This is real "Main" class of Application. When window is created for application, whole runtime is delegated to Graphic processing.
	//				Whole process would be described as:
	//
	//				  [Stage 1]
	//				Initialization ( during this stage all other objects execute their "Init" method. This is simple preparing instances for future use )
	//					||
	//					\/
	//				  [Stage 2]
	//				  Frame ( on this stage there is prepared picture for rendering ) It was separated for 3 stages :
	//						-- BeginScene ( clearing routine, preparing default values )
	//						-- RenderWorld and RenderSprites ( rendering models and sprites in scene )
	//						-- EndScene ( switching to next frame )
	//
	//				  This stage is looped till application closing
	//				    ||
	//					\/
	//				  [Stage 3]
	//				  Shutdown ( here all initialized objects would execute their "Shutdown" method. This is clearing routine during closing application )
	//
	class Graphic
	{
	public:
		Graphic();
		~Graphic();

		//Name		  : InitCore
		//Description : Executed on [Stage 1]
		//				This method initialize some matrix variables. 
		//				TODO: It should be moved to Init method 
		//Output	  : true when no errors, else false
		bool InitCore();

		//Name		  : Init
		//Description : Executed on [Stage 1] ( well, this is stage 1 )
		//				Initialization of "everything". 
		//Input		  : iEngine - this is whole configuration passed from *.ini file. Check "engine_ini.h" for reference.
		//				ihwnd   - handle of window. Used during Direct X initialization
		//Output	  : true when no errors, else false
		bool Init(EngineIni iEngine, HWND ihwnd);

		//Name		  : Frame
		//Description : Executed on [Stage 2]
		//				This is Frame from [Stage 2]. The only thing this method do is executing in order : BeginScene, RenderWorld, RenderSprites, EndScene
		//Input		  : gameTime - actual game time that is used for some logic
		//				input	 - actual key pressed 
		void Frame(GameTime gameTime, Input input);

		//Name		  : BeginScene
		//Description : Executed on [Stage 2]
		//				Clearing routine, preparing default values 
		void BeginScene();

		//Name		  : RenderWorld
		//Description : Executed on [Stage 2]
		//				Rendering models
		void RenderWorld();

		//Name		  : RenderSprites
		//Description : Executed on [Stage 2]
		//				Rendering sprites ( actually not really used )
		void RenderSprites();

		//Name		  : EndScene
		//Description : Executed on [Stage 2]
		//				Switching to next frame
		void EndScene();
	
		//Name		  : ShutDown
		//Description : Executed on [Stage 3]
		//				Here all initialized objects would execute their "Shutdown" method. This is clearing routine during closing application
		void ShutDown();

		//Name		  : Resize
		//Description : During resizing window event handling this method is executed to redresh Direct X properties.
		void Resize();

		//Actual width of the screen
		int width;

		//Actual height of the screen
		int height;

		//Flag set when in Full Screen mode
		bool FullScreen;

	private:

		//Description of graphic state:
		HWND hwnd;		//handler of window on which Direct X Buffer is bound
		bool initFrame; //Flag set when first switch of frame take place. Some activities might be executed only during first frame

		//Description of DirectX objects:
		ID3D10Device* device;						//Pointer to direct X Device - the core of Direct X
		IDXGISwapChain* swapChain;					//Pointer to Swap chain generated during initialization of D3XCore ( check D3XCore.h for reference ). Swap Chain describes back buffers for rendering graphic
		ID3D10RenderTargetView* renderTargetView;	//Pointer to RenderTargetView generated during initialization of D3XCore ( check D3XCore.h for reference ). Render Target View describes actually generated picture for Back Buffer
		ID3D10DepthStencilView* depthStencilView;	//Pointer to DepthStencilView generated during initialization of D3XCore ( check D3XCore.h for reference ). Depth view Buffer is responsible for rendering order of objects. Stencil View is responsible for some special effects ( check StencilController.h for reference )
		ID3DX10Font* font;							//Pointer to ID3DX10Font generated during initialization of D3XCore ( check D3XCore.h for reference ). This object is responsible for rendering fonts on the screen

		//Core objects
		D3XCore d3XCore;	//This is Direct X controller. Check D3XCore.h for reference
		EngineIni engine;	//This is configuration object. It maps values from *.ini file. Check engine_ini.h for reference
		GameTime time;		//Here is stored actual game time. Check GameTime.h and timer.h for reference
		Input input;		//Here are registered all pressing and releasing keys from input. Actually not really used. Check input.h for reference
		World world;		//Container of all models ( check World.h for reference ). Also place where communication with GPU take place (check EffectController.h for reference )
	
		//Base transformation matrices and positions
		D3DXMATRIX P;		//actual camera perspective transformation matrix
		D3DXMATRIX V;		//actual position of camera (View) transformation matrix
		D3DXMATRIX W;		//actual base transformation matrix (World)
		D3DXVECTOR3 eye;	//actual position of camera
		D3DXVECTOR3 pAt;	//actual point, on which camera looks	
		Actor actor;		//object representing camera ( check actor.h for reference )

		//Properties implementing game logic
		DrawWorld drawWorld;					//here is description and position of all models in game ( check DrawWorld.h and Model.h for reference )
		DrawConfiguration drawConfiguration;	//here is description of all fixed ellements of game ( like Fog ) ( check DrawConfiguration.h, AbstractVertex.h and cbFixedDesc.h for reference )
		DrawLights drawLights;					//here is description and position [ important: at this momment position is decribed in DrawWorld.h, that's wrong and it would be fixed ] of all lights ( check DrawLights.h for reference [ at this mommend also DrawWorld.h, but as I mentioned that's wrong ] )
		DrawTextInterface textInterface;		//here are writtend and described all fonts and texts in game ( check "DrawTextInterface.h" for reference )
		
		//Engine functionalities ( special effects, logic prepared for engine )
		MainTextInterface mainTextInterface;	//This controller is responsible for text rendered in console ( chat ) window ( check MainTextInterface.h for reference )
		BlendController blendController;		//This controller is container of all blend effects prepared for engine ( check BlendController.h for reference )
		EffectController effectController;		//This controller manage all HLSL script files and helps to communicate with them ( for changing values )(GPU<-->CPU communication). The real communication is passed to "World" class. Check "World.h" for description of communication. Some interesting material is also in "RasterController.h" and "EffectController.h"
		MirrorController mirrorController;		//This controller manage mirror effect for models that set Mirror=true on ModelDesc ( check MirrorController.h, ModelDesc.h, Model.h for reference )
		StencilController stencilController;	//This controller is container of all stencil effects prepared for engien ( check StencilController.h for reference )
		RasterController rasterController;		//This controller is container of all raster effects prepared for engine ( check RasterController.h for reference )
	};
};

#endif