#include "cbPerFrameDesc.h"

using namespace Engine_I;

Engine_I::CbPerFrameDesc::CbPerFrameDesc() 
{
	ZeroMemory(this, sizeof(this));

	LightDesc l;
	l.Ambient = D3DXCOLOR(0,0,0,0);
	l.Attenuation = D3DXVECTOR3(0,0,0);
	l.Diffuse = D3DXCOLOR(0,0,0,0);
	l.Dirrection = D3DXVECTOR3(0,0,0);
	l.Position = D3DXVECTOR3(0,0,0);
	l.Range = 0.0f;
	l.Specular = D3DXCOLOR(0,0,0,0);
	l.SpotPower = 0.0f;
	l.Type = 0;

	for(int i = 0; i < 256 ; i++)
	{
		Lights[i] = LightDesc(l);
	}

	LightNum = 0;
	
	EyePos = D3DXVECTOR3(0,0,0);
}