#include "TexVertex.h"

using namespace Engine_I;

Engine_I::TexVertex::TexVertex()
{

}

Engine_I::TexVertex::TexVertex(D3DXVECTOR3 position, D3DXVECTOR3 normal,  D3DXCOLOR specPow, D3DXVECTOR2 texels) : AbstractVertex()
{
	this->position = position;
	this->normal = normal;
	this->texels = texels;
	this->specPow = specPow;
}