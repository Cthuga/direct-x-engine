#include "StencilController.h"

using namespace Engine_I;

Engine_I::StencilController::StencilController()
{
	stateWriteToExistingStencilOnly = 0;
	stateCreateNewStencil = 0;
	device = 0;
	mainTextInterface = 0;
	rasterController = 0;
}

Engine_I::StencilController::~StencilController()
{

}

Engine_I::StencilController::StencilController(const StencilController& other)
{

}

StencilController& Engine_I::StencilController::operator=(const StencilController& other)
{
	return StencilController(other);
}

bool Engine_I::StencilController::Init(ID3D10Device *device, MainTextInterface *MainTextInterface, RasterController *rasterController)
{
	this->device = device;
	this->mainTextInterface = mainTextInterface;
	this->rasterController = rasterController;

	if(!StateCreateNewStencil()) return false;
	if(!StateWriteToExistingStencilOnly()) return false;

	return true;
}

void Engine_I::StencilController::ShutDown()
{
	if(mainTextInterface)
	{
		delete mainTextInterface;
		mainTextInterface = 0;
	}

	if(stateCreateNewStencil)
	{
		stateCreateNewStencil->Release();
		stateCreateNewStencil = 0;
	}

	if(stateWriteToExistingStencilOnly)
	{
		stateWriteToExistingStencilOnly->Release();
		stateWriteToExistingStencilOnly = 0;
	}
}

bool Engine_I::StencilController::StateCreateNewStencil()
{
	HRESULT result;

	D3D10_DEPTH_STENCIL_DESC desc;
	desc.DepthEnable = true;
	desc.DepthWriteMask = D3D10_DEPTH_WRITE_MASK_ALL;
	desc.DepthFunc = D3D10_COMPARISON_LESS;
	desc.StencilEnable = true;
	desc.StencilReadMask = 0xff;
	desc.StencilWriteMask = 0xff;
	
	desc.FrontFace.StencilFunc = D3D10_COMPARISON_ALWAYS;
	desc.FrontFace.StencilPassOp = D3D10_STENCIL_OP_REPLACE;
	desc.FrontFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	desc.FrontFace.StencilDepthFailOp = D3D10_STENCIL_OP_KEEP;

	desc.BackFace.StencilFunc = D3D10_COMPARISON_ALWAYS;
	desc.BackFace.StencilPassOp = D3D10_STENCIL_OP_REPLACE;
	desc.BackFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	desc.BackFace.StencilDepthFailOp = D3D10_STENCIL_OP_KEEP;

	result = device->CreateDepthStencilState(&desc, &stateCreateNewStencil);
	if(FAILED(result))
	{
		mainTextInterface->Warning("Failed to create stencil effect : Write To Stencil");
		Trace::Error("Failed to create stencil effect : Write To Stencil");
		return false;
	}

	return true;
}

bool Engine_I::StencilController::StateWriteToExistingStencilOnly()
{
	HRESULT result;

	D3D10_DEPTH_STENCIL_DESC desc;
	desc.DepthEnable = true;
	desc.DepthWriteMask = D3D10_DEPTH_WRITE_MASK_ALL;
	desc.DepthFunc = D3D10_COMPARISON_LESS;
	desc.StencilEnable = true;
	desc.StencilReadMask = 0xff;
	desc.StencilWriteMask = 0xff;
	
	desc.FrontFace.StencilFunc = D3D10_COMPARISON_EQUAL;
	desc.FrontFace.StencilPassOp = D3D10_STENCIL_OP_KEEP;
	desc.FrontFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	desc.FrontFace.StencilDepthFailOp = D3D10_STENCIL_OP_KEEP;

	desc.BackFace.StencilFunc = D3D10_COMPARISON_EQUAL;
	desc.BackFace.StencilPassOp = D3D10_STENCIL_OP_KEEP;
	desc.BackFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	desc.BackFace.StencilDepthFailOp = D3D10_STENCIL_OP_KEEP;

	result = device->CreateDepthStencilState(&desc, &stateWriteToExistingStencilOnly);
	if(FAILED(result))
	{
		mainTextInterface->Warning("Failed to create stencil effect : Write To Stencil");
		Trace::Error("Failed to create stencil effect : Write To Stencil");
		return false;
	}

	return true;
}

bool Engine_I::StencilController::CreateMirror()
{
	HRESULT result;

	D3D10_DEPTH_STENCIL_DESC desc;
	desc.DepthEnable = true;
	desc.DepthWriteMask = D3D10_DEPTH_WRITE_MASK_ZERO;
	desc.DepthFunc = D3D10_COMPARISON_ALWAYS;
	desc.StencilEnable = false;
	desc.StencilReadMask = 0xff;
	desc.StencilWriteMask = 0xff;
	
	desc.FrontFace.StencilFunc = D3D10_COMPARISON_EQUAL;
	desc.FrontFace.StencilPassOp = D3D10_STENCIL_OP_KEEP;
	desc.FrontFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	desc.FrontFace.StencilDepthFailOp = D3D10_STENCIL_OP_KEEP;

	desc.BackFace.StencilFunc = D3D10_COMPARISON_EQUAL;
	desc.BackFace.StencilPassOp = D3D10_STENCIL_OP_KEEP;
	desc.BackFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	desc.BackFace.StencilDepthFailOp = D3D10_STENCIL_OP_KEEP;

	result = device->CreateDepthStencilState(&desc, &stateCreateMirror);
	if(FAILED(result))
	{
		mainTextInterface->Warning("Failed to create stencil effect : Write To Stencil");
		Trace::Error("Failed to create stencil effect : Write To Stencil");
		return false;
	}

	return true;
}