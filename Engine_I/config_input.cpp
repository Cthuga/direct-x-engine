#include "config_input.h"

using namespace Engine_I;

Engine_I::ConfigInput::ConfigInput()
{
	MoveForward = "W";
	MoveBackward = "S";
	StrafeLeft = "A";
	StrafeRight = "D";
	Quit = "Esc";
	Pause = "P";
}

Engine_I::ConfigInput::~ConfigInput()
{

}