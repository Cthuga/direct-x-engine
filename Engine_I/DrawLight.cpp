#include "DrawLights.h"

using namespace Engine_I;

Engine_I::DrawLights::DrawLights()
{
}

Engine_I::DrawLights::~DrawLights()
{

}

//Another GameDev WinMain method - this one for Lighting initialization
bool Engine_I::DrawLights::Init()
{
	//Usefull Stuff:
	//LightDesc - description for new light
	//lightDesc.lights - array of lights that would be passed to GPU. There You go with all light definitions

	LightDesc light1;
	light1.Ambient = D3DXCOLOR(0.2f, 0.5f, 0.8f, 1.0f);
	light1.Attenuation = D3DXVECTOR3(1.0f, 1.0f, 0.0f);
	light1.Diffuse = D3DXCOLOR(0.2f, 0.8f, 0.5f, 1.0f);
	light1.Dirrection = D3DXVECTOR3(0,-1.0f,0);
	light1.Position = D3DXVECTOR3(1,6,0);
	light1.Range = 40;
	light1.Specular = D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f);
	light1.SpotPower = 2;
	light1.Type = 2;

	LightDesc light2;
	light2.Ambient = D3DXCOLOR(0.7f, 0.5f, 0.8f, 1.0f);
	light2.Attenuation = D3DXVECTOR3(1.0f, 1.0f, 0.0f);
	light2.Diffuse = D3DXCOLOR(0.7f, 0.8f, 0.5f, 1.0f);
	light2.Dirrection = D3DXVECTOR3(0,-1.0f,0);
	light2.Position = D3DXVECTOR3(1,2,1);
	light2.Range = 30;
	light2.Specular = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	light2.SpotPower = 5;
	light2.Type = 3;

	LightDesc light3;
	light3.Ambient = D3DXCOLOR(0.7f, 0.9f, 0.8f, 1.0f);
	light3.Attenuation = D3DXVECTOR3(1.0f, 1.0f, 0.0f);
	light3.Diffuse = D3DXCOLOR(0.7f, 0.8f, 0.9f, 1.0f);
	light3.Dirrection = D3DXVECTOR3(0,-1.0f,0);
	light3.Position = D3DXVECTOR3(1,2,1);
	light3.Range = 30;
	light3.Specular = D3DXCOLOR(0.5f, 0.9f, 0.5f, 1.0f);
	light3.SpotPower = 5;
	light3.Type = 3;

	LightDesc light4;
	light4.Ambient = D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.0f);
	light4.Attenuation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	light4.Diffuse = D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.0f);
	light4.Dirrection = D3DXVECTOR3(0,-1.0f,0);
	light4.Position = D3DXVECTOR3(0,1,0);
	light4.Range = 40;
	light4.Specular = D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);
	light4.SpotPower = 1;
	light4.Type = 1;

	lightDesc.Lights[0] = LightDesc(light1);
	lightDesc.Lights[1] = LightDesc(light2);
	lightDesc.Lights[2] = LightDesc(light3);
	lightDesc.Lights[3] = LightDesc(light4);

	lightDesc.LightNum = 4;

	return true;
}

void Engine_I::DrawLights::ChangePosition(int index, D3DXVECTOR3 position)
{
	lightDesc.Lights[index].Position = position;
}

D3DXVECTOR3 Engine_I::DrawLights::GetPosition(int index)
{
	return lightDesc.Lights[index].Position;
}

//Another GameDev WinMain method - this one for Lighting Dynamics
void Engine_I::DrawLights::Update(GameTime gameTime, D3DXVECTOR3 eyePos)
{
	//This part should be hidden, maybe in base class?
	lightDesc.EyePos = eyePos;
	time = gameTime;
	//End of part that should be hidden

	//Why is it empty ? Good question. Actually whole lighting movement logic is in "DrawWorld.Frame"
	//That's wrong - I should hide "DrawLight" class in "DrawWorld" because I violate my own design rules :)
}

void Engine_I::DrawLights::Shutdown()
{

}