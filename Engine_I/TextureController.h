#ifndef __TEXTURECONTROLLER_H_
#define __TEXTURECONTROLLER_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <string>
#include <sstream>

//Defining custon libraries
#include "TextureInterface.h"
#include "trace.h"
#include "MainTextInterface.h"
#include "TextureContainer.h"

using namespace std;

namespace Engine_I
{
	//ClassName   : TextureController
	//LifeTime    : Initialized during graphic initialization, executed on each frame
	//Description : This controller is passed to main application. It has structure of "TextureController" instances - check TextureContainer.h for important reference.
	//			    According to dirrectory structure in UNC path //Textures
	//				Whole texture controlling is written in structure like in example:
	//
	//						TextureController <-- TextureInterface
	//							   |				     |
	//	Texture Container -> Composite Structure   Flat Structure
	//						   of Textures			of Textures
	//							   |					 |
	//				+------+-------+      +------+-------+------+------+
	//				|      |       |	  |      |       |		|      |
	//			   tex1  dir1    dir2    tex1   tex2    tex3   tex4  tex5
	//					   |       |
	//				+------+       +------+
	//			    |      |       |      |
	//             tex2  tex3     tex4   tex5
	//
	//			   To get tex5 (stored in \\Textures\dir2\tex5.ext ) You have to write:
	//			   \\dir2\tex5.ext
	//
	class TextureController
	{
	public:
		TextureController();
		~TextureController();

		//Name		  : Init
		//Description : Initialization of instance it creates whole TextureContainer structure starting from "textureContainer" field 
		//Input		  : device		       - pointer to Direct X device
		//				mainTextInterface  - pointer to console ( chat window ) controller ( check MainTextInterface.h for reference )
		//Output	  : true when no errors, else false
		bool Init(ID3D10Device *device, MainTextInterface *mainTextInterface);

		//Name		  : ShutDown
		//Description : ShutDown routine
		void ShutDown();

		//Name		  : GetTextureContainer
		//Description : Get texture container root based on UNC path \\Texture (root) - check "TextureContainer.h" for reference
		//Output	  : TextureContainer Root
		TextureContainer GetTextureContainer();

		
		//Name		  : GetTexture
		//Description : Get texture based on UNC path - check "TextureContainer.h" for reference
		//Output	  : texture from UNC path
		ID3D10ShaderResourceView* GetTexture(string textureName);

	private:

		//Name		  : FindFilesInDir
		//Description : Helper method used in Init. It helps to find child UNC pathes based on parent UNC path of specified container
		//Input		  : container - pointer to parent container instance. Its collection would be changed in body of this method if output = true
		//Output	  : true if child container has been found else false
		bool FindFilesInDir(TextureContainer *container);

		//Root TextureContainer
		TextureContainer textureContainer;

		//Pointer to Direct X device
		ID3D10Device *device;

		//Pointer to console (chat) window. Check "MainTextInterface.h" for reference 
		MainTextInterface *mainTextInterface;

		//It has flat structure of textures in all folders. It also hides Direct X logic. Check "TextureInterface.h" for reference
		TextureInterface textureInterface;
	};
};

#endif