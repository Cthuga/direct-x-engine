#ifndef __BASEEFFECTDESC_H_
#define __BASEEFFECTDESC_H_

#include <Windows.h>

#include "define_core.h"

#include D3D
#include D3DX

#include "AbstractEffectDesc.h"

namespace Engine_I
{
	struct BaseEffectDesc : AbstractEffectDesc
	{
	public:
		BaseEffectDesc();
		BaseEffectDesc::BaseEffectDesc(D3DXMATRIX WVP);

		D3DXMATRIX WVP;
		D3DXMATRIX W;
		int texAdressingType;
		bool Clip;
	};
};

#endif