#ifndef __WINDOW_H_
#define __WINDOW_H_

//Defining system libraries
#include <Windows.h>
#include <math.h>
#include <string>
#include <sstream>

//Defining custon libraries
#include "trace.h"
#include "graphic.h"
#include "engine_ini.h"
#include "timer.h"
#include "input.h"

using namespace std;

namespace Engine_I
{
	//ClassName   : Window
	//LifeTime    : Initialized during start of application, destroyed during closing application
	//Description : Class representing main window of application
	class Window
	{
	public:
		Window(LPCWSTR name);
		~Window();

		//Name		  : Init
		//Description : Initialize main window
		//Output	  : true when no errors, else false
		bool Init();

		//Name		  : Run
		//Description : This metod contains rendering graphic, and peek message routine. As long as this method is running - graphic is shown.
		void Run();

		//Name		  : ShutDown
		//Description : Application shut down routine
		void ShutDown();

		//Application width property
		int width;

		//Application height property
		int height;

		//Handling all events from window
		LRESULT CALLBACK MessageHandler(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

	private:

		//Method executed during resizing window. It refreshes Direct X core.
		void OnResize();

		//Window Handle field
		HWND hwnd;

		//Instance of application field
		HINSTANCE hinstance;

		//Application name field
		LPCWSTR className;

		//Main graphic instance field - it has whole graphic logic. WinMain for Direct X
		Graphic graphic;

		//Config instance field
		EngineIni engine; 

		//Timer engine
		Timer timer;

		//Input controller
		Input input;

		//Class flags:
		bool deviceStarted; //Set when Direct X device has started
		bool appPaused;		//Set when application is paused or not focused, else Reset
		bool resizing;		//Set during resizing routine, else reset
		bool minimalized;   //Set when application is in minimalized state, else Reset
		bool maximalized;   //Set when application is in maximalized state, else Reset

	};

	//Handling all events from window - it is redirrected to Window message handling routine
	LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

};

#endif