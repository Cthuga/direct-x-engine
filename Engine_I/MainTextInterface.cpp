#include "MainTextInterface.h"

using namespace Engine_I;

Engine_I::MainTextInterface::MainTextInterface()
{
	secondController = 0;
}

Engine_I::MainTextInterface::~MainTextInterface()
{

}

void Engine_I::MainTextInterface::AddLog(string message, D3DXCOLOR color)
{
	list<TextInfoInterfaceStruct>::iterator iterator;
	bool exists = false;

	for(iterator = logs.begin() ; iterator != logs.end() ; iterator++)
	{
		string par = string(iterator->Parameter.begin(), iterator->Parameter.end());
		if(par == message)
		{
			exists = true;
			break;
		}
	}

	if(!exists)
	{
		TextInfoInterfaceStruct log;
		log.Color = color;
		log.Parameter = wstring(message.begin(), message.end());
		log.Priority = 30;
		logs.push_back(log);
	}
}

void Engine_I::MainTextInterface::Render(GameTime time)
{
	__int64 delta = time.S - secondController;

	logs.sort();

	list<TextInfoInterfaceStruct>::iterator iterator;
	list<TextInfoInterfaceStruct>::iterator toRemoveIterator;
	list<TextInfoInterfaceStruct> toRemove;

	int counter = 0;
	for(iterator = logs.begin() ; iterator != logs.end() ; iterator++)
	{
		iterator->Priority -= (int)delta;
		if(iterator->Priority <= 0)
		{
			toRemove.push_back(*iterator);
		}
		else
		{
			wstring message = iterator->Parameter;
			RECT rect = { 150, 5 + counter * 15, 0, 0};
			font->DrawText(NULL, message.c_str(), -1, &rect, DT_NOCLIP, iterator->Color);
			counter++;
		}
	}

	for(toRemoveIterator = toRemove.begin(); toRemoveIterator != toRemove.end() ; toRemoveIterator++)
	{
		logs.remove(*toRemoveIterator);
	}
	
	secondController += (int)delta;
}

bool Engine_I::MainTextInterface::Init(ID3DX10Font *iFont, int iWidth, int iHeight)
{
	font = iFont;
	width = iWidth;
	height = iHeight;

	return true;
}

void Engine_I::MainTextInterface::Warning(string message)
{
	AddLog(message, D3DXCOLOR(0.9f, 0.9f, 0, 1.0f));
}