#include "engine_ini.h"

using namespace Engine_I;

Engine_I::EngineIni::EngineIni()
{
	section = "";
	sections["Graphic Section"] = GraphicSection;
	sections["Input Section"] = InputSection;
	sections[""] = SNone;
}

Engine_I::EngineIni::~EngineIni()
{

}

bool Engine_I::EngineIni::Init()
{
	ifstream iniFile("engine.ini");
	
	if(!iniFile.good())
	{
		iniFile.close();
		if(!Serialize())
		{
			Trace::Error("Nie uda�o si� utworzy� domy�lnego pliku ini");
			return false;
		}
	}

	if(!iniFile.is_open())
	{
		iniFile.open("engine.ini");
	}

	section = "";
	string line;

	if(iniFile.is_open())
	{
		while(!iniFile.eof())
		{
			line.clear();
			getline(iniFile,line);

			if(line != "" && line[0] == '#')
			{
				section = line.substr(1, sizeof(line) - 1);
			}

			if(string::npos != line.find("="))
			{
				string par;
				string val;
	
				int at = line.find("=");
				par = line.substr(0, at - 1);
				val = line.substr(at+1, sizeof(line));
				trim(par);
				trim(val);

				switch(sections[section])
				{
					case InputSection:
						ParseInputSection(par, val);
						break;

					case SNone:
						break;

					case GraphicSection:
						ParseGraphicSection(par, val);
						break;

					default:
						break;
				}
			}
		}
	}
	else
	{
		Trace::Error("Nie uda�o si� otworzy� pliku engine.ini, zamykanie aplikacji");
		
		if(iniFile.is_open())
		{
			iniFile.close();
		}

		return false;
	}

	if(iniFile.is_open())
	{
		iniFile.close();
	}

	if(!Serialize())
	{
		Trace::Error("Nie uda�o si� utworzy� domy�lnego pliku ini");
		return false;
	}

	return true;
}

bool Engine_I::EngineIni::ParseInputSection(string par, string val)
{
	if(par == "Move Forward")
	{
		if(val == "") val = "W";
		config.configInput.MoveForward = val;
		Trace::Log("##Mapowanie : Move Forward = " + val);
		return true;
	}

	if(par == "Move Backward")
	{
		if(val == "") val = "S";
		config.configInput.MoveBackward = val;
		Trace::Log("##Mapowanie : Move Backward = " + val);
		return true;
	}

	if(par == "Strafe Left")
	{
		if(val == "") val = "A";
		config.configInput.StrafeLeft = val;
		Trace::Log("##Mapowanie : Strafe Left = " + val);
		return true;
	}

	if(par == "Strafe Right")
	{
		if(val == "") val = "D";
		config.configInput.StrafeRight = val;
		Trace::Log("##Mapowanie : Strafe Right = " + val);
		return true;
	}

	if(par == "Quit")
	{
		if(val == "") val = "Esc";
		config.configInput.Quit = val;
		Trace::Log("##Mapowanie : Quit = " + val);
		return true;
	}

	if(par == "Pause")
	{
		if(val == "") val = "P";
		config.configInput.Pause = val;
		Trace::Log("##Mapowanie : Pause = " + val);
		return true;
	}

	return true;
}

bool Engine_I::EngineIni::ParseGraphicSection(string par, string val)
{
	if(par == "Full Screen")
	{
		if(val == "") val = "false";
		istringstream is(val);
		is >> boolalpha >> config.configGraphic.FullScreen;
		Trace::Log("##Mapowanie : Full Screen = " + val);
		return true;
	}

	if(par == "Screen Width")
	{
		if(val == "") val = "800";
		config.configGraphic.ScreenWidth = atoi(val.c_str());
		Trace::Log("##Mapowanie : Screen Width = " + val);
		return true;
	}

	if(par == "Screen Height")
	{
		if(val == "") val = "600";
		config.configGraphic.ScreenHeight = atoi(val.c_str());
		Trace::Log("##Mapowanie : Screen Height = " + val);
		return true;
	}

	return true;
}

bool Engine_I::EngineIni::Serialize()
{
	ofstream iniFile;

	iniFile.open("engine.ini");

	iniFile << "#Input Section" << endl;

	iniFile << endl;

	iniFile << "Move Forward = " << config.configInput.MoveForward << endl;
	iniFile << "Move Backward = " << config.configInput.MoveBackward << endl;
	iniFile << "Strafe Left = " << config.configInput.StrafeLeft << endl;
	iniFile << "Strafe Right = " << config.configInput.StrafeRight << endl;
	iniFile << "Pause = " << config.configInput.Pause << endl;
	iniFile << "Quit = " << config.configInput.Quit << endl;

	iniFile << endl;

	iniFile << "#Graphic Section" << endl;
	
	iniFile << endl;

	iniFile << "Full Screen = " << boolalpha << config.configGraphic.FullScreen << endl;
	iniFile << "Screen Width = " << config.configGraphic.ScreenWidth << endl;
	iniFile << "Screen Height = " << config.configGraphic.ScreenHeight << endl;
	
	iniFile.close();

	return true;
}

Config Engine_I::EngineIni::GetConfig()
{
	return config;
}