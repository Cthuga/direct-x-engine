#ifndef __ABSTRACTCBPERFRAMEDESC_H_
#define __ABSTRACTCBPERFRAMEDESC_H_

//Defining system libraries
#include <windows.h>

//Defining custom libraries
#include "AbstractEffectDesc.h"

namespace Engine_I
{
	//StructName  : AbstractCbPerFrameDesc
	//Description : Check AbstractEffectDesc for reference
	struct AbstractCbPerFrameDesc : AbstractEffectDesc
	{
	public:
		AbstractCbPerFrameDesc();
	};
};

#endif