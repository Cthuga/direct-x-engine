#ifndef __RASTERCONTROLLER_H_
#define __RASTERCONTROLLER_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining custon libraries
#include "MainTextInterface.h"
#include "trace.h"

namespace Engine_I
{
	//ClassName   : RasterController
	//LifeTime    : Initialized during graphic initialization, executed during rendering some models with special Raster Effects for each frame
	//Description : Class responsible for changes during Rasterization Stage of rendering. 
	//				As reminder description of Direct X rendering stages :
	//
	//					Input Assembler Stage <------------------+
	//							||								 |
	//							\/								 |
	//					 Vertex Shader Stage <-------------------+
	//							||								 |
	//							\/								 |
	//				    Geometry Shader Stage <------------------+
	//							||								 |
	//							\/								 |
	//							 +----> Stream Output Stage ---->+-----> GPU Resources, Buffers, Textures
	//							||								 |	 
	//							\/								 |	(the communication between GPU and CPU has been done through World class - check World.h
	//					 Rasterizer Stage <----------------------+		       It is : World <---> EffectController <---> HLSL Script Files )
	//	(this class is responsible for effects for this stage )	 |					Check World.H and EffectController.h for reference
	//							||							     |
	//							\/								 |
	//					Pixel Shader Stage <---------------------+
	//							||							     |
	//							\/							     |
	//			       Output Merger Stage <-------------------->+
	class RasterController
	{
	public:
		RasterController();
		~RasterController();

	private:
		RasterController(const RasterController &other);
		RasterController& operator=(const RasterController &other);

	public:

		//Name		  : Init
		//Description : Initialization of RasterController. During initialization all Raster effects are prepared
		//Input		  : device		    	-  pointer to Direct X device
		//				mainTextInterface   -  pointer to console ( chat window ) controller 
		//Output	  : true when no errors, else false
		bool Init(ID3D10Device *device, MainTextInterface *mainTextInterface);

		//Name		  : ShutDown
		//Description : ShutDown routine
		void ShutDown();

	private:

		//Name		  : InitRSBackFace
		//Description : Helper method for Init stage. It creates RSBackFace raster effect.
		//Output	  : true when no errors, else false
		bool InitRSBackFace();

	public:
		//RSBackFace Raster effect - changes sides of rendering faces for triangles from clockwise to counterclockwide
		//Especially usefull for reflected models ( check MirrorController.h for reference )
		ID3D10RasterizerState *RSBackFace;

	private:

		//Pointer to Direct X device
		ID3D10Device *device;

		//Pointer to console ( chat window ) controller 
		MainTextInterface *mainTextInterface;
	};
};

#endif