#ifndef __CONFIG_H_
#define __CONFIG_H_

//Defining custon libraries
#include "config_graphic.h"
#include "config_input.h"

namespace Engine_I
{
	//ClassName   : Config
	//LifeTime    : As long as "engine_ini.h" lifetime. 
	//Description : It stores all values from "*.ini" configuration file. For some more info check "engine_ini.h" reference.
	class Config
	{
	public:
		Config();
		~Config();

		//Values stored in *.ini file are separated by sections. That's why Config class also was split on class sections:
		ConfigGraphic configGraphic;		//Graphic Section
		ConfigInput configInput;			//Input Section
	};
};

#endif
