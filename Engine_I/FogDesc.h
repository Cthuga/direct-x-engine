#ifndef __FOGDESC_H_
#define __FOGDESC_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D;
#include D3DX;

//Defining system libraries
#include <Windows.h>

namespace Engine_I
{
	//StructName  : FogDesc
	//Description : Description of Fog used in Fixed Description. Check "AbstractEffectDesc.h" for more info
	struct FogDesc
	{
	public:
		FogDesc();

		//Color for fog effect
		D3DXVECTOR3 gFogColor;

		//Radius for fog-free square
		float gFogStart;

		//Max range of fog ( range where gFogRange = MAX(fog saturation) )
		float gFogRange;

		//Empty pad fields
		float __pad0;
		float __pad1;
		float __pad2;
	};
};

#endif