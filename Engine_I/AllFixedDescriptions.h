#ifndef __ALLFIXEDDESCRIPTIONS_H_
#define __ALLFIXEDDESCRIPTIONS_H_

//Defining system libraries
#include <Windows.h>

//Defining custom libraries
#include "FogDesc.h"

namespace Engine_I
{
	//StructName  : AllFixedDescriptions
	//Description : All fixed descriptions for engine. It was designed for later use ( where there would be more fixed descriptions )
	struct AllFixedDescriptions
	{
	public:
		AllFixedDescriptions();

		//Fog descriptions
		FogDesc Fog;
	};
};

#endif