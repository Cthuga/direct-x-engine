#include "AbstractEffect.h"

using namespace Engine_I;

Engine_I::AbstractEffect::AbstractEffect()
{
	device = 0;
	technique = 0;
	inputLayout = 0;
	effect = 0;
	mainInterface = 0;
	inputLayout = 0;	
}

Engine_I::AbstractEffect::~AbstractEffect()
{

}

bool Engine_I::AbstractEffect::Init(wstring fileName, MainTextInterface *mInterface, string techniqueName, ID3D10Device *iDevice, AbstractCbFixedDesc *cbFixed)
{
	device = iDevice;
	bool isFailed = false;
	ID3D10Blob* errors = 0;
	HRESULT result;
	wstring flushString;
	wostringstream sStream;
	mainInterface = mInterface;
	wstring wErrorMessage;
	string errorMessage;

	result = D3DX10CreateEffectFromFile(fileName.c_str(), 0, 0, "fx_4_0", D3D10_SHADER_ENABLE_STRICTNESS | HLSL_FLAGS , 0, device, 0, 0, &effect, &errors, NULL);
	if(FAILED(result))
	{
		sStream << "Unable to load effect file : " << fileName << " HRESULT : " << result;
		wErrorMessage = sStream.str();
		errorMessage = string(wErrorMessage.begin(), wErrorMessage.end());
		mainInterface->Warning(errorMessage);
		Trace::Log(errorMessage);

		isFailed = true;
	}

	if(errors)
	{
		sStream.clear();
		sStream.str(flushString);
		sStream << "During compilling effect file " << fileName << " there was Error: ";
		wErrorMessage = sStream.str();
		errorMessage = string(wErrorMessage.begin(), wErrorMessage.end());
		mainInterface->Warning(errorMessage);
		Trace::Log(errorMessage);

		errorMessage = (char*)errors->GetBufferPointer();
		mainInterface->Warning(errorMessage);
		Trace::Log(errorMessage);

		errors->Release();
		errors = 0;
	}

	if(isFailed)
	{
		return false;
	}

	technique = effect->GetTechniqueByName(techniqueName.c_str());

	if(technique == 0)
	{
		stringstream stream;
		stream << "Technique " << techniqueName << " not found in effect file " << string(fileName.begin(), fileName.end());
		Trace::Log(stream.str());
		mainInterface->Warning(stream.str());
	}	

	return true;
}

void Engine_I::AbstractEffect::UpdateFixed(AbstractCbFixedDesc* abstractCbFixedDesc)
{

}

void Engine_I::AbstractEffect::UpdatePerObject(AbstractCbPerObjectDesc *abstractCbPerObjectDesc)
{

}

void Engine_I::AbstractEffect::UpdatePerFrame(AbstractCbPerFrameDesc *abstractCbPerFrameDesc)
{

}

void Engine_I::AbstractEffect::ShutDown()
{
	if(inputLayout)
	{
		inputLayout->Release();
		inputLayout = 0;
	}

	if(effect)
	{
		effect->Release();
		effect = 0;
	}
}

ID3D10InputLayout* Engine_I::AbstractEffect::GetInputLayout()
{
	return inputLayout;
}

void Engine_I::AbstractEffect::UpdatePerTexture(ID3D10ShaderResourceView* texture)
{

}