#ifndef __ABSTRACTEFFECTDESC_H_
#define __ABSTRACTEFFECTDESC_H_

//Defining system libraries
#include <windows.h>

namespace Engine_I
{
	//StructName  : AbstractEffectDesc
	//Description : It is root for all effect descriptions. Derivation schema:
	//
	//				AbstractEffectDesc
	//					  /\
	//					  ||
	//        +-----------++-----------+-----------------------------+
	//		  |                        |                             |
	// AbstractCbFiedDesc   AbstractCbPerObjectDesc       AbstractCbPerFrameDesc
	//        /\                       /\                            /\
	//        ||                       ||                            ||
	// [ConcreteDescriptions]   [ConcreteDescriptions]     [ConcreteDescriptions]
	//
	//
	// HLSL effect input/output structures were separated according to their updating routine. 
	// CbFixed structure is updated once during initialization process
	// CbPerObject structure is updated once for each game model
	// CbPerFrame struct is updated once for each frame
	//
	// Check "textured_light.fx" for reference
	struct AbstractEffectDesc
	{
	public:
		AbstractEffectDesc();
	};
};

#endif