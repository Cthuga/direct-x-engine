#ifndef __ENGINE_INI_H_
#define __ENGINE_INI_H_

//Defining system libraries
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <algorithm>
#include <iomanip>
#include <cctype>

//Defining custon libraries
#include "config.h"
#include "trace.h"
#include "utils.h"

using namespace std;

namespace Engine_I
{
	//EnumName    : Section
	//Description : It stores values of all sections from *.ini file. Check "sections" and "section" field in "EngineIni" class for more info
	enum Section
	{
		InputSection,
		GraphicSection,
		SNone
	};

	//ClassName   : EngineIni
	//LifeTime    : It starts during application start, ends when application is closed
	//Description : It maps environment configurations from [configuration].ini file. If doesn't exists it also creates new *.ini file.
	//				Whole structure of configuration file is described here:
	//
	//								EngineIni
	//								    |
	//								 Config
	//									|
	//					+---------Some Sections---------+
	//				    |			    |				|
	//				Section 1		 Section 2		Section n
	//
	//
	//				Path of configuration (ini) file is : \\Engine path\engine.ini
	class EngineIni
	{

	public:

		EngineIni();
		~EngineIni();

		//Name		  : Init
		//Description : Initialization of instance. It loads all values from *.ini during this stage
		//Output	  : true when no errors, else false
		bool Init();

		//Name		  : Serialize
		//Description : It saves new environment values to *.ini file
		//Output	  : true when no errors, else false
		bool Serialize();

		//Name		  : GetConfig
		//Description : Get all configuration values
		//Output	  : Configuration value description ( check config.h for reference )
		Config GetConfig();

	private:
		
		//Name		  : ParseGraphicSection
		//Description : Helper method that parses values from Graphic Section to Config singleton
		//Output	  : true when no errors, else false
		bool ParseGraphicSection(string par, string val);

		//Name		  : ParseInputSection
		//Description : Helper method that parses values from Input Section to Config Singleton
		//Output	  : true when no errors, else false
		bool ParseInputSection(string par, string val);

		//Main Config variable - it stores all informations from *.ini configuration file. ( check config.h for reference )
		Config config;	

		//*.ini file has specified some sections, for example #Graphic Section. This helper collection stores all defined sections as enum ( for switch/case logic )
		map<string, Section> sections;

		//TODO: Unnesesary value, use in inner logic only
		//Temporary section used during mapping sections to "map collection" 
		string section;

	};
};

#endif