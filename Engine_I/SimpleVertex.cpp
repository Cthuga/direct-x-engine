#include "SimpleVertex.h"

using namespace Engine_I;

Engine_I::SimpleVertex::SimpleVertex(D3DXVECTOR3 position, D3DXCOLOR color) : AbstractVertex()
{
	this->position = position;
	this->color = color;
}