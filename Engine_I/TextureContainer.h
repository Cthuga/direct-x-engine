#ifndef __TEXTURECONTAINER_H_
#define __TEXTURECONTAINER_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <map>
#include <string>
#include <vector>

using namespace std;

namespace Engine_I
{
	//ClassName   : TextureContainer
	//LifeTime    : Initialized during graphic initialization ( TextureController initialization), destroyed during application closing
	//Description : TextureInterface loads texture to memory ( check TextureInterface.h ). TextureController transforms whole structure to object oriented ( check TextureController.h for reference )
	//				TextureContainer is description of texture collection used by TextureController. Collection is split according to Texture Dirrectory structure.
	//				For Example Structure Dirrectory :
	//		
	//						Textures
	//						   |
	//				+----------+--------------------------+
	//				|		   |					      |
	//             Dir1      Dir2                       Dir3
	//              |          |       +------------------+
	//           [textures] [textures] |                  |
	//                               Dir4               Dir5
	//                                 |                  |
	//                              [textures]        [textures]
	//
	//
	// Would Make TextureContainer one-way composite for all dirrectories in same way:
	//
	//					TextureContainer (root)
	//						   |
	//				+----------+--------------------------+
	//				|		   |					      |
	//            TC[1/1]    TC[1/2]                   TC[1/3]
	//              |          |       +------------------+
	//           [textures] [textures] |                  |
	//                              TC[2/1]            TC[2/2]
	//                                 |                  |
	//                              [textures]        [textures]
	class TextureContainer
	{
	public:
		TextureContainer();
		~TextureContainer();

		//Name of container ( used as container index )
		string ConainerName;

		//Collection of textures in this container
		vector<string> Textures;

		//All child containers of this container
		map<string, TextureContainer> ChildContainers;		
	};
};

#endif