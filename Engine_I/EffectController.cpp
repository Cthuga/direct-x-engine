#include "EffectController.h"

using namespace Engine_I;

Engine_I::EffectController::EffectController()
{
	effects["Base Effect"] = new BaseEffect();
	effects["Base Light Effect"] = new BaseLightEffect();
	effects["Texture Light Effect"] = new TextureLightEffect();
}

Engine_I::EffectController::~EffectController()
{

}

bool Engine_I::EffectController::Init(MainTextInterface *mainTextInterface, ID3D10Device *&device, AllFixedDescriptions allFixedDescriptions)
{	
	this->mainTextInterface = mainTextInterface;

	cbFixedDesc.Fog = allFixedDescriptions.Fog;

	BaseEffect *baseEffect = (BaseEffect*)effects["Base Effect"];
	BaseLightEffect *baseLightEffect = (BaseLightEffect*)effects["Base Light Effect"];
	TextureLightEffect *textureLightEffect = (TextureLightEffect*)effects["Texture Light Effect"];

	baseEffect->Init(mainTextInterface, device, 0);
	baseLightEffect->Init(mainTextInterface,device, 0);
	textureLightEffect->Init(mainTextInterface, device, &cbFixedDesc);

	effects["Base Effect"] = baseEffect;
	effects["Base Light Effect"] = baseLightEffect;
	effects["Texture Light Effect"] = textureLightEffect;

	effects["Base Effect"]->UpdateFixed(0);
	effects["Base Light Effect"]->UpdateFixed(0);
	effects["Texture Light Effect"]->UpdateFixed(&cbFixedDesc);

	return true;
}

void Engine_I::EffectController::ShutDown()
{
	map<string, AbstractEffect*>::iterator it;

	for(it = effects.begin() ; it != effects.end() ; it++)
	{
		it->second->ShutDown();
		it->second = 0;
	}
}

AbstractEffect* Engine_I::EffectController::GetEffect(string effectName)
{
	map<string, AbstractEffect*>::iterator it;

	for(it = effects.begin() ; it != effects.end() ; it++)
	{
		if(it->first == effectName)
		{
			return it->second;
		}
	}

	ostringstream warningMessage;
	warningMessage << "Failed to load effect " << effectName;
	Trace::Error(warningMessage.str());
	mainTextInterface->Warning(warningMessage.str());
}

void Engine_I::EffectController::UpdatePerFrame(AbstractCbPerFrameDesc *abstractCbPerFrameDesc)
{
	effects["Base Effect"]->UpdatePerFrame(0);
	effects["Base Light Effect"]->UpdatePerFrame(abstractCbPerFrameDesc);
	effects["Texture Light Effect"]->UpdatePerFrame(abstractCbPerFrameDesc);
}

void Engine_I::EffectController::UpdatePerTexture(ID3D10ShaderResourceView *texture)
{
	effects["Base Effect"]->UpdatePerTexture(0);
	effects["Base Light Effect"]->UpdatePerTexture(0);
	effects["Texture Light Effect"]->UpdatePerTexture(texture);
}