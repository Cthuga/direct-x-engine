#include "Floor.h"

using namespace Engine_I;

Engine_I::Floor::Floor() : Model()
{
	Type = "Floor";
	faultedState = 0;
}

Engine_I::Floor::~Floor()
{

}

Engine_I::Floor::Floor(const Floor &other) : Model(other)
{

}

Floor& Engine_I::Floor::operator=(const Floor &other) 
{
	Model* m = (Model*)&other;
	Model* result = m;

	return Floor(*(Floor*)result);
}

bool Engine_I::Floor::Init(AbstractEffect *effect, ID3D10Device *&device, MainTextInterface *&mainTextInterface, string name, ModelDesc *desc, StencilController *stencilController)
{
	float pwidth = (float)desc->Width/2;
	float plong = (float)desc->Long/2;

	D3DXPLANE floorPlane;
	D3DXPlaneFromPoints(&floorPlane, &D3DXVECTOR3(-pwidth, 0, plong), &D3DXVECTOR3(pwidth, 0 ,-plong), &D3DXVECTOR3(-pwidth, 0,-plong));
	Planes.push_back(floorPlane);

	faultedState = 0;
	Model::Init(effect, device, mainTextInterface, name, desc, stencilController);

	numIndices = 3*2*2;
	numVertices = 4;
	numFaces = 2;

	strideLength = sizeof(TexVertex);

	D3DXVECTOR3 v1 = D3DXVECTOR3(1,-1,-1);
	D3DXVECTOR3 v2 = D3DXVECTOR3(-1,-1,-1);
	D3DXVECTOR3 v3 = D3DXVECTOR3(-1,-1,1);
	D3DXVECTOR3 v4 = D3DXVECTOR3(1,-1,1);

	D3DXVECTOR3 v1N = D3DXVECTOR3(0,0,0);
	D3DXVECTOR3 v2N = D3DXVECTOR3(0,0,0);
	D3DXVECTOR3 v3N = D3DXVECTOR3(0,0,0);
	D3DXVECTOR3 v4N = D3DXVECTOR3(0,0,0);

	D3DXVec3Normalize(&v1N, &v1);
	D3DXVec3Normalize(&v2N, &v2);
	D3DXVec3Normalize(&v3N, &v3);
	D3DXVec3Normalize(&v4N, &v4);

	TexVertex vertices[] = 
	{
		TexVertex(D3DXVECTOR3(pwidth, 0 ,-plong), v1N, desc->SpecPower, D3DXVECTOR2(1.0f / desc->TexXScale, 1.0f / desc->TexYScale)),
		TexVertex(D3DXVECTOR3(-pwidth, 0,-plong), v2N, desc->SpecPower, D3DXVECTOR2(0.0f,1.0f / desc->TexYScale)),
		TexVertex(D3DXVECTOR3(-pwidth, 0, plong), v3N, desc->SpecPower, D3DXVECTOR2(0.0f,0.0f)),
		TexVertex(D3DXVECTOR3(pwidth, 0,  plong), v4N, desc->SpecPower, D3DXVECTOR2(1.0f / desc->TexXScale,0.0f))
	};

	int indices[] =
	{
		2,1,0,		3,2,0,
		0,1,2,		0,2,3
	};

	return CreateNewBuffers(vertices, indices, sizeof(TexVertex));
}
	
bool Engine_I::Floor::Render()
{
	if(!Model::Render()) return false;
}

void Engine_I::Floor::ShutDown()
{
	Model::ShutDown();
}

