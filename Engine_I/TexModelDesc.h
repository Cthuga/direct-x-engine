#ifndef __TEXMODELDESC_H_
#define __TEXMODELDESC_H_

#include "define_core.h"

#include D3D
#include D3DX

#include <Windows.h>

#include "ModelDesc.h"

namespace Engine_I
{
	struct TexModelDesc : ModelDesc
	{
	public:
		TexModelDesc();
		~TexModelDesc();

		D3DXVECTOR2 Texels;
		int texAdressingType;
	};
};

#endif