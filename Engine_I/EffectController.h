#ifndef __EFFECTCONTROLLER_H_
#define __EFFECTCONTROLLER_H_

//Defining system libraries
#include <map>
#include <string>
#include <sstream>

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining custon libraries
#include "BaseEffect.h"
#include "MainTextInterface.h"
#include "AbstractEffect.h"
#include "BaseLightEffect.h"
#include "TextureLightEffect.h"
#include "AllFixedDescriptions.h"
#include "cbFixedDesc.h"
#include "trace.h"

namespace Engine_I
{
	//ClassName   : EffectController
	//LifeTime    : Initialized during graphic initialization, executed on each frame
	//Description : It is storage for all HLSL script files used for Engine. Description of whole structure :
	//
	//								EffectController  
	//									  |
	//							    Effect Collection
	//							   +----->|
	//	Fixed Values			   |	  +------AbstractEffect -- (polymorphed to) --> ConcreteEffect 1
	//	 Collection				   +----->|
	//		 |					   |	  +------AbstractEffect -- (polymorphed to) --> ConcreteEffect 2
	//		 +-----(bind)----------+----->|
	//									  +------AbstractEffect -- (polymorphed to) --> ConcreteEffect n
	//
	//				Check description of "AbsractEffect.h" and some concrete effects, like "TextureLightEffect.h" for reference. 
	class EffectController
	{
	public:
		EffectController();
		~EffectController();

		//Name		  : Init
		//Description : Initialization of instance. It also put into memory single instance for each effect ("fx" file) and store it to effects collection.
		//Input		  : effectController      - pointer to effect resources ( check "EffectController.h" for reference )
		//				mainTextInterface     - pointer to console ( chat window ) controller 
		//				device			      - pointer to Direct X device		
		//				allFixedDescriptions  - at this point there is also initialization of fixed values for each effect. Check more info about fixed values on "AbstractEffectDesc.h" and "World.h"
		//Output	  : true when no errors, else false
		bool Init(MainTextInterface *mainTextInterface, ID3D10Device *&device, AllFixedDescriptions allFixedDescriptions);

		//Name		  : ShutDown
		//Description : ShutDown routine
		void ShutDown();

		//Name		  : GetEffect
		//Description : Get pointer to concrete effect stored in Effect Controller by his name
		//Input		  : effect name
		//Output	  : concrete effect ( stored in abstract class ) 
		AbstractEffect* GetEffect(string effectName);

		//Name		  : UpdatePerFrame
		//Description : Update CbPerFrame description of effect files. Check "World.h" and "AbstractEffectDesc.h" for reference
		//Input		  : abstractCbPerFrameDesc - polymorphed, abstract class of cbPerFrame effect description
		void UpdatePerFrame(AbstractCbPerFrameDesc *abstractCbPerFrameDesc);

		//Name		  : UpdatePerTexture
		//Description : Update texture of effect files. Check "World.h" for reference
		//Input		  : texture - pointer to new texture for all effect files
		void UpdatePerTexture(ID3D10ShaderResourceView *texture);

	private:

		//Collection of all effect stored in memory of engine
		map<string, AbstractEffect*> effects;

		//main console(chat) window - check "MainTextInterface.h" for reference
		MainTextInterface *mainTextInterface;

		//Fixed effect description bound to all effects
		CbFixedDesc cbFixedDesc;
	};
};

#endif