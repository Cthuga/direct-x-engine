#include "EffectFixedDesc.h"

using namespace Engine_I;

Engine_I::EffectFixedDesc::EffectFixedDesc() : AbstractEffectFixedDesc()
{
	ZeroMemory(this, sizeof(EffectFixedDesc));
}