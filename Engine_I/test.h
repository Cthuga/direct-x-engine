#ifndef __TEST_H_
#define __TEST_H_

#include <vector>
#include "trace.h"

class Test
{
public:
	void Assign(Trace t);

private:
	std::vector<Trace> traces;

};

#endif