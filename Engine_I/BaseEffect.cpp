#include "BaseEffect.h"

using namespace Engine_I;

Engine_I::BaseEffect::BaseEffect() : AbstractEffect()
{

}

Engine_I::BaseEffect::~BaseEffect()
{

}

bool Engine_I::BaseEffect::Init(MainTextInterface* mInterface, ID3D10Device *device, AbstractCbFixedDesc *cbFixed)
{
	if(!AbstractEffect::Init(L"basic.fx", mInterface, "BaseTechnique", device, cbFixed)) return false;
	
	HRESULT result;

	D3D10_PASS_DESC passDesc;
	technique->GetPassByIndex(0)->GetDesc(&passDesc);

	D3D10_INPUT_ELEMENT_DESC elDes[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0}
	};

	result = device->CreateInputLayout(elDes, 2, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &inputLayout);
	if(FAILED(result))
	{
		string message = "Creating Iput Layout for effect basic.fx failed";
		Trace::Log(message);
		mainInterface->Warning(message);
		return false;
	}

	return true;
}

void Engine_I::BaseEffect::ShutDown()
{
	AbstractEffect::ShutDown();
}

void Engine_I::BaseEffect::UpdateFixed(AbstractCbFixedDesc *abstractCbFixedDesc)
{
	AbstractEffect::UpdateFixed(abstractCbFixedDesc);
}

void Engine_I::BaseEffect::UpdatePerFrame(AbstractCbPerFrameDesc *abstractCbPerFrameDesc)
{
	AbstractEffect::UpdatePerFrame(abstractCbPerFrameDesc);
}

void Engine_I::BaseEffect::UpdatePerObject(AbstractCbPerObjectDesc* baseEffectDesc)
{
	AbstractEffect::UpdatePerObject(baseEffectDesc);

	CbPerObjectDesc* baseDesc = (CbPerObjectDesc*)baseEffectDesc;
	ID3D10EffectMatrixVariable *matrix = effect->GetVariableByName("WVP")->AsMatrix();
	matrix->SetMatrix(baseDesc->WVP);
	ID3D10EffectMatrixVariable *wMatrix = effect->GetVariableByName("W")->AsMatrix();
	wMatrix->SetMatrix(baseDesc->W);
}

void Engine_I::BaseEffect::UpdatePerTexture(ID3D10ShaderResourceView* texture)
{
	AbstractEffect::UpdatePerTexture(texture);
}