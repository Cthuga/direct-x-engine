#include "TextInfoInterfaceStruct.h"

using namespace Engine_I;

bool Engine_I::TextInfoInterfaceStruct::operator()(const TextInfoInterfaceStruct& first, const TextInfoInterfaceStruct& second)  const
{
    if(first.Priority < second.Priority) return true;

    return false;
}

bool Engine_I::TextInfoInterfaceStruct::operator < (const TextInfoInterfaceStruct& first) const
{
	return Priority < first.Priority;
}

bool Engine_I::TextInfoInterfaceStruct::operator > (const TextInfoInterfaceStruct& first) const
{
	return Priority > first.Priority;
}

bool Engine_I::TextInfoInterfaceStruct::operator==(const TextInfoInterfaceStruct& first) const
{
	return Parameter==first.Parameter;
}