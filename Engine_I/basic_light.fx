//				As reminder description of Direct X rendering stages :
//
//					Input Assembler Stage <------------------+
//							||								 |
//							\/								 |
//					 Vertex Shader Stage <-------------------+
//							||								 |
//							\/								 |
//				    Geometry Shader Stage <------------------+
//							||								 |
//							\/								 |
//							 +----> Stream Output Stage ---->+-----> GPU Resources, Buffers, Textures
//							||								 |	 
//							\/								 |	(the communication between GPU and CPU has been done through World class - check World.h
//					 Rasterizer Stage <----------------------+		       It is : World <---> EffectController <---> HLSL Script Files )	
//							||							     |				     Check World.H and EffectController.h for reference
//							\/								 |
//					Pixel Shader Stage <---------------------+
//							||							     |
//							\/							     |
//			       Output Merger Stage <-------------------->+

//Define helper effect script files
#include "light_helper.fx"

//Communication between GPU ( effect script file ) and CPU ( C++ code ) has been done with help of cbuffer. 
//Cbuffer stores values that can be get/set from C++ code. Because of engine architecture there is few "cbuffers". Every cbuffer is updated in different stage of frame:

//cbPerFrame is updated once per frame
cbuffer cbPerFrame
{
	Light lightArray[256];		//description of all lights in game. Because array has fixed length, it is set to maximum number of lights 256. ( Check "light_helper.fx" for reference )
	float3 eyePos;				//actual position of camera. It is used to calculate some lighting effect
	int lightsCount;			//number of lights is passed because of performance reson. I am thinking about moving this value to "cbfixed" cbuffer.
};

//cbPerObject is updated once per rendered model
cbuffer cbPerObject
{
	float4x4 W;			//world transformation matrix - necessary for transformation calculations
	float4x4 WVP;		//world transformation matrix * view transformation matrix * presentation transformation matrix - representation of final position of object according to camera view
};

//This structure is passed as INPUT to Vertex Shader Stage. Reminder of render stages is described in RasterController.h. Also check TexVertex.h for C++ description of this InputStructure.
//Connection between C++ and GPU version of this structure is made in Model class ( check Model.h for reference ).
//Whole process take place in World class ( check World.h for reference )
struct VS_IN
{
	float3  pos	    : POSITION;		//Position of vertex
	float3  normal  : NORMAL;		//Vertex normal used with light angle to define color intensity
	float4  diffuse : DIFFUSE;		//Material color value
	float4  specPow : SPEC_POW;		//Specular power of material
};

//This structure is passed as OUTPUT from Vertex Shader Stage to INPUT Pixel Shader Stage. Reminder of render stages is described in RasterController.h
struct VS_OUT
{
	float4 pos		: SV_POSITION;   //Position of vertex
	float3 posW		: POSITION;		 //Position of vertex transformated in World space
	float3 normal   : NORMAL;		 //Vertex normal used with light angle to define color intensity
	float4 diffuse  : DIFFUSE;		 //Material color value
	float4 specPow  : SPEC_POW;		 //Specular power of material
};

//This is body of Vertex Shader Stage - logic executed once per vertex ( input was described earlier, check also TexVertex.h for C++ input description )
VS_OUT VS(VS_IN vIn)
{
	VS_OUT vOut;
	
	float4 helper;

	//This part is responsible for calculation of position every vertex and normal in 2d result screen
		vOut.pos = mul(float4(vIn.pos, 1.0f) , WVP);

		helper = mul(float4(vIn.pos, 1.0f), W);
		vOut.posW = float3(helper.x, helper.y, helper.z);

		helper= mul(float4(vIn.normal, 0.0f), WVP);
		vOut.normal = float3(helper.x, helper.y, helper.z);
	//end of calcularion

	//these values are simply passed to Pixel Shader stage
	vOut.diffuse = vIn.diffuse;
	vOut.specPow = vIn.specPow;

	return vOut;
}

//This is body of Pixel Shader Stage - logic executed once per pixel seen on the screen
float4 PS(VS_OUT vIn) : SV_TARGET
{
	//From here there is updating pixel color value based on each light described in lightArray ( check cbPerFrame->lightArray few lines up )

	//Surface info is helper structure described in light_helper.fx
	SurfaceInfo vSurface = { vIn.posW, vIn.normal, vIn.diffuse, vIn.specPow };
	
	float3 resultCol = {0,0,0};
	
	//Now according to every light type ( check light_helper.fx for reference ) update pixel color using concrete lighting type method
	for(int i = 0; i < lightsCount ; i++)
	{
		  if(lightArray[i].type == 1)
		  {
			 resultCol += ParrallelLight(vSurface, lightArray[i], eyePos);
		  }
		  else if(lightArray[i].type == 2)
		  {
			 resultCol += PointLight(vSurface, lightArray[i], eyePos);
		  }
		  else if(lightArray[i].type == 3)
		  {
			 resultCol += SpotLight(vSurface, lightArray[i], eyePos);
		  }
	} 

	//Return final color to next stage
	return float4(resultCol, vIn.diffuse.a);
}

//Technique representing effect for C++ code. We can say that's Main function for HLSL script files. Every model has attached Effect class ( see AbstractEffect.h for reference )
//and every Effect class has attached technique executed once per frame.
technique10 BaseTechnique
{
	//Every technique for real have array of "Main" functions. They might be combined in different ways during rendering models in C++ code. Every index in this array is "Pass" with number attached to it.
	//Actually there is only one Pass and one Technique per effect
	pass P0
	{
		//In Pass body there is Stage Rendering Routine ( check beginning of this file for reminder )

		//First execute method VS for Vertex Shader Stage
		SetVertexShader( CompileShader( vs_4_0, VS()) );

		//Then there is Geometry Shader Stage, actually empty
		SetGeometryShader( NULL );

		//Finally execute method PD for Pixel Shader Stage
		SetPixelShader ( CompileShader( ps_4_0, PS()) );
	}
}