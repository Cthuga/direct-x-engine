#ifndef __WORLD_H_
#define __WORLD_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <map>
#include <sstream>
#include <string>
#include <math.h>

//Defining custon libraries
#include "EffectController.h"
#include "MainTextInterface.h"
#include "Model.h"
#include "Cube.h"
#include "cbPerObjectDesc.h"
#include "Floor.h"
#include "ModelDesc.h"
#include "Grid.h"
#include "cbPerFrameDesc.h"
#include "BaseLightEffect.h"
#include "DrawLights.h"
#include "LightDesc.h"
#include "StencilController.h"

using namespace std;

namespace Engine_I
{
	//ClassName   : World
	//LifeTime    : Initialized during graphic initialization, executed on each frame
	//Description : It is main container for all models rendered in engine. It represents flat collection of models mapped to specified "Name" property
	//				World should be the only connection to effect files used for models ( Fixed description is exception because they are not targeting to specified model ):
	//
	//				WorldCollection:                           EffectCollection ------ EffectController ( check "EffectController.h" for reference )
	//					  |											  |
	//					  +---Model 1 <-----> Effect 1 <--------------+<----- From file Effect1.fx
	//					  |											  |
	//					  +---Model 2 <-----> Effect 2 <--------------+<----- From file Effect2.fx
	//					  |											  |
	//					  +---Model 3 <-----> Effect 2 <--------------+<----- From file Effect2.fx
	//					  |										      |
	//					  +---Model 4 <-----> Effect 1 <--------------+<----- From file Effect1.fx
	//
	//			    Effect Files are used according to the graphic lifetime:
	//					Once per application initialization (CbFixed) - exceptionally outside World boundary class but in Graphic init routine ( check "AbstractEffectDesc.h" for reference )
	//					Once per frame (CbPerFrame) - UpdatePerFrame -- (check "AbstractEffectDesc.h" for reference)
	//					Once per object (UpdateAndRenderModel) is also final step of updating HLSL script files and rendering model -- (check "AbstractEffectDesc.h" for reference)
	//					Once per texture - UpdatePerTexture (it could be used Once Per Object, but for performance reason it was moved to other method )
	class World
	{

	public:

		World();
		~World();

		//Name		  : Init
		//Description : Initialization of instance. 
		//Input		  : effectController   - pointer to effect resources ( check "EffectController.h" for reference )
		//				device			   - pointer to Direct X device		
		//				mainTextInterface  - pointer to console ( chat window ) controller 
		//				stencilController  - pointer to stencil resources ( check "StencilController.h" for reference )
		//Output	  : true when no errors, else false
		bool Init(EffectController *effectController, ID3D10Device *device, MainTextInterface *mainTextInterface,  StencilController *stencilController);

		//Name		  : Init
		//Description : Render all models according to their model description and bound effect description
		//Output	  : true when no errors, else false
		bool RenderWorld();

		//Name		  : ShutDown
		//Description : ShutDown routine
		void ShutDown();

		//Name		  : AddWorld
		//Description : Add new model to world with bound effect 
		//Input		  : model		       - pointer to new model ( check "Model.h" for reference )
		//				modelDesc		   - pointer to model description ( check "ModelDesc.h" for reference )
		//				modelName		   - world is flat dictionary collection where mapping to concrete model is used by fixed model name 
		void AddWorld(Model *model, ModelDesc modelDesc, string effectName, string modelName);

		//Name		  : UpdateAndRenderModel
		//Description : Update effect bound to model with new CbPerObject effect description ( check "AbstractEffectDesc.h" for reference ). It also renders that model to back buffer
		//Input		  : effectDescription  - pointer to CbPerObject description ( check "AbstractEffectDesc.h" for reference )
		//				modelName		   - world is flat dictionary collection where mapping to concrete model is used by fixed model name 
		//Output	  : true when no errors, else false
		bool UpdateAndRenderModel(AbstractCbPerObjectDesc *effectDescription, string modelName);

		//Name		  : UpdatePerFrame
		//Description : Update effect bound to model with new CbPerFrame effect description ( check "AbstractEffectDesc.h" for reference ). 
		//Input		  : abstractCbPerFrameDesc - pointer to CbPerFrame description ( check "AbstractEffectDesc.h" for reference )
		void UpdatePerFrame(AbstractCbPerFrameDesc *abstractCbPerFrameDesc);
		
		//Name		  : UpdatePerTexture
		//Description : Update effect bound to model with new texture
		//Input		  : texture - pointer to new texture that would be bound to effect file
		void UpdatePerTexture(ID3D10ShaderResourceView *texture);

		//TODO: It should be pointer, not new collection !!
		//Name		  : GetModels
		//Description : Get whole collection of models 
		//Output	  : collection of models
		map<string, Model*>* GetModels();

		//World class gather also some information about models:
		int GetGlobalVertexCounter();	//number of vertices in all models bound to world
		int GetGlobalIndexCounter();	//number of indices in all models bound to world
		int GetGlobalFacesCounter();	//number of faces in all models bound to world
		int GetGlobalModelCounter();	//number of models in all models bound to world

	private:
		
		//Private version of world informations ( preserve updating from outside )
		int globalIndexCounter;		//number of vertices in all models bound to world
		int globalVertexCounter;	//number of indices in all models bound to world
		int globalFacesCounter;		//number of faces in all models bound to world
		int globalModelCounter;		//number of models in all models bound to world

		//Whole collection of models
		map<string, Model*> sModels;

		//Some storages:
		EffectController *effectController;			//all engine fx files
		MainTextInterface *mainTextInterface;		//main console(chat) window - check "MainTextInterface.h" for reference
		StencilController *stencilController;		//all stencil effects for this engine - check "StencilController.h" for reference

		//last rendered texture - this state is stored in the world from performance ( caching ) resons
		ID3D10ShaderResourceView *actualTexture;	

		//Pointer to Direct X device
		ID3D10Device *device;
	};
};

#endif