#ifndef __DRAWCONFIGURATION_H_
#define __DRAWCONFIGURATION_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining custon libraries
#include "EffectController.h"
#include "AllFixedDescriptions.h"
#include "MainTextInterface.h"
#include "trace.h"

namespace Engine_I
{
	//ClassName   : DrawConfiguration
	//LifeTime    : Initialized during graphic initialization, executed only once
	//Description : Initialize fixed environment variables and configurations, like fog
	class DrawConfiguration
	{
	public:
		DrawConfiguration();
		~DrawConfiguration();
		DrawConfiguration(const DrawConfiguration &other);

		DrawConfiguration& operator=(const DrawConfiguration &other);

		//Name		  : Init
		//Description : Initialize fixed environment variables and configurations, like fog. 
		//Input		  : effectController  - stores all effects interfaces
		//				device			  - pointer to main DX device
		//				mainTextInterface - pointer to main chat window
		//Output	  : true when no errors, else false
		bool Init(EffectController *effectController, ID3D10Device *device, MainTextInterface *mainTextInterface);
	};
};

#endif