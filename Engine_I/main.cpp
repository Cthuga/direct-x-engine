//Defining system libraries
#include <Windows.h>
#include <string>
#include <sstream>

//Defining custon libraries
#include "trace.h"
#include "window.h"

using namespace Engine_I;

//Main Window------------------------------------------------------------------------------------
int WINAPI WinMain ( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	try
	{
		//Recommendation:
		//Use Visual Studio 2010-2012 for this project

		//Description of all classes, methods and properties are in header files, when examining code I suggest routine described below:

		//1.Examine code during debugging starting from here
		//2.Most comments doesn't exists in method body. If You don't know what object,method or variable do, check 
		//  object,method,variable declaration in header file of inspected class [TIP : usefull shortcut in VS = CTRL + F12 ]
		//3.For fields and properties - if description in header file of inspected class is not enough, check header class definition for inspected property

		//Example:
		///*	
		//    ----> Debugging Graphic::Init
		//		  
		//		  [Step 1 - checking description of examined code]
		//		  Initialization routine 
		//		  For more info about initialized instances check their descriptions in graphic class
		//		  if(!d3XCore.Init(hwnd, width, height, device, swapChain, renderTargetView, depthStencilView, font, config.configGraphic.FullScreen))

		//		  [Step 2 - checking header class definition of graphic class]
		//		  D3XCore d3XCore;	//This is Direct X controller. Check D3XCore.h for reference

		//		  [Step 3 - checking header class definition of inspected property]
		//		  ClassName   : D3XCore
		//	      LifeTime    : Initialized during graphic initialization, shutduwn during closing application
		//		  Description : Direct X Core. Initializing all important Direct X interfaces

		//		  Name		  : Init
		//		  Description   : Initialization of DirectX components which doesn't change during resizing routine
		//		  Input		  : hwnd		     - handler to application
		//		  				width		     - width of the screen
		//		  			    height		     - height of the screen
		//		  				device		     - reference to device pointer. Parameter would be filled with new value
		//		  				swapChain	     - reference to swap chain pointer. Parameter would be filled with new value
		//		  			    renderTargetView - reference to render target view pointer. Parameter would be filled with new value
		//		  				depthStencilView - reference to depth stencil view pointer. Parameter would be filled with new value
		//		  				font			 - reference to direct x font interface pointer. Parameter would be filled with new value
		//		  				fullscreen		 - full screen flag
		//		  Output	      : true when no errors, else false
		//*/

		//Remember that debugger doesn't take steps into *.fx files !

		//One more important annotation:
		//Engine is separated for two parts :
		// - Core : that is almost everything with exception on :
		// - Drawings : that's part of game elements , not core engine. It would be later separated to different project
		//		actually for drawings there is :
		//			-DrawWorld : drawing models to scene
		//			-DrawTextInterface : draw text in each corner of the screen
		//			-DrawLight - draw lights to scene
		//			-DrawConfiguration - some fixed configurations dependent on game type

		//------------------------------------------------------------------------------------------------------------------------------------------------

		//Creating new window named as "Direct X"
		//This window would have DirectX BackBuffer bound to render graphic
		Window MainWindow(L"Direct X");

		int width = 800;
		int height = 600;

		//Some logs
		Trace::Log("+---------------------------------------------------------------+");
		Trace::Log("|                    STARTING APPLICATION                       |");
		Trace::Log("+---------------------------------------------------------------+");
		Trace::Log("");
		std::ostringstream tracer;
		tracer << "Screen width : " << width;
		Trace::Log(tracer.str());
		tracer.str("");
		tracer << "Screen height : " << height;
		Trace::Log(tracer.str());
		Trace::Log("Starting window");

		//At this point main window screen will be created, and then
		//
		//MORE IMPORTANT
		//
		//Everything else is also initialized during Graphic::Init execution
		//I warmly encourage for analysing of Graphic::Init
		bool result = MainWindow.Init();

		if(!result)
		{
			Trace::Error("Failed to Init Main Window");
			return -1;
		}

		//This metod contains rendering graphic, and peek message routine. As long as this method is running - graphic is shown.
		MainWindow.Run();

		//When application is closed or unexpected errors detected MainWindow.Run() would end
		
		//Shutdown Routine
		MainWindow.ShutDown();
	}
	catch(char* str)
	{
		Trace::Error(str);
		MessageBoxA(NULL, str, "Error", MB_ICONERROR | MB_OK);
	}

	return 0;
}