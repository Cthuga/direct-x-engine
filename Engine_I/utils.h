#ifndef __UTILS_H_
#define __UTILS_H_

//Defining system libraries
#include <string>
#include <algorithm>
#include <Windows.h>

using namespace std;

namespace Engine_I
{
	//String Methods------------------------------------------------------------------------

	//Name		  : PadLeft
	//Description : Pad left part of the string with specified char and fixed length
	//Input		  : s		- pointed reference to string that would change
	//				ch		- padding char
	//				length  - fixed padded length
	void PadLeft(string *s, char ch, int length);

	//Name		  : PadRight
	//Description : Pad right part of the string with specified char and fixed length
	//Input		  : s		- pointed reference to string that would change
	//				ch		- padding char
	//				length  - fixed padded length
	void PadRight(string *s, char ch, int length);

	//Name		  : trim_left_inplace
	//Description : Trim left part of the whitespace string
	//Input		  : s		- pointed string that would change
	//Output	  : trimmed string
	string& trim_left_inplace(std::string& s);

	//Name		  : trim_right_inplace
	//Description : Trim right part of the whitespace string
	//Input		  : s		- pointed string that would change
	//Output	  : trimmed string
	string& trim_right_inplace(std::string& s);

	//Name		  : trim
	//Description : Trim both part of the whitespace string
	//Input		  : s		- pointed string that would change
	//Output	  : trimmed string
	string& trim(string& s);

	//Name		  : ToLower
	//Description : Change characters in string to lowercase
	//Input		  : s		- string that would change
	//Output	  : lowercase string
	string ToLower(string s);

	//File Methods--------------------------------------------------------------------------

	//Name		  : GetFileNameFromPath
	//Description : Returns file-only part from full path
	//Input		  : filename - string that would change
	//Output	  : file path string
	string GetFileNameFromPath(string fileName);
};

#endif