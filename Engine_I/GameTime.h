#ifndef __GAMETIME_H_
#define __GAMETIME_H_

namespace Engine_I
{
	//ClassName   : GameTime
	//Description : It is description of actual time passed to other components
	class GameTime
	{
	public:
		GameTime();
		~GameTime();

		__int64 Ms;		//Miliseconds that passed 
		__int64 S;		//Seconds that passed 
		__int64 Min;	//Minutes that passed 
		__int64 H;		//Hours that passed 
		__int64 D;		//Days that passed 
		__int64 M;		//Months that passed 
		__int64 Y;		//Years that passed 
	
		bool SFlag;		//Flag Set when seconds were changed since last object refresh
		bool MSFlag;	//Flag Set when miliseconds were changed since last object refresh

		int FPS;		//Frames Per Second
	};
};

#endif