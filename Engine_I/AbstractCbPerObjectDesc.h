#ifndef __ABSTRACTCBPEROBBJECTDESC_H_
#define __ABSTRACTCBPEROBBJECTDESC_H_

//Defining system libraries
#include <windows.h>

//Defining custom libraries
#include "AbstractEffectDesc.h"

namespace Engine_I
{
	//StructName  : AbstractCbPerObjectDesc
	//Description : Check AbstractEffectDesc for reference
	class AbstractCbPerObjectDesc : AbstractEffectDesc
	{
	public:
		AbstractCbPerObjectDesc();
	};
};

#endif