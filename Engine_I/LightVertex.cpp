#include "LightVertex.h"

using namespace Engine_I;

Engine_I::LightVertex::LightVertex(D3DXVECTOR3 position, D3DXVECTOR3 normal, D3DXCOLOR diffuse, D3DXCOLOR specPow) : AbstractVertex()
{
	this->position = position;
	this->normal = normal;
	this->diffuse = diffuse;
	this->specPow = specPow;
}