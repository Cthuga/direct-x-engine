#ifndef __ABSTRACTCBFIXEDDESC_H_
#define __ABSTRACTCBFIXEDDESC_H_

//Defining system libraries
#include <Windows.h>

//Defining custom libraries
#include "AbstractEffectDesc.h"

namespace Engine_I
{
	//StructName  : AbstractEffectDesc
	//Description : Check AbstractEffectDesc for reference
	struct AbstractCbFixedDesc : AbstractEffectDesc
	{
	public:
		AbstractCbFixedDesc();
	};
};

#endif