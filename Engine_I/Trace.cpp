#include "trace.h"

using namespace Engine_I;

Engine_I::Trace::Trace()
{
	
}

Engine_I::Trace::~Trace()
{

}

string Engine_I::Trace::BaseTrace(string message, string fileName)
{
		ofstream output;

		time_t t = time(0);

		string header;
		string tempString;

		struct tm now;
		localtime_s(&now,&t);	

		ostringstream tempTimeString;
		ostringstream record;
		ostringstream filePath;

		tempTimeString <<  now.tm_mon + 1;
		tempString = tempTimeString.str();
		PadLeft(&tempString, '0', 2);
		tempTimeString.clear();
		tempTimeString.flush();
		tempTimeString.str("");

		record << "[" << (now.tm_year - 100) + 2000 << "_"  << tempString;
		tempString = "";

		tempTimeString << now.tm_mday;
		tempString = tempTimeString.str();
		PadLeft(&tempString, '0', 2);
		tempTimeString.clear();
		tempTimeString.flush();
		tempTimeString.str("");

		record << "_" << tempString;
		tempString = "";

		filePath << ".\\" << record.str() << "]" << fileName;

		tempTimeString << now.tm_hour;
		tempString = tempTimeString.str();
		PadLeft(&tempString, '0', 2);
		tempTimeString.clear();
		tempTimeString.flush();
		tempTimeString.str("");

		record << " " << tempString<< ":";
		tempString = "";

		tempTimeString << now.tm_min;
		tempString = tempTimeString.str();
		PadLeft(&tempString, '0', 2);
		tempTimeString.clear();
		tempTimeString.flush();
		tempTimeString.str("");

		record << tempString << ":";
		tempString = "";

		tempTimeString << now.tm_sec;
		tempString = tempTimeString.str();
		PadLeft(&tempString, '0', 2);
		tempTimeString.clear();
		tempTimeString.flush();
		tempTimeString.str("");

		record << tempString << "] ";
		tempString = "";
		record << message;

		output.open(filePath.str(), std::ios_base::app);
		output << record.str() << endl;
		output.close();

		return record.str();
}


string Engine_I::Trace::Log(string message)
{
	return Trace::BaseTrace(message, "DXTrace.txt");
}

string Engine_I::Trace::Error(string message)
{
	wstring wMsg = wstring(message.begin(), message.end());

	MessageBox(NULL, wMsg.c_str() , L"Error", MB_ICONERROR | MB_OK);

	return Trace::BaseTrace(message, "errors.txt");
}