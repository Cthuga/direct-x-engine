#ifndef __BASEEFFECTDESC_H_
#define __BASEEFFECTDESC_H_

//Defining system libraries
#include <Windows.h>

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining custom libraries
#include "AbstractCbPerObjectDesc.h"

namespace Engine_I
{
	//StructName  : CbPerObjectDesc
	//Description : Check AbstractEffectDesc for reference
	class CbPerObjectDesc : public AbstractCbPerObjectDesc
	{
	public:
		CbPerObjectDesc();
		CbPerObjectDesc::CbPerObjectDesc(D3DXMATRIX WVP);

		//World * View * Perspective transformation Matrix for model
		D3DXMATRIX WVP;

		//World only transformation Matrix for model
		D3DXMATRIX W;

		//Texture Adressing Type for model. At this moment available values:
		//1 - BORDER
		//2 - WRAP
		int texAdressingType;

		//Should D3DXCOLOR(0.0f, 0.0f, 0.0f, alpha) - BLACK be transparent ( clipped ) flag
		bool Clip;

		//Should fog be enabled for updated model flag
		bool Fog;
	};
};

#endif