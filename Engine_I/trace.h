#ifndef __TRACE_H_
#define __TRACE_H_

//Defining system libraries
#include <fstream>
#include <string>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <Windows.h>

//Defining custon libraries
#include "utils.h"

using namespace std;

namespace Engine_I
{
	//ClassName   : Trace
	//LifeTime    : Initialized when exec start - statiic singleton
	//Description : It logs informations passed by Log and Error method to file with schema:
	//				[DateTime] Message
	class Trace
	{

	public:

		Trace();
		~Trace();

		//Name		  : Log
		//Description : Log information to file located in executable file path
		//Input		  : message		       - message that would be logged to file
		static string Log(string message);

		//Name		  : Error
		//Description : Log information to file as an Error ( additional message about error bound to basic message )
		//Input		  : message		       - message that would be logged to file
		static string Error(string message);

	private:

		//Name		  : BaseTrace
		//Description : Base method that log information to specified file in parameter. It is used by public Log methods
		//Input		  : message		       - message that would be logged to file
		//				fileName		   - file where log would be appended
		static string BaseTrace(string message, string fileName);
	};
};

#endif