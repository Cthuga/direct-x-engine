#ifndef __MODEL_H_
#define __MODEL_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <string>
#include <list>

//Defining custom libraries
#include "AbstractEffect.h"
#include "ModelDesc.h"
#include "GameTime.h"
#include "trace.h"
#include "MainTextInterface.h"
#include "AbstractVertex.h"
#include "AbstractEffectDesc.h"
#include "cbPerObjectDesc.h"
#include "StencilController.h"
#include "ModelState.h"

using namespace std;

namespace Engine_I
{
	//ClassName   : Model
	//LifeTime    : Initialized during DrawWorld initialization, rendered on each frame
	//Description : This is base class for each rendered object. It has in it bound Effect class ( check AbstractEffect.h, AbstractEffectDesc.h and *.fx file for reference )
	//				The whole rendering model process description:
	//
	//				Graphic ( main class for Direct X process )
	//				  |
	//			   (has a)
	//				  |
	//				 \|/
	//				World ---(has a)---------> Model Collection --(has a)-> Single Model
	//				  |															 |
	//			   (has a)													  (has a)
	//				  |														     |
	//				 \|/														\|/
	//			 EffectController --(has a)--> Effects ---(has a)-------> Single Effect <-- communication with -->  *.fx file
	//											   ||
	//											 (is a)
	//											   \/
	//									      AbstractEffect
	//
	//			   Model has only implementation of rendering process. Description of indices and vertices and defining input layouts is job
	//			   of concrete Model classes, like Grid, Floor, Cube.
	class Model
	{
	public:

		Model();
		~Model();

		//This metod must be overriden in concrete classes

		//Name		  : Init
		//Description : Initialization of instance. 
		//Input		  : effect				- the effect that is bound to this model. (check also *.fx files and EffectController.h for more information )
		//				device			    - pointer to Direct X device
		//				mainTextInterface	- main console(chat) window - check "MainTextInterface.h" for reference
		//				name				- every model has unique name to identify it in World collection
		//				desc			    - description of model. Check "ModelDesc.h" for more info
		//				stencilController	- actually obsolette parameter TODO : delete it from here
		//Output	  : true when no errors, else false
		virtual bool Init(AbstractEffect *effect, ID3D10Device *&device, MainTextInterface *&mainTextInterface, string name, ModelDesc *desc, StencilController *stencilController);

		//Name		  : UpdateAndRender
		//Description : There are different stages of communication with GPU ( check for more info AbstractEffectDesc.h ) this is updating GPU effects per Object.
		//				Also this is final step that would render object to back buffer, no future changes are allowed during this frame after executing this method
		//Input		  : abstractEffectDesc - new values bound "Per Object" ( check AbstractEffectDesc.h for more info )
		//Output	  : true when no errors, else false
		bool UpdateAndRender(AbstractCbPerObjectDesc *abstractEffectDesc);

		//Name		  : UpdateTexture
		//Description : There are different stages of communication with GPU ( check for more info AbstractEffectDesc.h ) this is updating GPU effects per Texture.
		//Input		  : texture - new values bound "Per Texture"
		void UpdateTexture(ID3D10ShaderResourceView *texture);

		//Name		  : Update
		//Description : There are different stages of communication with GPU ( check for more info AbstractEffectDesc.h ) this is updating GPU effects per Object.
		//Input		  : abstractEffectDesc - new values bound "Per Object" ( check AbstractEffectDesc.h for more info )
		//Output	  : true when no errors, else false
		bool Update(AbstractCbPerObjectDesc *abstractEffectDesc);

		//Name		  : UpdateDescription
		//Description : Updates properties of this model ( for example change it to be a mirror )
		//Input		  : modelDesc - new description of model ( check ModelDesc.h for more info )
		//Output	  : true when no errors, else false
		bool UpdateDescription(ModelDesc modelDesc) ;

		//[value obsolette] TODO: delete in the future
		//Name		  : Update
		//Description : There are different stages of communication with GPU ( check for more info AbstractEffectDesc.h ) this is updating GPU effects per Object.
		//Input		  : abstractEffectDesc - new values bound "Per Object" ( check AbstractEffectDesc.h for more info )
		//				transformationMatrix - new position of model
		//Output	  : true when no errors, else false
		bool Update(AbstractCbPerObjectDesc *abstractEffectDesc, D3DXMATRIX *transformationMatrix);

		//This metod must be overriden in concrete classes

		//Name		  : Render
		//Description : Virtual method that renders model. It is virtual only for eventually extensions in concrete classes
		//Output	  : true when no errors, else false
		virtual bool Render();

		//This metod must be overriden in concrete classes

		//Name		  : ShutDown
		//Description : ShutDown routine
		virtual void ShutDown();

		//Name		  : Refresh
		//Description : Update GPU effect cbPerObject description with model cache value
		void Refresh();

		//The Type of Model, for example Cube
		string Type;

		//Unique name of Model
		string Name;

		//The topology attached to model. Usualy it is trianglelis, but for example for Grid it uses LineList
		D3D10_PRIMITIVE_TOPOLOGY Topology;

		//If set to true, then model uses Indexed buffer for render with better performance
		bool Indexed;

		//Name		  : AttachNewEffect
		//Description : If for some reason there must be attached dynamically new effect to model, this is solution
		//Input		  : effect - new effect attached to model
		void AttachNewEffect(AbstractEffect *effect);

		//Name		  : GetTechniqueName
		//Description : Get the name technique for attached effect to this model
		//Output	  : name of attached technique ( check *.fx files for reference )
		string GetTechniqueName();

		//Name		  : GetLastModelState
		//Description : It returns ModelState description. In model state there are cached some informations about model. Check "ModelState.h" for more info.
		//Output	  : Pointer to actually cached state of model
		ModelState* GetLastModelState();

		//Name		  : GetPerObjectDesc
		//Description : It returns values that were ( or they would ) attached to "cbPerObject" structure in effect ( check AbstractEffectDesc.h for reference )
		//Output	  : Structure that were (or it would ) be attached to "cbPerObject" structure in effect 
		CbPerObjectDesc* GetPerObjectDesc();

		//Every model, for statistics keeps some numbers:
		int GetIndicesCount();		//number of indices
		int GetVerticesCount();		//number of vertices
		int GetNumFaces();			//number of faces

		//For eventually collision detection, or reflection there are kept planes for every objects. Here is collection of planes for each wall
		list<D3DXPLANE> Planes;

		//[obsolette, don't touch!] TODO : to delete
		Model* _ref;

	protected:

		//These 2 variables are used for communication between CPU ( C++ ) and GPU ( HLSL *.fx files ). They are passed to Vertex Shader Stage in *.fx files
		ID3D10Buffer* vertexBuffer;	//here are informations about vertices for model 
		ID3D10Buffer* indexBuffer;	//here are information about indices for model   

		//Effect attached to Model ( check EffectController.h for reference )
		AbstractEffect* effect;

		//Actually stencil shouldn't be used at this stage. Obsolette value TODO : delete
		StencilController *stencilController;

		//Pointer to Direct X device
		ID3D10Device *device;

		//Actual value of game time ( check GameTime.h for reference )
		GameTime time;

		//Pointer to console ( chat window ) controller 
		MainTextInterface* textInterface;

		//Name		  : CreateNewBuffers
		//Description : This is helper method that fills vertexBuffer and indexBuffer. It is used by concrete classes, like Cube ( check Cube.h for references )
		//Input		  : vertices			 - array of vertice structure ( check for example TexVertex.h ) that describes concrete model
		//				indices				 - for performance reason - indices for vertices
		//				sizeofVerticesStruct - because communication between CPU and GPU must give information about size of structure, here is third argument
		//Output	  : true when no errors, else false
		bool CreateNewBuffers(AbstractVertex vertices[], int indices[], int sizeofVerticesStruct);

		//Actual cached model state. Check ModelState.h for more info
		ModelState modelState;

		//Every model, for statistics keeps some numbers:
		int strideLength;   //number of strides
		int numIndices;		//number of indices
		int numVertices;	//number of vertices
		int numFaces;		//number of faces

		//If there would be cricital error for this model in some random reasons. It would set faultet state to 1
		int faultedState;	

		//For eventually collision detection, or reflection there are kept planes for every objects. These planes are kept in "Planes" property. Unfortunately
		//when model would be transformated, then also Planes normal vectors must be transformated. BasePlanes is used as temporary value that keep last state
		//of plane position, so it can be transformed to new values correctly
		list<D3DXPLANE> BasePlanes;
	};
};

#endif