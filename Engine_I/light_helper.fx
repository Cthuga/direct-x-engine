//This is description of light ( check Check AbstractEffectDesc.h and LightDesc.h for reference )
struct Light
{
	//1 Vector - position of light
	float3 position;

	//2 Vector - dirrection of light source
	float3 dirrection;
	
	//3 Vector - light ambient
	float4 ambient;

	//4 Vector - specular color of light 
	float4 specular;

	//5 Vector - light diffuse ( material color * light diffuse = result color )
	float4 diffuse;

	//6 Vector [D3DXVECTOR(a,b,c)] => [light source]/attenuation = [light source]/(a*x^2 + b*x + c) 
	float3 attenuation;
	float range; // maximum range of light ( not used for parallel light );
	
	//7 Vector
	int type;		 //Type of light. A this moment available values are : 1 - parallel light ; 2 - point light ; 3 - spot light
	float spotPower; //Used for spot light to define specular radius ( spot power is power of gauss function ). Bigger SpotPower = smaller radius
};

//Helper structure that is passed from PixelShader Stage of effect file through light method described in this file
struct SurfaceInfo
{
	float3  pos;		//Position of vertex
	float3  normal;		//Vertex normal used with light angle to define color intensity
	float4  diffuse;	//color of material
	float4  specPow;	//Specular power of material
};

//Now there are implementation of some lights. At this moment there are 3 light types:
//Parrallel light - it doesn't have range, light covering whole area
//Point light - basic light which limited range
//SpotLight - light with specular effect

//Calculating pixel color through Parallel Light
float3 ParrallelLight(SurfaceInfo sv, Light L, float3 eye)
{
	float3 outColor = float3(0.0f, 0.0f, 0.0f);
	float3 lightVec = -L.position;
	
	float4 outColor4 = sv.diffuse * L.ambient;
	outColor += float3(outColor4.x, outColor4.y, outColor.z);

	float diffuseFactor = dot(lightVec, sv.normal);

	if(diffuseFactor > 0)
	{
		float specPower = max(sv.specPow.a, 1.0f);
		float3 toEye = normalize(eye - sv.pos);
		float3 R = reflect(-lightVec, sv.normal);
		float specFactor = pow(max(dot(R, toEye), 0.0f), specPower);

		outColor4 = diffuseFactor * sv.diffuse * L.diffuse;
		outColor += float3(outColor4.x, outColor4.y, outColor4.z);

		outColor4 = specFactor * sv.specPow * L.specular;
		outColor += float3(outColor4.x, outColor4.y, outColor4.z);
	}

	return outColor;
}

//Calculating pixel color through Point Light
float3 PointLight(SurfaceInfo sv, Light L, float3 eye)
{
	float3 outColor = float3(0.0f, 0.0f, 0.0f);
	float3 lightVec = L.position - sv.pos;
	float d = length(lightVec);

	if(d > L.range) 
	{
		return float3(0.0f, 0.0f, 0.0f);
	}

	lightVec /= d;

	float4 outColor4 = sv.diffuse * L.ambient;
	outColor += float3(outColor4.x, outColor4.y, outColor4.z);

	float diffuseFactor = dot(lightVec, sv.normal);

	if(diffuseFactor > 0)
	{
		float specPower = max(sv.specPow.a, 1.0f);
		float3 toEye = normalize(eye - sv.pos);
		float3 R = reflect(-lightVec, sv.normal);
		float specFactor = pow(max(dot(R, toEye), 0.0f), specPower);

		outColor4 = diffuseFactor * sv.diffuse * L.diffuse;
		outColor += float3(outColor4.x, outColor4.y, outColor4.z);
		outColor4 = specFactor * sv.specPow * L.specular;
		outColor += float3(outColor4.x, outColor4.y, outColor4.z);
	}

	return outColor / dot ( L.attenuation, float3(1.0f, d, d*d)) ;
}

//Calculating pixel color through Spot Light
float3 SpotLight(SurfaceInfo sv, Light L, float3 eye)
{
	float3 outColor = PointLight(sv, L, eye);

	float3 lightVec = normalize(L.position - sv.pos);

	float s = pow( max( dot( -lightVec, L.dirrection ) , 0.0f), L.spotPower);

	return outColor * s;
}