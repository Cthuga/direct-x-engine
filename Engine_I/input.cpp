#include "input.h"

using namespace Engine_I;

Engine_I::Input::Input()
{
	for(int i = 0; i < 256; i++)
	{
		keys[i] = false;
	}
}

Engine_I::Input::~Input()
{

}

void Engine_I::Input::ResetKeys()
{
	for(int i = 0; i < 256; i++)
	{
		keys[i] = false;
	}
}

void Engine_I::Input::KeyDown(unsigned int key)
{
	keys[key] = true;
}

void Engine_I::Input::KeyUp(unsigned int key)
{
	keys[key] = false;
}

bool Engine_I::Input::IsKeyDown(unsigned int key)
{
	return keys[key];
}
