#ifndef __ACTOR_H_
#define __ACTOR_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <string>
#include <sstream>
#include <algorithm>  

//Defining custon libraries
#include "GameTime.h"
#include "input.h"
#include "config.h"
#include "utils.h"

using namespace std;

namespace Engine_I
{
	//ClassName   : Actor
	//LifeTime    : Initialized during graphic initialization, executed on each frame
	//Description : This concept is still under development stage ( I am open for ideas ! ). Actor would be container of AI ( player would be also treated as AI ).
	//				It would be also base class of implementation "decision interface". Decision Interface would be set of limited methods that actor can do.
	//				At this momment this is almost "Mock" class with some base implementation of movement for player.
	class Actor
	{
	public:
		Actor();
		~Actor();

		//Name		  : Init
		//Description : Initialization of instance. 
		//Input		  : config - because Player might controll actor in same way like AI would, we must pass keyboard configuration for pressing some keys. Check "engine_ini.h" and "config_input.h" for reference.
		void Init(Config config);

		//Name		  : Action
		//Description : Action is executed once per frame. It is nothing more like checking decisions and delegate logic to method executing that decision. At this moment it is simply, assynchronic checking of pressed key. Even not taken from Config file !
		//Input		  : gameTime - some decision might be dependent on time, so actual game time is passed to this method
		//				iInput - here pressed keys would be passed in the future. ( Check "input.h" for reference )
		void Action(GameTime gameTime, Input iInput);

		//Some example decisions:
		void MoveForward();		//Move actor forward
		void MoveBackward();	//Move actor backward
		void StrafeLeft();		//Strafe actor left
		void StrafeRight();		//Strafe actor right

		//Because moving Player camera would change transformation matrix View, there is additional logic:

		//Name		  : GetView
		//Description : Get actual View transformation matrix
		D3DXMATRIX GetView();

		//Name		  : GetCoordinates
		//Description : Get actual position of camera
		D3DXVECTOR3 GetCoordinates();

		//Actual position of camera 
		//TODO : it should be private
		D3DXVECTOR3 eye;

	private:

		//Some decision might be dependent on time, so here is stored actual game time
		GameTime time;

		//Some decision might be dependent on key pressed, so here is stored actual state of key pressed
		Input input;

		//Field with configured input ( check "engine_ini.h" and "config_input.h" for reference )
		Config config;

		//When we define new camera, we have to point to "up" vector describing where is up and where is down. In actor class Init method, we define 
		//new up vector. Actually this field is not used, but when "up" vector would be needed, this is the place where we can find it.
		D3DXVECTOR3 up;

		//Same like up vector - this value isn't actually externally used, but it would. It points the place on which camera looks.
		D3DXVECTOR3 target;

		//Actual value of transformation View Matrix
		D3DXMATRIX V;
	};
};

#endif