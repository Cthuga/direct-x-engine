DepthStencilState MirrorS1
{
	DepthEnable = true;
	DepthWriteMask = All;
	DepthFunc = Less;
	StencilEnable = true;
	StencilReadMask = 0xff;
	StencilWriteMask = 0xff;
	FrontFaceStencilFunc = Always;
	FrontFaceStencilPass = Replace;
	FrontFaceStencilFail = Keep;
	FrontFaceStencilDepthFail = Keep;
	BackFaceStencilFunc = Always;
	BackFaceStencilPass = Replace;
	BackFaceStencilFail = Keep;
	BackFaceStencilDepthFail = Keep;
};

DepthStencilState MirrorS2
{
	DepthEnable = true;
	DepthWriteMask = Zero;
	DepthFunc = Always;
	StencilEnable = true;
	StencilReadMask = 0xff;
	StencilWriteMask = 0xff;
	FrontFaceStencilFunc = Equal;
	FrontFaceStencilPass = Keep;
	FrontFaceStencilFail = Keep;
	FrontFaceDepthFail = Keep;
	BackFaceStencilFunc = Equal;
	BackFaceStencilPass = Keep;
	BackFaceStencilFail = Keep;
	BackFaceDepthFail = Keep;
};