#ifndef __D3XCORE_H_
#define __D3XCORE_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining custon libraries
#include "trace.h"

namespace Engine_I
{
	//ClassName   : D3XCore
	//LifeTime    : Initialized during graphic initialization, shutduwn during closing application
	//Description : Direct X Core. Initializing all important Direct X interfaces
	class D3XCore
	{

	public:
		D3XCore();
		~D3XCore();

		//Name		  : Init
		//Description : Initialization of DirectX components which doesn't change during resizing routine
		//Input		  : hwnd		     - handler to application
		//				width		     - width of the screen
		//			    height		     - height of the screen
		//				device		     - reference to device pointer. Parameter would be filled with new value
		//				swapChain	     - reference to swap chain pointer. Parameter would be filled with new value
		//			    renderTargetView - reference to render target view pointer. Parameter would be filled with new value
		//				depthStencilView - reference to depth stencil view pointer. Parameter would be filled with new value
		//				font			 - reference to direct x font interface pointer. Parameter would be filled with new value
		//				fullscreen		 - full screen flag
		//Output	  : true when no errors, else false
		bool Init(HWND hwnd, int width, int height, ID3D10Device*& device, IDXGISwapChain*& swapChain, ID3D10RenderTargetView*& renderTargetView, ID3D10DepthStencilView*& depthStencilView, ID3DX10Font*& font, bool fullscreen);
		
		//Name		  : CommonInit
		//Description : Initialization Direct X components which change during resizing routine
		//Input		  : hwnd		     - handler to application
		//				width		     - width of the screen
		//			    height		     - height of the screen
		//				device		     - reference to device pointer. Parameter would be filled with new value
		//				swapChain	     - reference to swap chain pointer. Parameter would be filled with new value
		//			    renderTargetView - reference to render target view pointer. Parameter would be filled with new value
		//				depthStencilView - reference to depth stencil view pointer. Parameter would be filled with new value
		//				font			 - reference to direct x font interface pointer. Parameter would be filled with new value
		//				fullscreen		 - full screen flag
		//Output	  : true when no errors, else false
		bool CommonInit(HWND hwnd, int width, int height, ID3D10Device*& device, IDXGISwapChain*& swapChain, ID3D10RenderTargetView*& renderTargetView, ID3D10DepthStencilView*& depthStencilView, ID3DX10Font*& font, bool fullscreen);

	private:

	};
};

#endif