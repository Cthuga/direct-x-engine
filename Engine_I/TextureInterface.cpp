#include "TextureInterface.h"

using namespace Engine_I;

Engine_I::TextureInterface::TextureInterface()
{
	this->device = 0;
	this->mainTextInterface = 0;
}

Engine_I::TextureInterface::~TextureInterface()
{

}

bool Engine_I::TextureInterface::Init(ID3D10Device *device, MainTextInterface *mainTextInterface)
{
	this->device = device;
	this->mainTextInterface = mainTextInterface;

	return true;
}

bool Engine_I::TextureInterface::AddTextureFromFile(string filename)
{
	wstring wFileName = wstring(filename.begin(), filename.end());
	string texName = filename.replace(0, 11, "");

	HRESULT result = D3DX10CreateShaderResourceViewFromFile(device, wFileName.c_str(), 0, 0, &textures[texName], 0);
	if(FAILED(result))
	{
		ostringstream error;
		error << "Failed to load Texture : " << texName;
		mainTextInterface->Warning(error.str());
		Trace::Error(error.str());

		return false;
	}

	return true;
}

void Engine_I::TextureInterface::ShutDown()
{
	map<string, ID3D10ShaderResourceView*>::iterator it;

	for(it = textures.begin() ; it != textures.end() ; it ++)
	{
		if(it->second)
		{
			it->second->Release();
			it->second = 0;
		}
	}
}

ID3D10ShaderResourceView* Engine_I::TextureInterface::GetTexture(string name)
{
	if(textures[name])
	{
		return textures[name];
	}

	ostringstream os;
	os << "Nie udalo sie pobrac tekstury z bufora : " << name;
	Trace::Log(os.str());
	mainTextInterface->Warning(os.str());

	return 0;
}