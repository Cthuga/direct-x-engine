#ifndef __STENCILCONTROLLER_H_
#define __STENCILCONTROLLER_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining custon libraries
#include "MainTextInterface.h"
#include "trace.h"
#include "RasterController.h"

namespace Engine_I
{
	//ClassName   : StencilController
	//LifeTime    : Initialized during graphic initpialization, executed during rendering some models with special Stencil Effects for each frame
	//Description : Stencil is optional Buffer where we can prepare some special effects, like reflecting only partial elements of model. ( Check "MirrorController.h" ).
	//				StencilController stores all preared StencilEffects for this engine.
	class StencilController
	{
	public:

		StencilController();
		~StencilController();

	private:

		StencilController(const StencilController& other);
		StencilController& operator=(const StencilController& other);

	public:

		//Name		  : Init
		//Description : Initialization of RasterController. During initialization all Raster effects are prepared
		//Input		  : device		    	-  pointer to Direct X device
		//				mainTextInterface   -  pointer to console ( chat window ) controller 
		//				rasterController	-  container of all rasterizer effects ( like BackFaceCulling ). For mirrored models writing vertices have Reversed Order, so RasterizerController is used to change it
		//Output	  : true when no errors, else false
		bool Init(ID3D10Device *device, MainTextInterface *MainTextInterface, RasterController *rasterController);

		//Name		  : ShutDown
		//Description : ShutDown routine
		void ShutDown();

		//Here are prepared StencilEffects:
		ID3D10DepthStencilState *stateCreateNewStencil;				//When rendering models with this stencil effect to RenderTargetView ( back buffer ) , set corresponding Pixel in Stencil Buffer for future use
		ID3D10DepthStencilState *stateWriteToExistingStencilOnly;	//When rendering model with this stencil effect to RenderTargetView ( back buffer ) , render only to pixel, which has corresponding Pixel in Stencil Buffer ( check stateCreateNewStencil )
		ID3D10DepthStencilState *stateCreateMirror;					//Obsolette effect - I'm still working on Mirroring, so this value will stay

	private:

		//These helper methods are used in Init stage:

		//Name		  : StateCreateNewStencil
		//Description : Fill "stateCreateNewStencil" with new value
		//Output	  : true when no errors, else false
		bool StateCreateNewStencil();

		//Name		  : StateWriteToExistingStencilOnly
		//Description : Fill "stateWriteToExistingStencilOnly" with new value
		//Output	  : true when no errors, else false
		bool StateWriteToExistingStencilOnly();

		//Name		  : CreateMirror
		//Description : Fill "stateCreateMirror" with new value
		//Output	  : true when no errors, else false
		bool CreateMirror();

	private:

		//Pointer to Direct X Device
		ID3D10Device *device;

		//Pointer to console ( chat window ) controller 
		MainTextInterface *mainTextInterface;	

		//Container of all rasterizer effects ( like BackFaceCulling ). For mirrored models writing vertices have Reversed Order, so RasterizerController is used to change it
		RasterController *rasterController;
	};
};

#endif