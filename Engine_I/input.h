#ifndef __INPUT_H_
#define __INPUT_H_

//Defining system libraries
#include <Windows.h>

namespace Engine_I
{
	//ClassName   : Input
	//LifeTime    : Initialized during graphic initialization, executed on each frame 
	//Description : This class stores all keys pressed from keyboard. Actually it's replaced with some aynchronous methods ( check actor.h for reference ) but it would
	//				be main class for controlling keys in the future
	class Input
	{
	public:
		Input();
		~Input();

		//Name		  : ResetKeys
		//Description : Reset state for all keys. Used during object initialization
		void ResetKeys();

		//Name		  : KeyDown
		//Description : This method is used by window to register state of Key Down
		//Input		  : key - code of pressed key
		void KeyDown(unsigned int key);

		//Name		  : KeyUp
		//Description : This method is used by window to register state of Key Up
		//Input		  : key - code of pressed key
		void KeyUp(unsigned int key);

		//Name		  : IsKeyDown
		//Description : This method check state of key 
		//Input		  : key - code of key which status would be checked
		//Output	  : true if key is pressed else false
		bool IsKeyDown(unsigned int key);

	private:

		//The collection of key states
		bool keys[256];
	};
};

#endif