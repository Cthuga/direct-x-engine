#ifndef __BASEEFFECT_H_
#define __BASEEFFECT_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining custon libraries
#include "AbstractEffect.h"
#include "cbPerObjectDesc.h"
#include "AbstractCbFixedDesc.h"
#include "AbstractCbPerFrameDesc.h"
#include "AbstractCbPerObjectDesc.h"

namespace Engine_I
{
	//ClassName   : BaseEffect
	//LifeTime    : Initialized during EffectController initialization - for "basic.fx" effect file
	//Description : This is concrete implementation of "AbstractEffect.h" file. It doesn't has lights and textures. Main purpouse is to implement 
	//				editor interface models, like Grid, or some Vectors. For more info check "AbstractEffect.h" reference
	class BaseEffect : public AbstractEffect
	{
	public:
		BaseEffect();
		~BaseEffect();

		//Name		  : Init
		//Description : Initialize instance, and load "basic.fx" effect file
		//Input		  : mInterface - main console(chat) window - check "MainTextInterface.h" for reference
		//				cbFixed - every Effect has initialized their Fixed variables. See "EffectController.h", "AbstractEffectDesc.h" and "World.h" for reference
		//Output	  : true when no errors, else false
		bool Init(MainTextInterface* mInterface, ID3D10Device *device, AbstractCbFixedDesc *cbFixed);
		
		//Name		  : UpdateFixed
		//Description : Fixed updating of effect file
		//Input		  : abstractCbFixedDesc - polymorphed to abstraction, concrete CbFixed description ( check "AbstractEffectDesc.h" for reference )
		void UpdateFixed(AbstractCbFixedDesc* abstractCbFixedDesc);
		
		//Name		  : UpdatePerObject
		//Description : Updating once for each object effect file
		//Input		  : abstractCbPerObjectDesc - polymorphed to abstraction, concrete CbPerObject description ( check "AbstractEffectDesc.h" for reference )
		void UpdatePerObject(AbstractCbPerObjectDesc *abstractCbPerObjectDesc);
		
		//Name		  : UpdatePerFrame
		//Description : Updating once for each frame effect file
		//Input		  : abstractCbPerFrameDesc - polymorphed to abstraction, concrete CbPerFrame description ( check "AbstractEffectDesc.h" for reference )
		void UpdatePerFrame(AbstractCbPerFrameDesc *abstractCbPerFrameDesc);
		
		//Name		  : UpdatePerTexture
		//Description : Updating once for each texture effect file
		//Input		  : texture - Direct X texture interface
		void UpdatePerTexture(ID3D10ShaderResourceView* texture);
	
		//Name		  : ShutDown
		//Description : ShutDown routine
		void ShutDown();
	};
};

#endif