#ifndef __CUBE_H_
#define __CUBE_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <list>

//Defining custom libraries
#include "Model.h"
#include "TexVertex.h"
#include "trace.h"
#include "MainTextInterface.h"
#include "GameTime.h"
#include "cbPerObjectDesc.h"
#include "ModelDesc.h"
#include "cbPerFrameDesc.h"

namespace Engine_I
{
	//ClassName   : Cube
	//LifeTime    : Initialized during DrawWorld initialization, rendered on each frame
	//Description : This is definition for Cube. Check Model.h for more info
	class Cube : public Model
	{
	public:
		Cube();
		~Cube();

		//Name		  : Init
		//Description : Initialization of vertices and indices for Cube definition. 
		//Input		  : effect				- the effect that is bound to this model. (check also *.fx files and EffectController.h for more information )
		//				device			    - pointer to Direct X device
		//				mainTextInterface	- main console(chat) window - check "MainTextInterface.h" for reference
		//				name				- every model has unique name to identify it in World collection
		//				desc			    - description of model. Check "ModelDesc.h" for more info
		//				stencilController	- actually obsolette parameter TODO : delete it from here
		//Output	  : true when no errors, else false
		bool Init(AbstractEffect *effect, ID3D10Device *&device, MainTextInterface *&mainTextInterface, string name, ModelDesc *desc, StencilController *stencilController);

		//Name		  : Render
		//Description : Method that renders cube. 
		//Output	  : true when no errors, else false
		bool Render();

		//Name		  : Render
		//Description : ShutDown routine
		void ShutDown();
	};
};

#endif