#ifndef __LIGHTDESC_H_
#define __LIGHTDESC_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <windows.h>

namespace Engine_I
{
	//StructName  : CbPerFrameDesc
	//Description : Check AbstractEffectDesc for reference
	struct LightDesc
	{
	public:
		LightDesc();

		//1 Vector - position of light
		D3DXVECTOR3 Position;
		float __pad1;

		//2 Vector - dirrection of light source
		D3DXVECTOR3 Dirrection;
		float __pad0;

		//3 Vector - light ambient
		D3DXCOLOR Ambient;

		//4 Vector - specular color of light 
		D3DXCOLOR Specular;

		//5 Vector - light diffuse ( material color * light diffuse = result color )
		D3DXCOLOR Diffuse;

		//6 Vector [D3DXVECTOR(a,b,c)] => [light source]/attenuation = [light source]/(a*x^2 + b*x + c) 
		D3DXVECTOR3 Attenuation;
		float Range; // maximum range of light ( not used for parallel light );

		//7 Vector
		int Type;			//Type of light. A this moment available values are : 1 - parallel light ; 2 - point light ; 3 - spot light
		float SpotPower;    //Used for spot light to define specular radius ( spot power is power of gauss function ). Bigger SpotPower = smaller radius
		float __pad2;
		float __pad3;
	};
};

#endif