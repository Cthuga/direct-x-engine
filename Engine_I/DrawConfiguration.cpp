#include "DrawConfiguration.h"

using namespace Engine_I;

Engine_I::DrawConfiguration::DrawConfiguration()
{

}

Engine_I::DrawConfiguration::~DrawConfiguration()
{

}

Engine_I::DrawConfiguration::DrawConfiguration(const DrawConfiguration &other)
{

}

DrawConfiguration& Engine_I::DrawConfiguration::operator=(const DrawConfiguration &other)
{
	return DrawConfiguration(other);
}

bool Engine_I::DrawConfiguration::Init(EffectController *effectController, ID3D10Device *device, MainTextInterface *mainTextInterface)
{
	AllFixedDescriptions allFixedDescription;
	allFixedDescription.Fog.gFogRange = 170.0f;
	allFixedDescription.Fog.gFogStart = 4.0f;
	allFixedDescription.Fog.gFogColor = D3DXVECTOR3(0.7f, 0.7f, 0.7f);

	if(!effectController->Init(mainTextInterface, device, allFixedDescription))
	{
		Trace::Error("Failed to initialize effectController");
		mainTextInterface->Warning("Failed to initialize effectController");
		return false;
	}

	return true;
}