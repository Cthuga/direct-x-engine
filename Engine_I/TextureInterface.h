#ifndef __TEXTUREINTERFACE_H_
#define __TEXTUREINTERFACE_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <string>
#include <sstream>
#include <map>

//Defining custon libraries
#include "utils.h"
#include "MainTextInterface.h"
#include "trace.h"

using namespace std;

namespace Engine_I
{
	//ClassName   : TextInfoInterface
	//LifeTime    : Initialized during graphic initialization, executed on each frame
	//Description : It is container for textures. It hides Direct X logic. Specified path for texture uses UNC path (//Textures)
	class TextureInterface
	{
	public:
		TextureInterface();
		~TextureInterface();

		//Name		  : Init
		//Description : Initialization of instance
		//Input		  : device		       - pointer to Direct X device
		//				mainTextInterface  - pointer to console ( chat window ) controller 
		//Output	  : true when no errors, else false
		bool Init(ID3D10Device *device, MainTextInterface *mainTextInterface);

		//Name		  : AddTextureFromFile
		//Description : Add texture to container from specified path ( transformation to UNC path //Textures )
		//Input		  : filename - path to texture 
		//Output	  : true when no errors, else false
		bool AddTextureFromFile(string filename);

		//Name		  : ShutDown
		//Description : ShutDown routine
		void ShutDown();

		//Name		  : GetTexture 
		//Description : Get texture from specified UNC path ( starting from //Textures )
		//Input		  : name - UNC path to texture 
		//Output	  : true when no errors, else false
		ID3D10ShaderResourceView* GetTexture(string name);

	private:

		//Container of all textures ( stored as UNC path //Textures )
		map<string, ID3D10ShaderResourceView*> textures;

		//Pointer to Direct X device
		ID3D10Device *device;

		//Pointer to console(chat) window
		MainTextInterface *mainTextInterface;
	};
};

#endif