#ifndef __CONFIG_GRAPHIC_H_
#define __CONFIG_GRAPHIC_H_

namespace Engine_I
{
	//ClassName   : ConfigGraphic
	//LifeTime    : As long as "engine_ini.h" lifetime. 
	//Description : It stores all values from "*.ini" configuration file - Graphic Section. For some more info check "engine_ini.h" and "config.h" reference.
	class ConfigGraphic
	{
	public:
		ConfigGraphic();
		~ConfigGraphic();

		//Full Screen Flag. When Set - application would start in Full Screen mode
		bool FullScreen;

		//Screen Width - application would start with specified screen width
		int ScreenWidth;

		//Screen Height - application would start with specified screen height
		int ScreenHeight;
	};
};

#endif