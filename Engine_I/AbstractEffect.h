#ifndef __ABSTRACTEFFECT_H_
#define __ABSTRACTEFFECT_H_

//When variable __DEBUG is defined in preprocessor, then all warnings and errors should be outputted from effect script files
#ifdef __DEBUG
	#define HLSL_FLAGS D3D10_SHADER_DEBUG | D3D10_SHADER_SKIP_OPTIMIZATION
#else
	#define HLSL_FLAGS
#endif

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <string>

//Defining custon libraries
#include "MainTextInterface.h"
#include "trace.h"
#include "AbstractEffectDesc.h"
#include "AbstractCbFixedDesc.h"
#include "AbstractCbPerObjectDesc.h"
#include "AbstractCbPerFrameDesc.h"

using namespace std;

namespace Engine_I
{
	//ClassName   : AbstractEffect
	//LifeTime    : Initialized during EffectController initialization - once for each effect file
	//Description : Every effect file has been mapped to Concrete Effect class. Every concrete effect class derives from AbstractEffect.
	//
	//						AbstractEffect   <------- EffectControllerCollection <--------- Effect Controller
	//						     ||
	//							 \/
	//              +-------------+-------------+
	//				|			  |			    |
	//		Concrete Effect 1 Concrete Effect 2 Concrete Effect 3
	//
	//				For more info about controlling effects in project see "EffectController.h" and then "World.h"
	class AbstractEffect
	{
	public:

		AbstractEffect();
		~AbstractEffect();

		//DirectX Effect Interfaces - rules of binding effects to Models are described in "World.h"
		//
			//For each Concrete Effect there is mapped Dirrect X effect interface
			ID3D10Effect* effect;

			//Each effect has only one effect technique - for simplier access Abstract Effect has got pointer to Direct X effect technique
			ID3D10EffectTechnique* technique;
		
			//Name		  : GetInputLayout
			//Description : Each effect has also only one Input Layout ( check Model.h, concrete models, and ModelDesc.h for more info ). Like for technique, this property 
			//				was created to simplify access
			ID3D10InputLayout* GetInputLayout();
		//
		//End of DirectX Effect Interfaces

		//Name		  : Init
		//Description : Initialize instance, and load effect file from parameter "fileName" - so it hides some Direct X routine for derived classes
		//Input		  : fileName - name of effect File. Effect files are located in application exec dirrectory at this momment
		//				mInterface - main console(chat) window - check "MainTextInterface.h" for reference
		//				techniqueName - name of technique used in effect file
		//				device - pointer to Direct X device
		//				cbFixed - every Effect has initialized their Fixed variables. See "EffectController.h", "AbstractEffectDesc.h" and "World.h" for reference
		//Output	  : true when no errors, else false
		bool Init(wstring fileName, MainTextInterface *mInterface, string techniqueName, ID3D10Device *device, AbstractCbFixedDesc *cbFixed);

		//Updating Interface
		//Implementation of these virtual methods are in derived class only 

		//Effect Controller makes updates for each attached Concrete Effect. For more info check "EffectController.h" and "World.h"

		//Name		  : UpdateFixed
		//Description : Fixed updating of effect file
		//Input		  : abstractCbFixedDesc - polymorphed to abstraction, concrete CbFixed description ( check "AbstractEffectDesc.h" for reference )
		virtual void UpdateFixed(AbstractCbFixedDesc* abstractCbFixedDesc);				

		//Name		  : UpdatePerObject
		//Description : Updating once for each object effect file
		//Input		  : abstractCbPerObjectDesc - polymorphed to abstraction, concrete CbPerObject description ( check "AbstractEffectDesc.h" for reference )
		virtual void UpdatePerObject(AbstractCbPerObjectDesc *abstractCbPerObjectDesc);	

		//Name		  : UpdatePerFrame
		//Description : Updating once for each frame effect file
		//Input		  : abstractCbPerFrameDesc - polymorphed to abstraction, concrete CbPerFrame description ( check "AbstractEffectDesc.h" for reference )
		virtual void UpdatePerFrame(AbstractCbPerFrameDesc *abstractCbPerFrameDesc);	

		//Name		  : UpdatePerTexture
		//Description : Updating once for each texture effect file
		//Input		  : texture - Direct X texture interface
		virtual void UpdatePerTexture(ID3D10ShaderResourceView* texture);				

		//Shut Down Interface
		//Implementation of this virtual method is in derived class only

		//Name		  : ShutDown
		//Description : ShutDown routine
		virtual void ShutDown();

	protected:
		
		//Pointer to Direct X device
		ID3D10Device* device;

		//main console(chat) window - check "MainTextInterface.h" for reference
		MainTextInterface* mainInterface;

		//Each effect has also only one Input Layout ( check Model.h, concrete models, and ModelDesc.h for more info ). Like for technique, this property 
		//was created to simplify access
		ID3D10InputLayout* inputLayout;
	};
};

#endif