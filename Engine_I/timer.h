#ifndef __TIMER_H_
#define __TIMER_H_

//Defining system libraries
#include <Windows.h>
#include <math.h>

//Defining custon libraries
#include "GameTime.h"

using namespace std;

namespace Engine_I
{
	//ClassName   : Timer
	//LifeTime    : Initialized during initialization of WinMain, deleted during closing application
	//Description : Class responsible for logic of game time. Value of actual time is passed by GameTime class ( check GameTime.h for reference ).
	//				For synchronization reason to calculate time there is used High Definition Query Performance Counter.
	class Timer
	{
	public:
		Timer();
		~Timer();

		//Name		  : Reset
		//Description : Resets timer
		void Reset();

		//Name		  : Tick
		//Description : Updating value of timer
		void Tick();

		//Name		  : Pause
		//Description : When executed, Tick won't update timer
		void Pause();

		//Name		  : UnPause
		//Description : When executed, Tick would update timer again
		void UnPause();

		//Name		  : GetGameTime
		//Description : Get actual value of time stored in GameTime class ( check GameTime.h for reference )
		//Output	  : Actual Game Time ( check GameTime.h for reference )
		GameTime GetGameTime();

	private:

		//Actual Game Time ( check GameTime.h for reference )
		GameTime gameTime;

		//Actual frequency
		__int64 frequency;

		//Actual value of High Definition Query Performance Counter 
		__int64 curCounter;

		//Previous value of High Definition Query Performance Counter
		__int64 prevCounter;

		//curCounter - prevValue - used for time calculation
		__int64 delta;

		//This value gather delta value from each tick - used for time calculation
		__int64 counter;

		//This is temporary value from counter - value is reset each time when new millisecond arise
		__int64 tempCounter;

		//Flag set, when game is paused ( Pause method is executed )
		bool pause;
	};
};

#endif