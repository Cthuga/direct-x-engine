#include "ModelDesc.h"

using namespace Engine_I;

Engine_I::ModelDesc::ModelDesc()
{
	ZeroMemory(this, sizeof(ModelDesc));

	BlendState = 0;
	Mirror = 0;
	Mirrored = 1;
	ReflectedState = 0;
	
	BlendFactor[0] = 0.0f;
	BlendFactor[1] = 0.0f;
	BlendFactor[2] = 0.0f;
	BlendFactor[3] = 0.0f;
}

Engine_I::ModelDesc::~ModelDesc()
{

}