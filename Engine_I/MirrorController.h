#ifndef __MIRRORCONTROLLER_H_
#define __MIRRORCONTROLLER_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <list>
#include <string>

//Defining custon libraries
#include "MainTextInterface.h"
#include "trace.h"
#include "World.h"
#include "ModelDesc.h"
#include "StencilController.h"
#include "BlendController.h"
#include "DrawLights.h"
#include "LightDesc.h"
#include "cbPerFrameDesc.h"

namespace Engine_I
{
	//ClassName   : MirrorController
	//LifeTime    : Initialized during graphic initialization, executed after rendering all models, for each frame
	//Description : This class is responsible for mirror behaviour of textures, when Model has property "Mirror" set to "True" ( check "ModelDesc.h" for reference )
	//
	//							[Stage 1]
	//			  Initialization of All Models and Lights
	//							   |
	//							[Stage 2]
	//		 Calculate New Positions of Models ( Except Mirror Models )
	//							   |
	//							[Stage 3]
	//				 Create Mirrors to StencilBuffer. 
	//							   |
	//							[Stage 4]
	//			 Create some Effects, like MirrorBlending
	//							   |
	//							[Stage 5]
	//		  Use Mirror Planes to calculate Model Reflections	
	//							   |
	//							[Stage 6]
	//	 Render Reflected Models with "WriteToExistingStencilOnly" stencil Effect, 
	//			that clip graphic outside of Mirror Boundary
	//							   |
	//							[Stage 7]
	//			Clear Stencil and Depth Buffer for next frame
	//
	class MirrorController
	{
	public:
		MirrorController();	
		~MirrorController();
		
	private:
		MirrorController(const MirrorController& other);
		MirrorController& operator=(const MirrorController& other);

	public:

		//Name		  : Init
		//Description : Initialization of MirrorController. It only binds parameters to Class Fields.
		//Input		  : device			   - pointer to Direct X device		
		//				depthStencilView   - Direct X buffer where are Stencil Effects are written using StencilController
		//				mainTextInterface  - pointer to console ( chat window ) controller 
		//				world			   - container of all models in a game. Nessesary to calculate reflected position ( check "World.h" for reference )
		//				stencilController  - container of all stencil effects. There is "CreateNewStencil" and "WriteToExistingStencilOnly" effect used in this class ( check "StencilController.h" for reference )	
		//				rasterController   - container of all rasterizer effects ( like BackFaceCulling ). For mirrored models writing vertices have Reversed Order, so RasterizerController is used to change it
		//				blendController	   - container ot all blend effects - used for some Mirror Texturing effect ( check "BlendController.h" - MirrorBlending for reference )
		//Output	  : true when no errors, else false
		bool Init(ID3D10Device *device, ID3D10DepthStencilView *depthStencilView, MainTextInterface *mainTextInterface, World *world, StencilController *stencilController, RasterController *rasterController, BlendController *blendController);
		
		//Name		  : Frame
		//Description : Render Mirrors in Scene for each frame ( check class description for steps )
		//Input		  : world			   - container of all models in a game. Nessesary to calculate reflected position ( check "World.h" for reference )
		//				drawLights		   - like world, but container for lights ( check "DrawLights.h"for reference )
		//				V				   - view transformation matrix        \
		//				P				   - perspective transformation matrix  |=> those parameters are used to calculate reflection
		//				eyePos			   - position of camera                /
		void Frame(World *world, DrawLights *drawLights, D3DXMATRIX V, D3DXMATRIX P, D3DXVECTOR3 eyePos);
	
		//Name		  : ShutDown
		//Description : ShutDown routine
		void ShutDown();

	private:

		//Name		  : GetMirrorsFromWorld
		//Description : Helper method that filters only Mirrors from all models in "World" class. Used in Frame method for [Stage 3] ( check class description )
		//				TODO: Not best solution. Unnesesary logic
		//Output	  : List of Mirrors polymorphed to base "Model"class 
		list<Model*> GetMirrorsFromWorld();

		//Name		  : Reflect
		//Description : Helper method that makes reflection Models over Mirror Planes in "Frame"method for [Stage 5] ( check class description )
		//Input		  : model  - model that would be reflected
		//				mirror - model would be reflected over Planes included in Mirror object
		//				V	   - view transformation matrix - used for reflection calculation
		//				P	   - perspective transformation matrix - used for reflection calculation
		void Reflect(Model* model, Model* mirror, D3DXMATRIX V, D3DXMATRIX P);

	private:

		//Pointer to Direct X device
		ID3D10Device *device;

		//Pointer to Direct X buffer where are Stencil Effects are written using StencilController
		ID3D10DepthStencilView *depthStencilView;

		//Pointer to console ( chat window ) controller 
		MainTextInterface *mainTextInterface;

		//Container of all models in a game. Nessesary to calculate reflected position ( check "World.h" for reference )
		World *world;

		//Container of all stencil effects. There is "CreateNewStencil" and "WriteToExistingStencilOnly" effect used in this class ( check "StencilController.h" for reference )	
		StencilController *stencilController;

		//Container of all rasterizer effects ( like BackFaceCulling ). For mirrored models writing vertices have Reversed Order, so RasterizerController is used to change it
		RasterController *rasterController;

		//Container ot all blend effects - used for some Mirror Texturing effect ( check "BlendController.h" - MirrorBlending for reference )
		BlendController *blendController;
	};
};

#endif