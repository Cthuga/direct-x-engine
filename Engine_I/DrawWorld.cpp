#include "DrawWorld.h"

using namespace Engine_I;

Engine_I::DrawWorld::DrawWorld()
{
	device = 0;
	world = 0;
	mainTextInterface = 0;

	textureController = TextureController();
}

Engine_I::DrawWorld::~DrawWorld()
{
	
}

//Two methods that are especially interesting : DrawWorld::Init and DrawWorld::Frame. Two WinMains for GameDev. First one for initialization, second one for dynamic of game. 
//From here we don't think about angine anymore
bool Engine_I::DrawWorld::Init(World *world, MainTextInterface* mainTextInterface, ID3D10Device *device, BlendController *blendController, DrawLights *drawLights)
{
	//This part should be hidden ( maybe DrawWorld base class ? )
	this->world = world;
	this->device = device;
	this->mainTextInterface = mainTextInterface;
	this->blendController = blendController;
	this->drawLights = drawLights;

	mainTextInterface->AddLog("Initializing textures ...", D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	//And this part should be forwarded to Graphic::Init method ( yeah, still testing many things ) : TODO in the future
	if(!textureController.Init(device, mainTextInterface))
	{
		Trace::Error("-Nie uda�o si� zainicjowa� tekstur");
		return false;	
	}
	//End of hidden part : TODO in the future

	//And the fun part : creating models on the scene.
	//Models must have their description ( check ModelDesc.h )
	//To add new model to the world, use :
	//model->AddWorld

	ModelDesc desc;
	desc.Height = 3;
	desc.Long = 30;
	desc.Width = 3;
	desc.SpecPower = 2;
	desc.TexXScale = 1.0f;
	desc.TexYScale = 1.0f;

	world->AddWorld(&Cube(), desc, "Texture Light Effect", "left_wall");
	world->AddWorld(&Cube(), desc, "Texture Light Effect", "right_wall");
	world->AddWorld(&Cube(), desc, "Texture Light Effect", "front_wall");
	world->AddWorld(&Cube(), desc, "Texture Light Effect", "back_wall");

	desc.Height = 8;
	desc.Long = 60;
	desc.Width = 0.1f;
	desc.SpecPower = 2;
	desc.TexXScale = 0.1f;
	desc.TexYScale = 0.7f;

	world->AddWorld(&Cube(), desc, "Texture Light Effect", "left_net");
	world->AddWorld(&Cube(), desc, "Texture Light Effect", "right_net");
	world->AddWorld(&Cube(), desc, "Texture Light Effect", "front_net");
	world->AddWorld(&Cube(), desc, "Texture Light Effect", "back_net");

	desc.Long = 40.0f;
	desc.Height = 1.0f;
	desc.Width = 40.0f;
	desc.SpecPower = D3DXCOLOR(0.7f, 0.7f, 0.7f, 3.0f);
	desc.TexXScale = 1;
	desc.TexYScale = 1;

	world->AddWorld(&Floor(), desc, "Texture Light Effect", "fl");

	desc.Long = 100.0f;
	desc.Height = 1.0f;
	desc.Width = 100.0f;
	desc.SpecPower = D3DXCOLOR(0.7f, 0.7f, 0.7f, 3.0f);
	desc.TexXScale = 0.1;
	desc.TexYScale = 0.1;
	
	for(int i = 0; i < 10 ; i++)
	{
		for(int j = 0; j < 10 ; j++)
		{
			ostringstream grassName;
			grassName << "gr_" << i << "_" << j;
			world->AddWorld(&Floor(), desc, "Texture Light Effect",  grassName.str());
		}
	}

	desc.Long = 30.0f;
	desc.Height = 1.0f;
	desc.Width = 30.0f;
	desc.SpecPower = D3DXCOLOR(0.3f, 0.3f, 0.7f, 4.0f);
	desc.TexXScale = 1.0f;
	desc.TexYScale = 1.0f;
	desc.BlendState = blendController->WaterBlending;

	world->AddWorld(&Floor(), desc, "Texture Light Effect",   "water");

	desc.Long = 9.0f;
	desc.Height = 1.0f;
	desc.Width = 15.0f;
	desc.SpecPower = D3DXCOLOR(0.0f, 0.0f, 0.7f, 3.0f);
	desc.TexXScale = 1.0f;
	desc.TexYScale = 1.0f;
	desc.BlendState = 0;
	desc.Mirror = true;
	
	world->AddWorld(&Floor(), desc, "Texture Light Effect", "mirror1");
	world->AddWorld(&Floor(), desc, "Texture Light Effect", "mirror2");

	desc.Long = 4000.0f;
	desc.Height = 1.0f;
	desc.Width = 4000.0f;
	desc.SpecPower = D3DXCOLOR(0.0f, 0.0f, 0.7f, 3.0f);
	desc.TexXScale = 0.1f;
	desc.TexYScale = 0.1f;
	desc.BlendState = 0;
	desc.Mirror = false;

	world->AddWorld(&Floor(), desc, "Texture Light Effect", "sky");

	return true;
}

//Two methods that are especially interesting : DrawWorld::Init and DrawWorld::Frame. Two WinMains for GameDev. First one for initialization, second one for dynamic of game. 
//From here we don't think about angine anymore
void Engine_I::DrawWorld::Frame(D3DXMATRIX V, D3DXMATRIX P, D3DXVECTOR3 eyePos, GameTime time )
{
	//Another "THIS IS WRONG" part. Here I have dynamic logic for lighting. I've created separated "WinMain" for lighting which is "DrawLights"
	//For testing I am closing my eyes for some things, this is one of them. Anyway, this is only example code in engine, feel free to modify it in Your
	//own way

	CbPerObjectDesc desc;
	desc.WVP = V*P;
	static int counter = 0;
	static int ms = 0;

	const int max_counter = textureController.GetTextureContainer().ChildContainers[".\\Textures\\water\\"].Textures.size();

	CbPerFrameDesc lightEffectDescription;
	lightEffectDescription.EyePos = eyePos;

	D3DXVECTOR3 actualLightPosition = drawLights->GetPosition(1);

	actualLightPosition.x = 10 * sin(0.003f * (float)time.Ms);
	actualLightPosition.z = 10 * cos(0.003f * (float)time.Ms);
	actualLightPosition.y = 1;

	drawLights->ChangePosition(1, actualLightPosition);

	D3DXVECTOR3 actualLightPosition2 = drawLights->GetPosition(0);

	actualLightPosition2.x = 3 * cos(0.005f * (float)time.Ms);
	actualLightPosition2.z = 3 * sin(0.005f * (float)time.Ms);
	actualLightPosition2.y = 2;

	drawLights->ChangePosition(0, actualLightPosition2);

	actualLightPosition2 = drawLights->GetPosition(2);

	actualLightPosition2.x = 15 * cos(0.005f * (float)time.Ms);
	actualLightPosition2.z = 15 * sin(0.005f * (float)time.Ms);
	actualLightPosition2.y = 1;

	drawLights->ChangePosition(2, actualLightPosition2);

	for(int i = 0; i < drawLights->lightDesc.LightNum ; i++)
	{
		lightEffectDescription.Lights[i] = LightDesc(drawLights->lightDesc.Lights[i]);
	}
		
	lightEffectDescription.LightNum = drawLights->lightDesc.LightNum;

	world->UpdatePerFrame(&lightEffectDescription);
	
	//End of part with lighting----------------------------------------------------------
	
	//So from here is what it should really be in this body...
	//positions of models in time - so ... the fun part

	//Some usefull stuff:
	//UpdatePerTexture - changes texture for next models
	//ModelDesc - description of model
	//UpdateAndRender - rendering object with new position on screen

	world->UpdatePerTexture(textureController.GetTexture("Sample\\woods.jpg"));

	D3DXMATRIX locationMatrix;
	D3DXMatrixTranslation(&locationMatrix, - 13.5f, -1.5f, 0.0f);
	desc.WVP = locationMatrix * V * P;
	desc.W = locationMatrix;

	world->UpdateAndRenderModel(&desc, "left_wall");
		
	D3DXMatrixTranslation(&locationMatrix, 13.5f, -1.5f, 0.0f);
	desc.WVP = locationMatrix * V * P;
	desc.W = locationMatrix;

	world->UpdateAndRenderModel(&desc, "right_wall");

	D3DXMATRIX rotationMatrix;
	D3DXMatrixRotationY(&rotationMatrix, D3DX_PI/2);

	D3DXQUATERNION rotation;
	D3DXQuaternionRotationMatrix(&rotation, &rotationMatrix);
	D3DXVECTOR3 translation =  D3DXVECTOR3(0.0f, -1.5f, -16.5f);
	D3DXMatrixTransformation(&locationMatrix, 0, 0, 0, 0, &rotation, &translation);
	desc.WVP = locationMatrix  * V * P;
	desc.W = locationMatrix;

	world->UpdateAndRenderModel(&desc, "front_wall");

	translation =  D3DXVECTOR3(0.0f, -1.5f, 16.5f);
	D3DXMatrixTransformation(&locationMatrix, 0, 0, 0, 0, &rotation, &translation);
	desc.WVP = locationMatrix  * V * P;
	desc.W = locationMatrix;

	world->UpdateAndRenderModel(&desc, "back_wall");

	world->UpdatePerTexture(textureController.GetTexture("Sample\\net.dds"));

	D3DXMatrixTranslation(&locationMatrix, -30.0f, 1.0f, 0.0f);
	desc.WVP = locationMatrix * V * P;
	desc.W = locationMatrix;
	desc.Clip = true;
	desc.texAdressingType = 2;

	world->UpdateAndRenderModel(&desc, "left_net");

	translation =  D3DXVECTOR3(0.0f, 1.0f, -30.0f);
	D3DXMatrixTransformation(&locationMatrix, 0, 0, 0, 0, &rotation, &translation);
	desc.WVP = locationMatrix * V * P;
	desc.W = locationMatrix;

	world->UpdateAndRenderModel(&desc, "back_net");

	translation =  D3DXVECTOR3(0.0f, 1.0f, 30.0f);
	D3DXMatrixTransformation(&locationMatrix, 0, 0, 0, 0, &rotation, &translation);
	desc.WVP = locationMatrix * V * P;
	desc.W = locationMatrix;

	world->UpdateAndRenderModel(&desc, "front_net");

	D3DXMatrixRotationY(&rotationMatrix, -D3DX_PI);
	D3DXQuaternionRotationMatrix(&rotation, &rotationMatrix);
	D3DXMatrixTranslation(&locationMatrix, 30.0f, 1.0f, 0.0f);
	desc.WVP = locationMatrix * V * P;
	desc.W = locationMatrix;

	world->UpdateAndRenderModel(&desc, "right_net");

	D3DXMatrixTranslation(&locationMatrix, 0.0f, -3.0f, 0.0f);
	desc.WVP = locationMatrix * V * P;
	desc.W = locationMatrix;
	desc.texAdressingType = 1;
	desc.Clip = false;

	world->UpdatePerTexture(textureController.GetTexture("Sample\\floor.jpg"));

	world->UpdateAndRenderModel(&desc, "fl");

	world->UpdatePerTexture(textureController.GetTexture("Sample\\grass.jpg"));

	for(int i = 0; i < 10 ; i++)
	{
		for(int j = 0; j < 10 ; j++)
		{
			ostringstream grassName;
			grassName << "gr_" << i << "_" << j;

			D3DXMatrixTranslation(&locationMatrix, -500.0f + i * 100 , -3.1f, -500.0f + j * 100);
			desc.WVP = locationMatrix * V * P;
			desc.W = locationMatrix;
			desc.texAdressingType = 2;
				
			world->UpdateAndRenderModel(&desc, grassName.str());
		}
	}
		
	D3DXMatrixTranslation(&locationMatrix, 0.0f, -1.0f, 0.0f);
	desc.WVP = locationMatrix * V * P;
	desc.W = locationMatrix;
	desc.texAdressingType = 1;
	string waterAnimationTxt = textureController.GetTextureContainer().ChildContainers[".\\Textures\\water\\"].Textures[counter];
	waterAnimationTxt = waterAnimationTxt.substr(11, sizeof(waterAnimationTxt)-11);

	world->UpdatePerTexture(textureController.GetTexture(waterAnimationTxt));

	world->UpdateAndRenderModel(&desc, "water");

	if(counter >= max_counter - 1)
	{
		counter = 0 ;
	}
	else
	{
		counter ++;
	}

	world->UpdatePerTexture(textureController.GetTexture("Sample\\mirror.JPG"));

	D3DXMATRIX transW;
	D3DXMATRIX RotX;
	D3DXMatrixRotationY(&rotationMatrix, -D3DX_PI/5);
	D3DXMatrixRotationX(&RotX, -2 * D3DX_PI/3 + 0.2 * sin((double)time.Ms*0.005));

	D3DXMatrixTranslation(&locationMatrix,10.0f, 4.0f, 21.0f);
	desc.WVP = RotX *locationMatrix * V * P;
	desc.W = RotX *locationMatrix;

	world->UpdateAndRenderModel(&desc, "mirror1");
	
	D3DXMATRIX Rot2X;
	D3DXMatrixRotationX(&Rot2X,   -2 * D3DX_PI/3 + 0.2 * cos((double)time.Ms*0.005));

	D3DXMatrixTranslation(&locationMatrix,-10.0f, 4.0f, 21.0f);

	desc.WVP = Rot2X*locationMatrix * V * P;
	desc.W = Rot2X*locationMatrix;

	world->UpdateAndRenderModel(&desc, "mirror2");

	world->UpdatePerTexture(textureController.GetTexture("Sample\\sky.jpg"));

	D3DXMatrixTranslation(&locationMatrix,  0.0f, 100.0f, 0.0f);
	desc.WVP = locationMatrix * V * P;
	desc.W = locationMatrix;
	desc.Fog = false;
	desc.texAdressingType = 2;
	world->UpdateAndRenderModel(&desc, "sky");

	desc.WVP = V * P;
}

void Engine_I::DrawWorld::ShutDown()
{
	
}