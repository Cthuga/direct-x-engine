#include "graphic.h"

using namespace Engine_I;

Engine_I::Graphic::Graphic()
{
	device = 0;
	swapChain = 0;
	renderTargetView = 0;
	depthStencilView = 0;
	font = 0;
}

Engine_I::Graphic::~Graphic()
{

}

bool Engine_I::Graphic::InitCore()
{
	D3DXMatrixIdentity(&P);
	D3DXMatrixIdentity(&V);
	D3DXMatrixIdentity(&W);

	D3DXMatrixPerspectiveFovLH(&P, (float)D3DX_PI * 0.25f, (float)width/(float)height, 1.0f, 10000.0f);

	return true;
}

bool Engine_I::Graphic::Init(EngineIni iEngine, HWND ihwnd)
{
	engine = iEngine;
	Config config = engine.GetConfig();
	hwnd = ihwnd;
	width = config.configGraphic.ScreenWidth;
	height = config.configGraphic.ScreenHeight;
	FullScreen = config.configGraphic.FullScreen;

	//Initialization routine 
	//For more info about initialized instances check their descriptions in graphic class

	if(!d3XCore.Init(hwnd, width, height, device, swapChain, renderTargetView, depthStencilView, font, config.configGraphic.FullScreen))
	{
		Trace::Error("-Failed to load core of DirectX engine");
		return false;
	}

	if(!InitCore()) 
	{
		Trace::Error("-Failed to load graphic engine");
		return false;	
	}

	actor.Init(config);
	V = actor.GetView();

	if(!textInterface.Init(font, width, height))
	{
		Trace::Error("-Failed to init font interface");
		return false;	
	}

	if(!mainTextInterface.Init(font, width, height))
	{
		Trace::Error("-Failed to load main text interface");
		return false;	
	}

	if(!rasterController.Init(device, &mainTextInterface))
	{
		Trace::Error("Failed to load Raster Controller");
		return false;
	}
	
	if(!blendController.Init(device, &mainTextInterface, &time))
	{
		mainTextInterface.Warning("Failed to load Blend Controller");
		Trace::Log("Failed to load Blend Controller");
	}

	mainTextInterface.AddLog("Initializing environment configuration ...", D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	if(!drawConfiguration.Init(&effectController, device, &mainTextInterface))
	{
		mainTextInterface.Warning("Failed to initialize environment configuration");
		Trace::Log("Failed to initialize environment configuration");
		return false;
	}

	mainTextInterface.AddLog("Initializing lights ...", D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	if(!drawLights.Init())
	{
		Trace::Error("-Failed to load lights");
		return false;	
	}

	if(!stencilController.Init(device, &mainTextInterface, &rasterController))
	{
		mainTextInterface.Warning("Failed to initialize stencil controller");
		Trace::Log("Failed to initialize stencil controller");
		return false;
	}

	mainTextInterface.AddLog("World is loading ...", D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	if(!world.Init(&effectController, device, &mainTextInterface, &stencilController))
	{
		Trace::Error("-Failed to load empty World");
		return false;	
	}

	mainTextInterface.AddLog("Elements of the world are loading ...", D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	//One more info for this method. This is the place where all models are defined. We can say, that here is first part of GameLogic.
	//Dynamic part is defined in DrawWorld::Frame method ( later part of code )
	if(!drawWorld.Init(&world, &mainTextInterface, device, &blendController, &drawLights))
	{
		Trace::Error("-Failed to load world elements");
		return false;	
	}
	
	mainTextInterface.AddLog("Initializing mirrors ...", D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	if(!mirrorController.Init(device, depthStencilView, &mainTextInterface, &world, &stencilController, &rasterController, &blendController))
	{
		Trace::Error("-Failed to load mirrors");
		return false;	
	}

	return true;
}

void Engine_I::Graphic::Resize()
{
	HRESULT result;

	Config config = engine.GetConfig();
	BOOL fullScreenState;
	swapChain->GetFullscreenState(&fullScreenState, NULL);

	if(device)
	{
		depthStencilView->Release();
		depthStencilView = 0;

		renderTargetView->Release();
		renderTargetView = 0;

		result = swapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM,  DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH);
		if(FAILED(result))
		{
			Trace::Error("After changing size of window, failed to init SwapChain again");
			return;
		}

		d3XCore.CommonInit(hwnd, width, height, device, swapChain, renderTargetView, depthStencilView, font, config.configGraphic.FullScreen);
	}

	textInterface.Init(font, width, height);
	mainTextInterface.Init(font, width, height);
}

void Engine_I::Graphic::Frame(GameTime gameTime, Input iInput)
{
	time = gameTime;
	input = iInput;

	BeginScene();

	RenderWorld();

	RenderSprites();

	EndScene();
}

void Engine_I::Graphic::BeginScene()
{
	//Set Render Target, Blend State and Depth Stencil To default
	device->ClearRenderTargetView(renderTargetView, D3DXCOLOR(0.0f,0.0f,0.0f,0.0f));
	device->ClearDepthStencilView(depthStencilView, D3D10_CLEAR_DEPTH | D3D10_CLEAR_STENCIL, 1.0f, 0);
	device->OMSetDepthStencilState(0,0);
	float bf[4] = {0,0,0,0};
	device->OMSetBlendState(0, bf, 0XFFFFFFFF); 
	
	//Update Actor
	actor.Action(time, input);
	V = actor.GetView();
	eye = actor.eye;

	//Update text interfaces
	textInterface.BeginScene(time, actor.GetCoordinates(), world.GetGlobalVertexCounter(), world.GetGlobalIndexCounter(), world.GetGlobalFacesCounter(), world.GetGlobalModelCounter());	
}

void Engine_I::Graphic::RenderWorld()
{
	//Set default topology
	device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//Draw elements of world

	//Some additional info : in the future there would be separation from CoreEngine, GraphicEngine, AIEngine and PhysicEngine (I Hope)
	//Anyway this Method would be WinMain for Game Logic, separated from all that weard *Engine stuff
	//So if You want to make some experiments, here is the place You go
	//(Second one is drawWorld.Init where models are defined)
	drawWorld.Frame(V, P, eye, time);

	mirrorController.Frame(&world, &drawLights, V, P, eye);

	//Draw text interfaces
	textInterface.RenderWorld();

	//Draw main Log window
	mainTextInterface.Render(time);
}

void Engine_I::Graphic::RenderSprites()
{
	textInterface.RenderSprites();
}

void Engine_I::Graphic::EndScene()
{
	textInterface.EndScene();

	//Switch to next frame
	swapChain->Present(0,0);
}

void Engine_I::Graphic::ShutDown()
{
	rasterController.ShutDown();
	stencilController.ShutDown();
	effectController.ShutDown();
	drawLights.Shutdown();
	drawWorld.ShutDown();
	world.ShutDown();

	if(font)
	{
		font->Release();
		font = 0;
	}

	if(renderTargetView)
	{
		renderTargetView->Release();
		renderTargetView = 0;
	}

	if(depthStencilView)
	{
		depthStencilView->Release();
		depthStencilView = 0;
	}

	if(swapChain)
	{
		swapChain->Release();
		swapChain = 0;
	}

	if(device)
	{
		device->Release();
		device = 0;
	}
}