#ifndef __LIGHTVERTEX_H_
#define __LIGHTVERTEX_H_

#include "define_core.h"

#include D3D
#include D3DX

#include "AbstractVertex.h"

namespace Engine_I
{
	struct LightVertex : AbstractVertex
	{
	public:
		LightVertex(D3DXVECTOR3 position, D3DXVECTOR3 normal, D3DXCOLOR diffuse, D3DXCOLOR specPow);

		D3DXVECTOR3 position;
		D3DXVECTOR3 normal;
		D3DXCOLOR diffuse;
		D3DXCOLOR specPow;
	};
};

#endif