#include "utils.h"

using namespace Engine_I;

void Engine_I::PadLeft(string *s, char ch, int length)
{
	if(length <= (int)s->length()) return;

	int insertion = length - s->length();
	s->insert(s->begin(), insertion, ch);
}

void Engine_I::PadRight(string *s, char ch, int length)
{
	if(length <= (int)s->length()) return;

	int insertion = length - s->length();
	s->insert(s->end(), insertion, ch);
}

string& Engine_I::trim_right_inplace(std::string& s)
{
	 string delimiters = " \f\n\r\t\v"; 
	 return s.erase( s.find_last_not_of( delimiters ) + 1 );
}

string& Engine_I::trim_left_inplace(std::string& s)
{
	  string delimiters = " \f\n\r\t\v";
	  return s.erase( 0, s.find_first_not_of( delimiters ) );
}

string& Engine_I::trim(string& s)
{
	  string delimiters = " \f\n\r\t\v";
	  return trim_left_inplace( trim_right_inplace( s ) );
}

string Engine_I::ToLower(string s)
{
	transform(s.begin(), s.end(), s.begin(), ::tolower);
	return s;
}

string Engine_I::GetFileNameFromPath(string fileName)
{
	int indexOf = fileName.find_last_of('\\');

	if(indexOf == -1 || indexOf == 0) return "";

	string result = fileName.substr(indexOf + 1, sizeof(fileName) - indexOf - 1);

	return result;
}