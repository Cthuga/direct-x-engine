#ifndef __TEXTUREEFFECTDESC_H_
#define __TEXTUREEFFECTDESC_H_

#include "define_core.h"

#include D3D
#include D3DX

#include <windows.h>

namespace Engine_I
{
	struct TextureEffectDesc
	{
	public:
		TextureEffectDesc();

		//1 Vector
		D3DXVECTOR3 Position;
		float __pad1;

		//2 Vector
		D3DXVECTOR3 Dirrection;
		float __pad0;

		//3 Vector
		D3DXCOLOR Ambient;

		//4 Vector
		D3DXCOLOR Specular;

		//5 Vector
		D3DXCOLOR Diffuse;

		//6 Vector
		D3DXVECTOR3 Attenuation;
		float Range;

		//7 Vector
		int Type;
		float SpotPower;
		float __pad2;
		float __pad3;
	};
};

#endif