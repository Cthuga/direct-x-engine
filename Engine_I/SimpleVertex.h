#ifndef __SIMPLEVERTEX_H_
#define __SIMPLEVERTEX_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining custom libraries
#include "AbstractVertex.h"

namespace Engine_I
{
	//StructName  :  SimpleVertex
	//Description :  Check AbstractVertex for reference. This description was used in early stage of engine development. Now it is used only for some
    //				 model that doesn't need light, like grid definition
	//				 Vertex structures are passed by DirectX Input Layout to HLSL script files
	struct SimpleVertex : AbstractVertex
	{
	public:
		SimpleVertex(D3DXVECTOR3 position, D3DXCOLOR color);

		//Position of vertex
		D3DXVECTOR3 position;

		//Vertex color ( it can be used to get some diffusion color effects )
		D3DXCOLOR color;
	};
};

#endif