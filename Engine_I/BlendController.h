#ifndef __BLENDCONTROLLER_H_
#define __BLENDCONTROLLER_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining custon libraries
#include "MainTextInterface.h"
#include "GameTime.h"
#include "trace.h"

namespace Engine_I
{
	//ClassName   : BlendController
	//LifeTime    : Initialized during graphic initialization, executed on each frame
	//Description : Class that stores all blend effects prepared for game engine
	class BlendController
	{
	public:
		BlendController();
		BlendController(const BlendController &blendController);

		~BlendController();

		BlendController& operator=(const BlendController &blendController);

		//Name		  : Init
		//Description : Initialization of all prepared effects. It uses helper methods "Generate[...]Blending" for each effects 
		//Input		  : device			   - pointer to Direct X device					
		//				mainTextInterface  - pointer to console ( chat window ) controller 
		//				gameTime		   - pointer to actual game time ( check "GameTime.h" and "timer.h" for reference )
		//Output	  : true when no errors, else false
		bool Init(ID3D10Device *device, MainTextInterface *mainTextInterface, GameTime *time);

		//Name		  : Shutdown
		//Description : Shutdown routine
		void Shutdown();

		//Concrete Blend Effects - they are filled during initialization phase
		ID3D10BlendState* WaterBlending;	//Water transparency
		ID3D10BlendState* MirrorBlending;	//Mixing mirror texture color with reflected objects

	private:

		//Name		  : GenerateWaterBlending
		//Description : Executed during Init. Generates value for WaterBlending Property.
		//Output	  : true when no errors, else false
		bool GenerateWaterBlending();

		//Name		  : GenerateMirrorBlending
		//Description : Executed during Init. Generates value for MirrorBlending Property.
		//Output	  : true when no errors, else false
		bool GenerateMirrorBlending();

		//Pointer to Direct X Device
		ID3D10Device *device;

		//Pointer to console ( chat window ) controller 
		MainTextInterface *mainTextInterface;

		//Pointer to actual game time ( check "GameTime.h" and "timer.h" for reference )
		GameTime *time;
	};
};

#endif