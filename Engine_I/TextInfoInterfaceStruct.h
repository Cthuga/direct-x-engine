#ifndef __TEXTINFOINTERFACESTRUCT_H_
#define __TEXTINFOINTERFACESTRUCT_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3DX

//Defining system libraries
#include <string>

using namespace std;

namespace Engine_I
{
	//StructName  : TextInfoInterfaceStruct
	//Description : Describe text rendered in one of the screen corners ( check TextInfoInterface.h for reference )
	struct TextInfoInterfaceStruct
	{
	public:

		//Priority of text. Text with bigger priority is rendered first ( ascending )
		int Priority;

		//For standarization reason, text in the corner has structure : Parameter = Value
		wstring Parameter;  //Text parameter
		wstring Value;		//Text value
		D3DXCOLOR Color;    //Text color

		//Some operator overriding that takes into consideration "Priority" value in comparsion and sorting
		bool operator()(const TextInfoInterfaceStruct& first, const TextInfoInterfaceStruct& second) const;
		bool operator< (const TextInfoInterfaceStruct& first) const;
		bool operator> (const TextInfoInterfaceStruct& first) const;
		bool operator==(const TextInfoInterfaceStruct& first) const;
	};
};

#endif