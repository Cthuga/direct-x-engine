#ifndef __DRAWTEXTINTERFACE_H_
#define __DRAWTEXTINTERFACE_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <string>
#include <sstream>
#include <math.h>

//Defining custon libraries
#include "TextInfoInterface.h"
#include "TextInfoInterfaceStruct.h"
#include "GameTime.h"

using namespace std;

namespace Engine_I
{
	//ClassName   : DrawTextInterface
	//LifeTime    : Initialized during graphic initialization, executed on each frame
	//Description : Storage text structures rendered on corners of the screen
	class DrawTextInterface
	{
	public:
		DrawTextInterface();
		~DrawTextInterface();

		//Name		  : Init
		//Description : Initialize all texts for each corner of the screen
		//Input		  : fontInterface - pointer to DirectX font controller
		//				width		  - width of the screen
		//			    height		  - height of the screen
		//Output	  : true when no errors, else false
		bool Init(ID3DX10Font* fontInterface, int width, int height);

		//Name		  : BeginScene
		//Description : Initialize all texts for each corner of the screen
		//Input		  : gameTime	  - actual game time

		//TODO: [DrawTextInterface: BeginScene] Don't put specialized parameters to method input, find other solution

		//Concrete parameters used for render some technical informations:
		//				V			  - view transformationMatrix ( used for x,y,z coords )
		//			    verCnt		  - number of vertices used by engine
		//				indCnt		  - number of indices used by endine
		//				facCnt		  - number of faces used by engine
		//			    mdlCnt		  - number of models used by engine
		void BeginScene(GameTime gameTime, D3DXVECTOR3 V, int verCnt, int indCnt, int facCnt, int mdlCnt);

		//Name		  : RenderWorld
		//Description : Executed during RenderWorld stage in frame lifetime - render text for each corners
		void RenderWorld();

		//Name		  : RenderSprites
		//Description : Executed during RenderSprites stage in frame lifetime - render text for sprites
		void RenderSprites();

		//Name		  : EndScene
		//Description : Executed during EndScene stage in frame lifetime - end scene routine
		void EndScene();

		//Name		  : ShutDown
		//Description : Clearing routine
		void ShutDown();

	private:

		//Name		  : StructHelper
		//Description : Binds value to parameter for single TextInfoInterface Structure. 
		//Input		  : parameter	  - parameter name for text rendered in corner of the screen
		//				value		  - value for parameter
		//				out			  - reference to changed textInfoInterfaceStruct which filled parameter and value
		void StructHelper(string parameter, string value, TextInfoInterfaceStruct* out);

		//Engine for rendering text in corner of the screens ( check TextInfoInterface.h for reference )
		TextInfoInterface textInterface;

		//TODO : Should be deleted
		//Actual Game Time
		GameTime time;

		//TODO : Should be deleted
		//Actual FPS value
		string FPS;
	};
};

#endif