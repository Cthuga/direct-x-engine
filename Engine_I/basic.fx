//				As reminder description of Direct X rendering stages :
//
//					Input Assembler Stage <------------------+
//							||								 |
//							\/								 |
//					 Vertex Shader Stage <-------------------+
//							||								 |
//							\/								 |
//				    Geometry Shader Stage <------------------+
//							||								 |
//							\/								 |
//							 +----> Stream Output Stage ---->+-----> GPU Resources, Buffers, Textures
//							||								 |	 
//							\/								 |	(the communication between GPU and CPU has been done through World class - check World.h
//					 Rasterizer Stage <----------------------+		       It is : World <---> EffectController <---> HLSL Script Files )	
//							||							     |				     Check World.H and EffectController.h for reference
//							\/								 |
//					Pixel Shader Stage <---------------------+
//							||							     |
//							\/							     |
//			       Output Merger Stage <-------------------->+

//Communication between GPU ( effect script file ) and CPU ( C++ code ) has been done with help of cbuffer. 
//Cbuffer stores values that can be get/set from C++ code. Because of engine architecture there is few "cbuffers". Every cbuffer is updated in different stage of frame:

//cbPerObject is updated once per rendered model
cbuffer cbPerObject
{
	float4x4 W;			//world transformation matrix - necessary for transformation calculations
	float4x4 WVP;		//world transformation matrix * view transformation matrix * presentation transformation matrix - representation of final position of object according to camera view
};

//Input of structure SimpleVertex.h is passed to INPUT of Vertex Shader Stage. Reminder of render stages is described in RasterController.h. Also check SimpleVertex.h for C++ description of this InputStructure.
//Connection between C++ and GPU version of this structure is made in Model class ( check Model.h for reference ).
//Whole process take place in World class ( check World.h for reference )

//iPos [input], oPos [output] - Position of vertex
//iCol [input], oCol [output] - Material color value

//This is body of Vertex Shader Stage - logic executed once per vertex ( input was described earlier, check also SimpleVertex.h for C++ input description )
void VS(float3 iPos : POSITION, float4 iCol : COLOR, out float4 oPos : SV_POSITION, out float4 oCol : COLOR)
{
	//Calculation of position every vertex and normal in 2d result screen
	oPos = mul(float4(iPos, 1) , WVP);

	//Passing color to Pixel Shader Stage
	oCol = iCol;
}

//This is body of Pixel Shader Stage - logic executed once per pixel seen on the screen
float4 PS(float4 iPos : SV_POSITION, float4 iCol : COLOR) : SV_TARGET
{
	//Simply return color for each pixel from input
	return iCol;
}

//Technique representing effect for C++ code. We can say that's Main function for HLSL script files. Every model has attached Effect class ( see AbstractEffect.h for reference )
//and every Effect class has attached technique executed once per frame.
technique10 BaseTechnique
{
	//Every technique for real have array of "Main" functions. They might be combined in different ways during rendering models in C++ code. Every index in this array is "Pass" with number attached to it.
	//Actually there is only one Pass and one Technique per effect
	pass P0
	{
		//In Pass body there is Stage Rendering Routine ( check beginning of this file for reminder )

		//First execute method VS for Vertex Shader Stage
		SetVertexShader( CompileShader( vs_4_0, VS()) );

		//Then there is Geometry Shader Stage, actually empty
		SetGeometryShader( NULL );

		//Finally execute method PD for Pixel Shader Stage
		SetPixelShader ( CompileShader( ps_4_0, PS()) );
	}
}