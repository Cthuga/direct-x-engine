#include "BlendController.h"	

using namespace Engine_I;

Engine_I::BlendController::BlendController()
{
	device = 0;
	mainTextInterface = 0;
	time = 0;
}

Engine_I::BlendController::BlendController(const BlendController &blendController)
{

}

Engine_I::BlendController::~BlendController()
{

}

BlendController& Engine_I::BlendController::operator=(const BlendController &blendController) 
{
	return BlendController(blendController);
}

bool Engine_I::BlendController::Init(ID3D10Device *device, MainTextInterface *mainTextInterface, GameTime *time)
{
	this->device = device;
	this->mainTextInterface = mainTextInterface;
	this->time = time;

	if(!GenerateWaterBlending()) return false;
	if(!GenerateMirrorBlending()) return false;

	return true;
}

void Engine_I::BlendController::Shutdown()
{

}

bool Engine_I::BlendController::GenerateWaterBlending()
{
	HRESULT result;

	D3D10_BLEND_DESC desc = {0};
	desc.AlphaToCoverageEnable = false;
	desc.BlendEnable[0] = true;
	desc.BlendOp = D3D10_BLEND_OP_ADD;
	desc.BlendOpAlpha = D3D10_BLEND_OP_ADD;
	desc.SrcBlend = D3D10_BLEND_SRC_COLOR;
	desc.SrcBlendAlpha = D3D10_BLEND_SRC_ALPHA;
	desc.DestBlendAlpha = D3D10_BLEND_DEST_ALPHA;
	desc.DestBlend = D3D10_BLEND_DEST_COLOR;
	desc.RenderTargetWriteMask[0] = D3D10_COLOR_WRITE_ENABLE_ALL;

	result = device->CreateBlendState(&desc, &WaterBlending);
	if(FAILED(result))
	{
		mainTextInterface->Warning("Generating water blending failed ...");
		Trace::Error("Generating water blending failed ...");
		return false;
	}

	return true;
}

bool Engine_I::BlendController::GenerateMirrorBlending()
{
	HRESULT result;

	D3D10_BLEND_DESC desc = {0};
	desc.AlphaToCoverageEnable = false;
	desc.BlendEnable[0] = true;
	desc.BlendOp = D3D10_BLEND_OP_ADD;
	desc.BlendOpAlpha = D3D10_BLEND_OP_ADD;
	desc.SrcBlend = D3D10_BLEND_BLEND_FACTOR;
	desc.SrcBlendAlpha = D3D10_BLEND_ONE;
	desc.DestBlendAlpha = D3D10_BLEND_ZERO;
	desc.DestBlend = D3D10_BLEND_INV_BLEND_FACTOR;
	desc.RenderTargetWriteMask[0] = D3D10_COLOR_WRITE_ENABLE_ALL;

	result = device->CreateBlendState(&desc, &MirrorBlending);
	if(FAILED(result))
	{
		mainTextInterface->Warning("Generating mirror blending failed ...");
		Trace::Error("Generating mirror blending failed ...");
		return false;
	}

	return true;
}
