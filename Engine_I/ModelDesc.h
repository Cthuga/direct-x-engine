#ifndef __MODELDESC_H_
#define __MODELDESC_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <Windows.h>

namespace Engine_I
{
	//StructName  : ModelDesc
	//Description : Description for every model in game. Some information are passed to CbPerObject ( check AbstractEffectDesc.h for reference )
	struct ModelDesc
	{
	public:
		ModelDesc();
		~ModelDesc();

		//Specular power for model material ( passed to CbPerObject description - check AbstractEffectDesc.h for reference )
		D3DXCOLOR SpecPower;

		//Texture Coordinates in texel format ( passed to CbPerObject description - check AbstractEffectDesc.h for reference )
		D3DXVECTOR2 TexCoord;

		//Texels are scaled by these 2 values:
		float TexXScale;	//Scale in X dimension
		float TexYScale;	//Scale in Y dimension

		//Model size variables
		float Width;		//Object width  [ rescaling from <-1, 1> normalized range ]
		float Height;		//Object height [ rescaling from <-1, 1> normalized range ]
		float Long;			//Object long   [ rescaling from <-1, 1> normalized range ]

		//Mirror flags
		bool Mirror;		  //Flag determined does object is mirror ( default = false )
		bool Mirrored;		  //Flag determined does object should be reflected in mirror ( default = true )
		bool ReflectedState;  //Fixed value (it shouldn't be changed) Set when mirror algoritm is executed ( check MirrorController.h for reference )

		//Blend effect variables
		float BlendFactor[4];			//Some blend effects use blend factor to specify final color. It is used with blend effect bound to "BlendState"
		ID3D10BlendState *BlendState;	//Blend effect bound to model ( check BlendController.h for reference )
	};
};

#endif