#include "timer.h"

using namespace Engine_I;

Engine_I::Timer::Timer()
{
	pause = false;
}

Engine_I::Timer::~Timer()
{

}

void Engine_I::Timer::Reset()
{
	gameTime.D = 0;
	gameTime.H = 0;
	gameTime.M = 0;
	gameTime.Min = 0;
	gameTime.Ms = 0;
	gameTime.S = 0;
	gameTime.Y = 0;
	gameTime.FPS = 0;

	counter = 0;
	frequency = 0;
	tempCounter = 0;

	QueryPerformanceCounter((LARGE_INTEGER*)&prevCounter);
	QueryPerformanceCounter((LARGE_INTEGER*)&curCounter);
	delta = 0;

	gameTime.SFlag = false;
	gameTime.MSFlag = false;
}

void Engine_I::Timer::Tick()
{
	QueryPerformanceCounter((LARGE_INTEGER*)&curCounter);
	
	if(!pause)
	{
		gameTime.SFlag = false;
		gameTime.MSFlag = false;
		
		delta = curCounter - prevCounter;

		counter += delta;
		tempCounter +=delta;

		QueryPerformanceFrequency((LARGE_INTEGER*)&frequency);
		frequency= (int)floor((long double)(frequency/1000));

		__int64 addMs = 0;

		if(tempCounter >= frequency)
		{
			addMs = (__int64)floor((long double)(tempCounter/frequency));
			gameTime.MSFlag = true;
			tempCounter -= (addMs * frequency);
			gameTime.Ms += addMs;
		}

		gameTime.FPS = (int)floor((long double)frequency*1000/tempCounter);
	
		__int64 tempS = gameTime.S;
		gameTime.S = (__int64)floor((long double)gameTime.Ms / 1000);
		if((gameTime.S - tempS) > 0) gameTime.SFlag = true;

		gameTime.Min = (__int64)floor((long double)gameTime.S / 60);
		gameTime.H = (__int64)floor((long double)gameTime.Min / 60);
		gameTime.D = (__int64)floor((long double)gameTime.H / 24);
		gameTime.M = (__int64)floor((long double)gameTime.D / 30);
		gameTime.Y = (__int64)floor((long double)gameTime.M / 12);
	}
	
	prevCounter = curCounter;	
}

GameTime Engine_I::Timer::GetGameTime()
{
	return gameTime;
}

void Engine_I::Timer::Pause()
{
	pause = true;
}

void Engine_I::Timer::UnPause()
{
	pause = false;
}