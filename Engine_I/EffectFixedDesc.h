#ifndef __EFFECTFIXEDDESC_H_
#define __EFFECTFIXEDDESC_H_

#include "define_core.h"

#include D3D;
#include D3DX;

#include <Windows.h>

#include "FogDesc.h"
#include "AbstractEffectFixedDesc.h"

namespace Engine_I
{
	struct EffectFixedDesc : AbstractEffectFixedDesc
	{
	public:
		EffectFixedDesc();

		FogDesc Fog;
	};
};

#endif