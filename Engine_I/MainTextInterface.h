#ifndef __MAINTEXTINTERFACE_H_
#define __MAINTEXTINTERFACE_H_

//Defining preporcessor variables
#include "define_core.h"

//Defining DirectX libraries
#include D3D
#include D3DX

//Defining system libraries
#include <list>
#include <sstream>
#include <string>

//Defining custon libraries
#include "TextInfoInterface.h"
#include "TextInfoInterfaceStruct.h"
#include "GameTime.h"
#include "trace.h"

using namespace std;

namespace Engine_I
{
	//ClassName   : MainTextInterface
	//LifeTime    : Initialized during graphic initialization, executed on each frame
	//Description : Main console(chat) window controller. It is responsible for rendering text for console
	class MainTextInterface
	{
	public:
		MainTextInterface();
		~MainTextInterface();

		//Name		  : Init
		//Description : Initialize console
		//Input		  : font	 - pointer to Direct X font controller
		//				width    - width of the screen
		//				height	 - height of the screen
		//Output	  : true when no errors, else false
		bool Init(ID3DX10Font *font, int width, int height);

		//Name		  : AddLog
		//Description : Add log with specified color to console. Log would persist for 30 seconds
		//Input		  : message	 - message sent to console
		//				color    - color of message
		void AddLog(string message, D3DXCOLOR color);

		//Name		  : Render
		//Description : Rendering routine - executed once for each frame. 
		//Input		  : time	 - actual time ( it helps determine how much time of persistance left for each log )
		void Render(GameTime time);

		//Name		  : Warning
		//Description : It is simply AddLog with yellow color message. It helps for standarizing warning message and it is for future changes
		//Input		  : message	 - message sent to console
		void Warning(string message);

	private:

		//Collection for all rendered logs
		list<TextInfoInterfaceStruct> logs;

		//Inner timer transformation field
		int secondController;

		//Pointer to font used for rendering text
		ID3DX10Font* font;

		//Width of the screen field
		int width;

		//Height of the screen field
		int height;
	};
};

#endif