//This is definition of fog structure used for fog effect. Check FogDesc.h for C++ version of this structure
struct Fog
{
	float3 gFogColor;		//Color for fog effect
	float  gFogStart;		//Radius for fog-free square
	float  gFogRange;		//Max range of fog ( range where gFogRange = MAX(fog saturation) )
};