#ifndef __BASELIGHTEFFECTDESC_H_
#define __BASELIGHTEFFECTDESC_H_

#include "define_core.h"

#include D3D
#include D3DX

#include <Windows.h>

#include "AbstractEffectDesc.h"
#include "LightDesc.h"

namespace Engine_I
{
	struct BaseLightEffectDesc : AbstractEffectDesc
	{
	public:
		BaseLightEffectDesc();

		LightDesc Lights[256];
		D3DXVECTOR3 EyePos;
		int LightNum;
	};
};

#endif