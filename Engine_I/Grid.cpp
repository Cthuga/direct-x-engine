#include "Grid.h"

using namespace Engine_I;

Engine_I::Grid::Grid() : Model()
{
	Type = "Grid";
	faultedState = 0;
}

Engine_I::Grid::~Grid()
{

}

bool Engine_I::Grid::Init(AbstractEffect *effect, ID3D10Device *&device, MainTextInterface *&mainTextInterface, string name, ModelDesc *desc, StencilController *stencilController)
{
	faultedState = 0;
	Model::Init(effect, device, mainTextInterface, name, desc, stencilController);
	
	LightModelDesc *lightModel = (LightModelDesc*)desc;

	Topology = D3D10_PRIMITIVE_TOPOLOGY_LINELIST;
	Indexed = false;

	numIndices = 0;
	numVertices = 0;
	numFaces = 0;

	float pwidth = (float)lightModel->Width/2;
	float pheight = (float)lightModel->Height/2;
	float plong = (float)lightModel->Long/2;

	vector<SimpleVertex> verticeVector;
	vector<int> indiceVector;

	verticeVector.push_back(SimpleVertex(D3DXVECTOR3(0, 0, plong), lightModel->Diffuse));
	verticeVector.push_back(SimpleVertex(D3DXVECTOR3(0, 0, -plong), lightModel->Diffuse));
	verticeVector.push_back(SimpleVertex(D3DXVECTOR3(pwidth, 0, 0), lightModel->Diffuse));
	verticeVector.push_back(SimpleVertex(D3DXVECTOR3(-pwidth, 0, 0), lightModel->Diffuse));
	verticeVector.push_back(SimpleVertex(D3DXVECTOR3(0, pheight, 0), lightModel->Diffuse));
	verticeVector.push_back(SimpleVertex(D3DXVECTOR3(0, -pheight, 0), lightModel->Diffuse));

	numVertices += 6;

	for(int i = lightModel->TexXScale ; i < pwidth; i+=lightModel->TexXScale)
	{
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(i, 0, plong), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(i, 0, -plong), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(-i, 0, -plong), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(-i, 0, plong), lightModel->Diffuse));

		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(pwidth, 0, i), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(-pwidth, 0, i), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(pwidth, 0, -i), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(-pwidth, 0, -i), lightModel->Diffuse));

		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(i, -pheight, 0), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(i, pheight, 0), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(-i, -pheight, 0), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(-i, pheight, 0), lightModel->Diffuse));

		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(0, -pheight, i), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(0, pheight, i), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(0, -pheight, -i), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(0, pheight, -i), lightModel->Diffuse));

		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(0, i, plong), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(0, i, -plong), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(0, -i, -plong), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(0, -i, plong), lightModel->Diffuse));

		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(pwidth, i, 0), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(-pwidth, i, 0), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(pwidth, -i, 0), lightModel->Diffuse));
		verticeVector.push_back(SimpleVertex(D3DXVECTOR3(-pwidth, -i, 0), lightModel->Diffuse));

		numVertices += 24;
	}

	strideLength = sizeof(SimpleVertex);

	SimpleVertex *vertices = &verticeVector[0];

	HRESULT result;

	D3D10_BUFFER_DESC vertexDesc;
	ZeroMemory(&vertexDesc, sizeof(vertexDesc));

	vertexDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	vertexDesc.ByteWidth = sizeof(SimpleVertex) * numVertices;
	vertexDesc.CPUAccessFlags = 0;
	vertexDesc.MiscFlags = 0;
	vertexDesc.Usage = D3D10_USAGE_IMMUTABLE;

	D3D10_SUBRESOURCE_DATA vData;
	ZeroMemory(&vData, sizeof(vData));

	vData.pSysMem = vertices;

	result = device->CreateBuffer(&vertexDesc, &vData, &vertexBuffer);
	if(FAILED(result))
	{
		string message = "Failed to create vertex buffer for Cube";
		Trace::Log(message);
		textInterface->Warning(message);
		return false;
	}

	return true;
}


bool Engine_I::Grid::Render()
{
	if(!Model::Render()) return false;
}

void Engine_I::Grid::ShutDown()
{
	Model::ShutDown();
}

