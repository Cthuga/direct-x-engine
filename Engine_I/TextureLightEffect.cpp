#include "TextureLightEffect.h"

using namespace Engine_I;

Engine_I::TextureLightEffect::TextureLightEffect()
{
	textureState = 0;
}

Engine_I::TextureLightEffect::~TextureLightEffect()
{

}

bool Engine_I::TextureLightEffect::Init(MainTextInterface* mInterface, ID3D10Device *device, AbstractCbFixedDesc *cbFixed)
{
	if(!AbstractEffect::Init(L"textured_light.fx", mInterface, "BaseTechnique", device, cbFixed)) return false;
	
	HRESULT result;

	D3D10_PASS_DESC passDesc;
	technique->GetPassByIndex(0)->GetDesc(&passDesc);

	D3D10_INPUT_ELEMENT_DESC elDes[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{ "SPEC_POW", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 24, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{ "TEXELS", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 40, D3D10_INPUT_PER_VERTEX_DATA, 0}
	};

	result = device->CreateInputLayout(elDes, 4, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &inputLayout);
	if(FAILED(result))
	{
		string message = "Creating Input Layout for effect textured_light.fx failed";
		Trace::Log(message);
		mainInterface->Warning(message);
		return false;
	}

	return true;
}

void Engine_I::TextureLightEffect::UpdateFixed(AbstractCbFixedDesc* abstractCbFixedDesc)
{
	AbstractEffect::UpdateFixed(abstractCbFixedDesc);

	CbFixedDesc* effectFixed = (CbFixedDesc*)abstractCbFixedDesc;
	ID3D10EffectVariable *fog = effect->GetVariableByName("fog");
	fog->SetRawValue(&effectFixed->Fog, 0, sizeof(effectFixed->Fog));
}

void Engine_I::TextureLightEffect::UpdatePerObject(AbstractCbPerObjectDesc* abstractEffectDesc)
{
	AbstractEffect::UpdatePerObject(abstractEffectDesc);

	CbPerObjectDesc* baseDesc = (CbPerObjectDesc*)abstractEffectDesc;
	ID3D10EffectMatrixVariable *matrix = effect->GetVariableByName("WVP")->AsMatrix();
	matrix->SetMatrix(baseDesc->WVP);
	ID3D10EffectMatrixVariable *wMatrix = effect->GetVariableByName("W")->AsMatrix();
	wMatrix->SetMatrix(baseDesc->W);
	ID3D10EffectScalarVariable *texAdressType = effect->GetVariableByName("texAdressingType")->AsScalar();
	texAdressType->SetRawValue(&baseDesc->texAdressingType, 0, sizeof(int));
	ID3D10EffectScalarVariable* clipVal = effect->GetVariableByName("gClip")->AsScalar();
	clipVal->SetBool(baseDesc->Clip);
	ID3D10EffectScalarVariable* fogVal = effect->GetVariableByName("gFog")->AsScalar();
	fogVal->SetBool(baseDesc->Fog);
}

void Engine_I::TextureLightEffect::UpdatePerFrame(AbstractCbPerFrameDesc* abstractEffectDesc)
{
	CbPerFrameDesc* baseDesc;
	baseDesc = (CbPerFrameDesc*)abstractEffectDesc;		

	ID3D10EffectVariable* lightArray = effect->GetVariableByName("lightArray");
	LightDesc *d = baseDesc->Lights;
	HRESULT result = lightArray->SetRawValue(&baseDesc->Lights, 0, sizeof(LightDesc) * 256);

	ID3D10EffectVariable* eye = effect->GetVariableByName("eyePos")->AsVector();
	eye->SetRawValue(baseDesc->EyePos, 0, sizeof(D3DXVECTOR3));

	ID3D10EffectScalarVariable* lightsCount = effect->GetVariableByName("lightsCount")->AsScalar();
	lightsCount->SetInt(baseDesc->LightNum);
}

void Engine_I::TextureLightEffect::ShutDown()
{
	AbstractEffect::ShutDown();
}

void Engine_I::TextureLightEffect::UpdatePerTexture(ID3D10ShaderResourceView* texture)
{
	AbstractEffect::UpdatePerTexture(texture);

	if(!textureState || textureState != texture)
	{
		textureState = texture;
		ID3D10EffectShaderResourceVariable *shaderVariable = effect->GetVariableByName("gTexture")->AsShaderResource();
		shaderVariable->SetResource(texture);
	}
}