# DIRECT X TRAINING ENGINE V 0.0.1  #

![](http://geekmessage.files.wordpress.com/2013/02/repositoryimage.png)

1. About Me
2. Installation and requirements
3. How to use it
4. FAQ
5. References

## 1. ABOUT ME ##

- **Author** 	      : Norbert Ozdoba
- **Blog**	      : [http://geekmessage.wordpress.com](http://geekmessage.wordpress.com)
- **e-mail**	      : norbert.ozdoba@gmail.com
- **profession**    : programmer

I am interested in programming since I was 12. Mostly my career revolved around business programing using C# and MSSQL, however when I was Young, I had some
game develpment experience. Since 2013\02 I've decided to change my interests to Game Development. Now, most of free time, when I am not working as business
developer, I am learning C++, DirectX, Adveced Physic, Math, AI, and other usefull things hoping for become professionalist.

I believe that best way of learning is sharing Your knowledge. I know, that there is a lot more to do with my actuall work, but I hope that someone would find 
something usefull in this code. That's why I've spent some time to document it well. Every help, recommendation, noticing for some incorrect syntax would be 
warmly welcomed.

Also, I really warmly encourage for visiting my blog: 

### [http://geekmessage.wordpress.com](http://geekmessage.wordpress.com) ###

## 2. INSTALLATION AND REQUIREMENTS  ##

Requirements:

  - Visual Studio ( any edition ), probably it would work from 2005 to 2012 ( please pm when there would be problems )
  - .NET Framework 3.5
    ([http://www.microsoft.com/en-us/download/details.aspx?id=22](http://www.microsoft.com/en-us/download/details.aspx?id=22))
  - DirectX 10 Libraries 
    ([http://www.microsoft.com/en-us/download/details.aspx?id=6812](http://www.microsoft.com/en-us/download/details.aspx?id=6812))

Installation:

  - Install/Unzip Dorect X folder to any destination
  - Unzip folder "Engine_I" to any destination
  - Run *.sln file using Visual Studio Environment
  - Point for DirectX library in Visual Studio Environment. 
    Check for example:
     [http://www.rastertek.com/dx10tut01.html](http://www.rastertek.com/dx10tut01.html)
  - Rebuild project hoping for no errors. If there would be some problems, please pm me     

## 3. HOW TO USE IT ##

 - You should start examination from main.cpp
 - Description of all classes, methods and properties are in header files, when examining code I suggest routine described below:
	
  1. Examine code during debugging starting from here
  2. Most comments doesn't exists in method body. If You don't know what object,method or variable do, check 
  	  object,method,variable declaration in header file of inspected class [TIP : usefull shortcut in VS = CTRL + F12 ]
  3. For fields and properties - if description in header file of inspected class is not enough, check header class definition for inspected property

----------

Example: Debugging **Graphic::Init**
			  
**Step 1** - checking description of examined code

	//Initialization routine 
	//For more info about initialized instances check their descriptions in graphic class
	if(!d3XCore.Init(hwnd, width, height, device, swapChain, renderTargetView, depthStencilView, font, config.configGraphic.FullScreen))

**Step 2** - checking header class definition of graphic class

	D3XCore d3XCore;	//This is Direct X controller. Check D3XCore.h for reference

**Step 3** - checking header class definition of inspected property

	//ClassName   : D3XCore
	//LifeTime: Initialized during graphic initialization, shutduwn during closing application
	//Description : Direct X Core. Initializing all important Direct X interfaces

	//Name		  : Init
	//Description : Initialization of DirectX components which doesn't change during resizing routine
	//Input		  : hwnd		 - handler to application
	//		width		 - width of the screen
	//		height		 - height of the screen
	//		device		 - reference to device pointer. Parameter would be filled with new value
	//		swapChain	 	 - reference to swap chain pointer. Parameter would be filled with new value
	//		renderTargetView 	 - reference to render target view pointer. Parameter would be filled with new value
	//		depthStencilView 	 - reference to depth stencil view pointer. Parameter would be filled with new value
	//		font	 - reference to direct x font interface pointer. Parameter would be filled with new value
	//		fullscreen		 - full screen flag
	//Output	  : true when no errors, else false

----------

 - Remember that debugger doesn't take steps into *.fx files !
 - One more important annotation:
   Engine is separated for two parts :
	- Core : that is almost everything with exception on :
	- Drawings : that's part of game elements , not core engine. It would be later separated to different project actually for drawings there is :
	    - DrawWorld : drawing models to scene
		- DrawTextInterface : draw text in each corner of the screen
		- DrawLight - draw lights to scene
		- DrawConfiguration - some fixed configurations dependent on game type

## 4. FAQ ##

*Not at this moment*

## 5. REFERENCES ##

Textures for presentation:

- Animated water texture from ( \\Textures\water\ ) : [http://www.dgp.toronto.edu](http://www.dgp.toronto.edu) ( [http://www.dgp.toronto.edu/people/stam/reality/Research/PeriodicCaustics/index.html](http://www.dgp.toronto.edu/people/stam/reality/Research/PeriodicCaustics/index.html) )
- \\Textures\Sample\floor.jpg : [http://www.dregsld.com](http://www.dregsld.com)
- \\Textures\Sample\grass.jpg : [http://bestdesignoptions.com/?p=5250](http://bestdesignoptions.com/?p=5250)
- \\Textures\Sample\sky.jpg : [http://loadpaper.com/id27718/night-sky-texture-by-amdillon-on-deviantart-1600x1280-pixel.html](http://loadpaper.com/id27718/night-sky-texture-by-amdillon-on-deviantart-1600x1280-pixel.html)
- \\Textures\Sample\woods.jpg : [http://www.hdwallpapersarena.com/texture-wallpapers.htm](http://www.hdwallpapersarena.com/texture-wallpapers.htm)
 
   